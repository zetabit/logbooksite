<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->post('/api/auth', 'ApiController@postAuth');
$router->get('/api/custom_page', function(){
    return 'Ждите обновлений...  <script>document.location.href = \'/\';</script>';
});

//restricted area
$router->group(['middleware' => 'auth', 'prefix' => 'api'], function () use ($router) {
    /** @var \Laravel\Lumen\Routing\Router $router */
    $router->get('/color_list', 'Api\ApiController@colorList');
    $router->post('/color_create', 'Api\ApiController@colorCreate');

    //user
    $router->get('/user_data', 'Api\UserController@getPersonalData');
    $router->post('/user_save', 'Api\UserController@setPersonalData');
    $router->post('/change_password', 'Api\UserController@changePassword');

    //car
    $router->post('/car_save', 'Api\CarController@carSave');
    $router->post('/car_delete', 'Api\CarController@carDelete');
    $router->get('/cars_list', 'Api\CarController@carsList');
    $router->get('/car_detail/{id}', 'Api\CarController@carDetail');
    $router->get('/car_notifications/{id}', 'Api\NotificationController@carNotifications');
    $router->get('/car_history/{id}', 'Api\HistoryController@carHistory');
    $router->get('/car_last_5_history/{id}', 'Api\HistoryController@carLast5History');

    $router->get('/get_brands', 'Api\CarController@getBrands');
    $router->get('/get_brand_models/{brand_id}', 'Api\CarController@getBrandModels');

    //managers
    $router->get('/get_managers_list','Api\ManagerController@getManagersList');
    $router->post('/save_manager_data', 'Api\ManagerController@saveManagerData');
    $router->post('/remove_manager_data/{manager_id}', 'Api\ManagerController@removeManagerData');

    //driver
    $router->get('/driver_list', 'Api\DriverController@driverList');
    $router->post('/add_driver', 'Api\DriverController@save');
    $router->post('/select_driver', 'Api\InspectionController@selectDriver');

    //notifications
    $router->get('/notification_list', 'Api\NotificationController@notificationList');
    $router->get('/set_notifications_read','Api\NotificationController@setNotificationsRead');
    $router->get('/set_car_notifications_read/{car_id}','Api\NotificationController@setCarNotificationsRead');

    $router->group(['prefix' => 'checkup'], function () use ($router) {
        /** @var \Laravel\Lumen\Routing\Router $router */
        $router->post('/start', 'Api\InspectionController@startCheckup');
        $router->post('/acceptPhotoPart', 'Api\InspectionController@acceptPhotoPart');
        $router->post('/acceptComplection', 'Api\InspectionController@acceptCompleteness');
        $router->post('/acceptCarPicture', 'Api\InspectionController@acceptCarPicture');
        $router->post('/acceptOrderOutfit', 'Api\InspectionController@acceptOrderOutfit');
        $router->post('/acceptFinishCheckup', 'Api\InspectionController@acceptFinishCheckup');
    });
});
