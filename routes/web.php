<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function(){
   return view('pages.main');
});
$router->post('/contact_us',['as' => 'contact_us', 'uses' => 'Web\DashboardController@send']);

$router->get('/tmp', function() {
    $contract = \App\Models\BaseContract::with('autoPark', 'autoPark.address', 'autoPark.lawAddress')->firstOrCreate(['autopark_id' => 1]);
    $user = \Auth::user();
    $contracts = $user->autoPark->contracts;
    return view('pages.contract', compact('contract', 'contracts'));
});
/** @var \Laravel\Lumen\Routing\Router $router */
$router->post('/auth', 'ApiController@postAuth');
$router->get('/login', 'Web\DashboardController@login');
$router->get('/loginN', 'Web\DashboardController@loginN');
//restricted web area
$router->group(['middleware' => 'web_auth','prefix' => '/web'], function() use ($router) {
    /** @var \Laravel\Lumen\Routing\Router $router */
    $router->get('/', ['as' => 'home', 'uses' => 'Web\DashboardController@index']);
    $router->get('/logout', ['as' => 'logout', 'uses' => 'Web\DashboardController@logout']);
    $router->get('/activate', 'Web\DashboardController@activate');
    //cars
    $router->get('/cars', ['as' => 'cars', 'uses' => 'Web\CarController@index']);
    $router->get('/car/add', ['as' => 'car_add', 'uses' => 'Web\CarController@add']);
    $router->post('/car/save', ['as' => 'car_save', 'uses' => 'Web\CarController@save']);
    $router->get('/car/{car_id}', ['as' => 'car', 'uses' => 'Web\CarController@edit']);
    $router->get('/car_info/{car_id}', ['as' => 'car_info', 'uses' => 'Web\CarController@car_info']);
    $router->get('/car_info_bills/{car_id}', ['as' => 'car_info_bills', 'uses' => 'Web\CarController@car_info_bills']);
    $router->post('/car/remove',['as' => 'car_remove', 'uses' => 'Web\CarController@remove']);
    $router->get('/car_to_check/{car_id}', 'Web\DashboardController@carTOcompleted');
    $router->get('/get_brand_models/{brand_id}', 'Web\CarController@get_brand_models');
    $router->get('/get_sorted_cars','Web\CarController@get_sorted_cars');

    //managers
    $router->get('/managers', ['as' => 'managers', 'uses' => 'Web\ManagerController@index']);
    $router->get('/manager/add/{step}', ['as' => 'manager_add', 'uses' => 'Web\ManagerController@add']);
    $router->post('/manager/save', ['as' => 'manager_save', 'uses' => 'Web\ManagerController@save']);
    $router->get('/manager/edit_info/{manager_id}', ['as' => 'manager_info', 'uses' => 'Web\ManagerController@edit_info']);
    $router->get('/manager/edit_data/{manager_id}', ['as' => 'manager_data', 'uses' => 'Web\ManagerController@edit_data']);
    $router->get('/get_role_permissions/{role_id}','Web\ManagerController@get_role_permissions');

    //drivers
    $router->get('/drivers', ['as' => 'drivers', 'uses' => 'Web\DriverController@index']);
    $router->get('/driver_info/{driver_id}', ['as' => 'driver_info', 'uses' => 'Web\DriverController@driver_info']);
    $router->get('/driver/add_driver_data', ['as' => 'driver_add_data', 'uses' => 'Web\DriverController@add_driver_data']);
    $router->post('/driver/add_driver_images', ['as' => 'driver_add_images', 'uses' => 'Web\DriverController@add_driver_images']);
    $router->post('/driver/save', ['as' => 'driver_save', 'uses' => 'Web\DriverController@save']);
    $router->get('/driver/edit_info/{driver_id}', ['as' => 'driver_info', 'uses' => 'Web\DriverController@edit_info']);
    $router->post('/driver/edit_data/{driver_id}', ['as' => 'driver_data', 'uses' => 'Web\DriverController@edit_data']);
    $router->post('/driver/save_additional_pics', ['as' => 'driver_save', 'uses' => 'Web\DriverController@save_additional_pics']);
    $router->post('/driver/remove_additional_image', ['as' => 'driver_remove_additional_image', 'uses' => 'Web\DriverController@remove_additional_image']);
    $router->post('/driver/set_additional_image_description', ['as' => 'set_additional_image_description', 'uses' => 'Web\DriverController@set_additional_image_description']);
    $router->post('/driver/remove',['as'=>'driver_remove', 'uses'=>'Web\DriverController@remove']);

    //notifications
    $router->get('/notifications', ['as'=> 'notifications', 'uses'=> 'Web\NotificationController@notifications']);
    $router->get('/show_notif/{notif_id}', ['as'=> 'notification', 'uses'=> 'Web\NotificationController@show']);
    $router->get('/more_notifs', ['as'=> 'notification', 'uses'=> 'Web\NotificationController@more']);

    //profile
    $router->get('/profile', 'Web\ProfileController@index');
    $router->post('/profile/save', 'Web\ProfileController@save');


    //other
    $router->get('/fines', ['as' => 'bills', 'uses' => 'Web\BillsController@index']);
    $router->get('/fines/syncCars', ['as' => 'bills.syncCars', 'uses' => 'Web\BillsController@syncCars']);
    $router->get('/trello', function (){
        return view('pages.in_progress');
    });
    $router->get('/statistic', function (){
        return view('pages.in_progress');
    });
    $router->get('/archive', function (){
        return view('pages.in_progress');
    });


    $router->get('/checkup/{id}', ['as' => 'checkup', 'uses' => 'Web\DashboardController@checkup']);

    $router->get('/bills/reauth', ['as' => 'bills_reauth', 'uses' => 'Web\BillsController@reauth']);

    $router->group([
        'prefix' => '/contracts',
    ], function() use ($router) {
        $router->get('/', ['as' => 'contracts', 'uses' => 'Web\ContractsController@index']);
        $router->get('/add', ['as' => 'contracts.add', 'uses' => 'Web\ContractsController@add']);
        $router->get('/show/{id}', ['as' => 'contracts.show', 'uses' => 'Web\ContractsController@show']);
        $router->post('/save', ['as' => 'contracts.save', 'uses' => 'Web\ContractsController@save']);
        $router->get('/print/{id}', ['as' => 'contracts.print', 'uses' => 'Web\ContractsController@print_contract']);
    });


});
