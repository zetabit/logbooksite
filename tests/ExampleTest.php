<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    /** @test */
    function test_something()
    {
        $user = \App\User::first();
        $this->expectsNotification($user, \App\Notifications\CarDamagedNotification::class);
        $this->doesntExpectNotification($user, \App\Notifications\CarDamagedNotification::class);
    }
}
