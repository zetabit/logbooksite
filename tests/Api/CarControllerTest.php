<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CarControllerTest extends TestCase
{
  public function testCarList()
    {
      /** @var \App\User $user */
      $user = \App\User::first();
      $this->be($user);
      $cars = $user->cars();

      //car list
      $req = $this
        ->get('/api/cars_list');
      $req
        ->assertJson($cars->toJson());

      $carsArray = json_decode($req->response->getContent());
      $carArray = (array)$carsArray[0];

      //check one car for needed data
      $this->assertTrue(
        isset($carArray['model']) &&
        isset($carArray['checkup']) &&
        isset($carArray['status'])
      );
    }
}
