<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ApiControllerTest extends TestCase
{
  public function testAuth()
  {
    $this->authApi();
  }

  public function testColors()
    {
      $this->withoutMiddleware();
      $colors = \App\Models\Color::all();

      $this
        ->get('/api/color_list')
        ->assertJson($colors->toJson());
    }

  /**
   * @dataProvider providerColors
   */
    public function testColorCreate($name, $colorCode)
    {
      $this->be(\App\User::first());
      /** @var \App\Models\Color $color */
      $color = \App\Models\Color::where([
        'name' => $name,
        'color' => $colorCode
      ])->first();

      if($color) $color->delete();

      $colors = \App\Models\Color::all();

      $this
        ->post('/api/color_create', [
          'name'    => $name,
          'value'   => $colorCode
        ])
        ->seeJsonContains([
          'name'    => $name,
          'color'   => $colorCode
        ]);
    }

    public function providerColors() {
      return [
        [
          'name'    => 'Красный (красивый)',
          'color'   => '#ff0000'
        ],
        [
          'name'    => 'Белый (элегантность)',
          'color'   => '#ffffff',
        ],
        [
          'name'    => 'Синий (свежесть)',
          'color'   => '#0000ff'
        ]
      ];
    }

//    /** @test */
//    function test_something()
//    {
//        $user = \App\User::first();
//        $this->expectsNotification($user, \App\Notifications\CarDamagedNotification::class);
//        $this->doesntExpectNotification($user, \App\Notifications\CarDamagedNotification::class);
//    }
}
