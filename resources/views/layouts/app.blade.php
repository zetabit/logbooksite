<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Бортовой журнал | Панель</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
@section('header_css')
    <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="/Admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/Admin/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/Admin/bower_components/Ionicons/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="/Admin/bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/Admin/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/Admin/css/skins/_all-skins.min.css">
@show

@section('header_js')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
@show

<!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="/web" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>БЖ</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">Бортовой журнал</span>
        </a>

    @section('navbar')
        <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#">
                                <span class="hidden-xs">Сообщения</span>
                            </a>
                        </li><li class="dropdown user user-menu">
                            <a href="{{url('/web/profile')}}">
                                <span class="hidden-xs">Профиль</span>
                            </a>
                        </li>
                        <li class="dropdown user user-menu">
                            <a href="{{ route('logout') }}">
                                <span class="hidden-xs">Выход</span>
                            </a>
                        </li>
                    </ul>
                </div>

            </nav>
        @show
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->

            <ul class="sidebar-menu" data-widget="tree">
                <li><a href="/web/cars"><i class="fa fa-car"></i> <span>Автомобили</span></a></li>
                <li><a href="/web/managers"><i class="fa fa-users"></i> <span>Сотрудники</span></a></li>
                <li><a href="/web/drivers"><i class="fa fa-black-tie"></i> <span>Водители</span></a></li>
                <li><a href="/web/trello" onclick="inProgress()"><i class="fa fa-hand-o-right"></i> <span>Планировщик задач</span></a></li>
                <li><a href="/web/fines"><i class="fa fa-rub"></i> <span>Штрафы</span></a></li>
                <li><a href="/web/notifications"><i class="fa fa-warning "></i>
                        <span>Уведомления</span>
                        @if(Auth::user()->unreadNotifications->count())
                            <span class="pull-right1-container">
                                <small class="label pull-right bg-yellow">{{ Auth::user()->unreadNotifications->count()  }}</small>
                            </span>
                        @endif
                    </a>
                </li>
                <li><a href="/web/statistic" onclick="inProgress()"><i class="fa fa-line-chart"></i> <span>Статистика</span></a></li>
                <li><a href="{{ route('contracts') }}"><i class="fa fa-registered"></i> <span>Документация</span></a></li>
                <li><a href="/web/archive" onclick="inProgress()"><i class="fa fa-bank"></i> <span>Архив</span></a></li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @section('content')
        @show
    </div>
    <!-- /.content-wrapper -->

    @section('footer')
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 0.1.12
            </div>
            <strong>Copyright &copy; 2017 - <?=date('Y')?> Powered by zetaBit.</strong> All rights
            reserved.
        </footer>
    @show

</div>
<!-- ./wrapper -->

@section('footer_js')
    <!-- jQuery 3 -->
    <script src="/Admin/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/Admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/Admin/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="/Admin/js/adminlte.min.js"></script>
    <!-- Sparkline -->
    <script src="/Admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap  -->
    <script src="/Admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/Admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll -->
    <script src="/Admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script>
        function inProgress(){
            event.preventDefault();
            event.stopPropagation();
            alert('Извините, данная страница находится в разработке.');
        }
    </script>

@show

</body>
</html>
