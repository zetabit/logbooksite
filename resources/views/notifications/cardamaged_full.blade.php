<div style="margin-bottom:20px;">
    Менеджер: {{ $notification->checkupCarPart->checkup->user->info->fio() }}<br/>
    Водитель: {{ $notification->checkupCarPart->checkup->driver->fio() }}<br/>
    Автомобиль: {{ $notification->checkupCarPart->checkup->car->brand->name }} {{ $notification->checkupCarPart->checkup->car->carModel->name }} ({{ $notification->checkupCarPart->checkup->car->number }}) <br/>
    Создано: {{ $databaseNotification->created_at->format('d.m.Y H:i:s') }} <br/>
    @if( $databaseNotification->read_at)
        Просмотрено: {{ $databaseNotification->read_at->format('d.m.Y H:i:s')  }} <br/>
    @endif
    Комментарий: {{ $notification->checkupCarPart->picture->alt }}
</div>
<div>
    <img src="/Upload/Pictures/{{ $notification->checkupCarPart->picture->slug }}.{{ $notification->checkupCarPart->picture->extension }}" alt="Повреждение {{ $notification->checkupCarPart->carPart->name}}" style="width:100%;">
</div>