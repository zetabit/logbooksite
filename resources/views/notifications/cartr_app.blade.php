<article class="car__notification-item" id="notification_{{ $databaseNotification->id }}" data-car="{{ $notification->car->id }}">
    <div class="car__notification-info">
        <div class="car__notification-user">
            {{ $notification->car->model }} ({{ $notification->car->number }})<br>
        </div>
        <time class="car__notification-date" datetime="{{ $databaseNotification->created_at }}">{{ $databaseNotification->created_at }}</time>
        <time>{{ $databaseNotification->id }}</time>
    </div>
    <div class="car__notification-descr">
        <p>До прохождения ТО осталось {{$notification->car->run_closest - $notification->car->run_total}} км.</p>
    </div>
</article>