<article class="car__notification-item @if($databaseNotification->read()) notification__item--check @endif" id="notification_{{ $databaseNotification->id }}" data-car="{{ $notification->car->id }}">
    <div class="car__notification-info">
        <div class="car__notification-user">
            Менеджер:
            <span class="car__notification-name">{{ $notification->manager->info->FirstName }}</span>
            <span class="car__notification-surname">{{ $notification->manager->info->LastName }}</span>
        </div>
        <time class="car__notification-date" datetime="{{ $databaseNotification->created_at }}">{{ $databaseNotification->created_at }}</time>
    </div>
    <div class="car__notification-descr">
        <p>Обнаружено поврежедение. Фото обновлено.</p>
        <a class="car__notification-link">Показать</a>
    </div>
    <div class="car__notification-photo">
        @php($picture = $notification->checkupCarPart->picture)
        <img src="{{ url("/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension) }}" alt="">
    </div>
</article>