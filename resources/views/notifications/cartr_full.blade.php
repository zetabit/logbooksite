<div style="margin-bottom:20px;">
    Менеджер: {{ $databaseNotification->notifiable->info->fio() }}
    Автомобиль: {{ $notification->car->brand->name }} {{ $notification->car->carModel->name }} ({{ $notification->car->number }}) <br/>
    Создано: {{ $databaseNotification->created_at->format('d.m.Y H:i:s') }} <br/>
    @if( $databaseNotification->read_at)
        Просмотрено: {{ $databaseNotification->read_at->format('d.m.Y H:i:s')  }} <br/>
    @endif
    Комментарий: До прохождения ТО осталось {{ $notification->car->run_closest - $notification->car->run_total }}
</div>
