<tr style="cursor:pointer;" class="info_row" data-type="damage" data-notif-id="{{$databaseNotification->id}}" onclick="openNotification(this)" >
    <td>{{ $notification->car->model }} ({{ $notification->car->number }})</td>
    <td>{{ $databaseNotification->notifiable->info->fio() }}</td>
    <td>Обнаружено повреждение ({{ $notification->checkupCarPart->carPart->name }})</td>
    <td>{{ $databaseNotification->created_at }}</td>
    <td style="text-align: center; font-size:150%;">@if($databaseNotification->read_at)<i class="fa fa-check" style="color:green;"></i> @else <i class="fa fa-remove" id="{{ $databaseNotification->id }}" style="color:red;"></i> @endif</td>
</tr>