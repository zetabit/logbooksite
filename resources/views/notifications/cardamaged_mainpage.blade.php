
    <div class="box collapsed-box box-solid">
        <div class="box-header no-border">
            <div>
                <p class="box-title" style="font-size: 16px ;">
                    Менеджер: {{ $databaseNotification->notifiable->info->firstname }} {{ $databaseNotification->notifiable->info->lastname }}</p>,
                    Осмотр: #{{ $notification->checkupCarPart->checkup->id }}
                <p style="color: darkgrey; font-size: 90%" >{{ $databaseNotification->created_at->format('d.m.Y') }}</p>
            </div>
            <div>
                <p class="box-title" style="font-size: 14px ;">Обнаружено повреждение <span style="font-weight:600;">{{ $notification->checkupCarPart->carPart->name }}</span> автомобиля <span style="font-weight:600;">{{ $notification->car->number }}</span>. Фото обновлено</p>
            </div>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <p>
                {{ $notification->checkupCarPart->picture->alt }}
            </p>
            <img style="width: 50%" src="/Upload/Pictures/{{ $notification->checkupCarPart->picture->slug }}.{{ $notification->checkupCarPart->picture->extension }}">
        </div>
        <!-- /.box-body -->
    </div>
