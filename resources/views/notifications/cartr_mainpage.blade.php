    <div class="box collapsed-box box-solid">
        <div class="box-header no-border">
            <div>
                <p class="box-title">{{ $notification->car->number }}, Менеджер: {{ $databaseNotification->notifiable->info->firstname }} {{ $databaseNotification->notifiable->info->lastname }}</p>
                <p style="color: darkgrey;">{{ $databaseNotification->created_at->format('d.m.Y H:i:s') }} - #{{ $databaseNotification->id }}</p>
            </div>
            <div>
                <p class="box-title">До прохождения ТО осталось {{ $notification->car->run_closest - $notification->car->run_total }}</p>
            </div>
        </div>
    </div>