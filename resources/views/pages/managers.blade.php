@extends('layouts.app')
@section('header_css')
    @parent
    <link rel="stylesheet" href="/Admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li class="active">Сотрудники</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" style="margin-top:20px;">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <p class="pull-left" style="font-size: 120%;">
                            Сотрудники <span class="label label-primary">@if($managers) {{count($managers)}} @endif</span>
                        </p>
                        <a class="btn btn-info pull-right" href="{{url('/web/manager/add/1')}}">Добавить сотрудника</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                            <h3 class="box-title">Список сотрудников</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" id="table_place">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th>№</th>
                                            <th >ФИО</th>
                                            <th >Должность</th>
                                            <th >Логин</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($managers && count($managers)>0)
                                            @foreach($managers as $manager)
                                                <tr role="row" style="cursor: pointer;" onclick="window.location.href=('/web/manager/edit_info/{{ $manager->id }}')">
                                                    <td class="sorting_1">{{ $manager->generateAbbreviation }}</td>
                                                    <td>{{ $manager->info->fio() }}</td>
                                                    <td>{{ $manager->role->readable_name }}</td>
                                                    <td>{{ $manager->email}}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1">ID</th>
                                            <th rowspan="1">ФИО</th>
                                            <th rowspan="1">Должность</th>
                                            <th rowspan="1">Логин</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
                    <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section ('footer_js')
    @parent
    <script src="/Admin/js/jquery.dataTable.localised.js"></script>
    <script src="/Admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            initTable();
        });
        function initTable(){
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': false,
                'info': true,
                'autoWidth': false
            });
        }
    </script>
@endsection