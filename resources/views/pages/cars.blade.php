@extends('layouts.app')
@section('header_css')
    @parent
    <link rel="stylesheet" href="/Admin/plugins/bootstrap-slider/slider.css">
    <link rel="stylesheet" href="/Admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li class="active">Автомобили</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" style="margin-top:20px;">
            <div class="col-xs-12">
                <div class="box no-border">
                    <div class="box-body">
                        <p class="pull-left" style="font-size: 120%;">
                            Автомобили <span class="label label-primary">@if($cars) {{count($cars)}} @endif</span>
                        </p>
                        <a class="btn btn-info pull-right" href="{{url('/web/car/add')}}">Добавить автомобиль</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default no-border collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Сортировка</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="display: none;">
                        <div class="col-md-9">
                            <div class="form-group col-md-4">
                                <label>Марка</label>
                                <select class="form-control" name="brand-name">
                                    <option value="0" selected>Не выбрана</option>
                                    @if(!empty($brands))
                                        @foreach($brands as $brand)
                                            <option value="{{$brand->id}}">{{$brand->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Гос. номер</label>
                                <input class="form-control" name="car-number">
                            </div>
                            <div class="form-group col-md-4" style="min-height: 59px;">
                                <label>Расстояние до ближайшего ТО</label>
                                <input name="TO-distance" type="text" value="100,10000" class="slider form-control" data-slider-min="100" data-slider-max="10000" data-slider-step="100" data-slider-value="[100,10000]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue" style="display: none;" data-value="100,10000">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Модель</label>
                                <select class="form-control" name="model-name">
                                    <option value="0" selected>Не выбрана</option>
                                    @if(!empty($models))
                                        @foreach($models as $model)
                                            <option value="{{$model->id}}">{{$model->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-3" id="status-list">
                            <label>Текущий статус</label>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="1">
                                    На линии
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="0">
                                    В простое
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="2">
                                    В ремонте
                                </label>
                            </div>
                            <div>
                                <button class="btn btn-sm btn-info" onclick="applyFilterParams()">Применить</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- Main row -->

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Список автомобилей</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" id="table_place">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending">ID</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Марка</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Модель</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Гос Номер</th>
                                            <th id="color">Цвет</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Дата последнего осмотра</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Пробег при последнем осмотре</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Следующее ТО</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Статус</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Водитель</th>
                                            <th id="edit">Редактировать</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($cars && count($cars)>0)
                                            @foreach($cars as $car)
                                                <tr role="row" style="cursor: pointer;" onclick="getCarInfo({{$car->id}})">
                                                    <td class="sorting_1">{{ $car->generateAbbreviation }}</td>
                                                    <td>{{ $car->getBrandName() }}</td>
                                                    <td>{{ $car->getModelName() }}</td>
                                                    <td>{{ $car->number }}</td>
                                                    <td>
                                                        {{ $car->color()->first()->name }}
                                                    </td>
                                                    <td>@if($car->getLastCheckup()) {{ $car->getLastCheckup()->created_at }} @else Не проводился @endif</td>
                                                    <td>{{ $car->run_total}}</td>
                                                    <td>{{ $car->run_closest}}</td>
                                                    <td><span class="label @if($car->status==App\Models\Car::STATUS_ON_HOLD) label-default @elseif($car->status==App\Models\Car::STATUS_ON_LINE) label-info @elseif($car->status==App\Models\Car::STATUS_ON_SERVICE) label-default bg-navy @endif">{{ $car->getCarStatus()}}</span></td>
                                                    <td>@if($car->driver()) {{ $car->driver()->fio()}} @endif</td>
                                                    <td onclick="editCar({{$car->id}})" style="text-align: center; vertical-align: middle;"><i class="fa fa-edit" style="font-size: 150%; margin:0 auto;"></i></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1">ID</th>
                                            <th rowspan="1">Марка</th>
                                            <th rowspan="1">Модель</th>
                                            <th rowspan="1">Гос Номер</th>
                                            <th rowspan="1">Цвет</th>
                                            <th colspan="1">Дата последнего осмотра</th>
                                            <th rowspan="1">Пробег при последнем осмотре</th>
                                            <th rowspan="1">Следующее ТО</th>
                                            <th rowspan="1">Статус</th>
                                            <th rowspan="1">Водитель</th>
                                            <th rowspan="1">Редактировать</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
                <!-- /.box -->
        </div>
        <!-- /.row -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-lg" style="width: 80%;" role="document">
                <div class="modal-content">
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('footer_js')
    @parent

    <script src="/Admin/js/jquery.dataTable.localised.js"></script>
    <script src="/Admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/Admin/plugins/bootstrap-slider/bootstrap-slider.js"></script>
    <script>
        $(function () {
            $('.slider').slider();
            initTable();
            $('select[name="brand-name"]').change(function(){
                $.ajax({
                    url: '{{url('/web/get_brand_models')}}/'+$(this).val(),
                    success:function(msg){
                        $('select[name="model-name"]').html(msg)
                    }
                });
            });
            var url_string = window.location.href;
            var url = new URL(url_string);
            var car_id = url.searchParams.get("car_id");
            if(car_id.length){
                getCarInfo(car_id)
            }
        });
        function initTable(){
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });
            $('#edit').removeClass('sorting');
        }
        function applyFilterParams(){
            var $_brand=$('select[name="brand-name"]').val();
            var $_model=$('select[name="model-name"]').val();
            var $_number=$('input[name="car-number"]').val();
            var $_TO_distance=$('input[name="TO-distance"]').val();
            var $_car_status=[];
            $('#status-list').find('input:checkbox:checked').each(
                function(){
                    $_car_status.push($(this).val());
                }
            );
            var request_data={
                brand : $_brand,
                model : $_model,
                number : $_number,
                TO_distance : $_TO_distance,
                car_status : $_car_status
            };
            $.ajax({
                url: '{{url('/web/get_sorted_cars')}}',
                data: {sort_request_data:request_data},
                success : function(msg){
                    $('#table_place').html('').append(msg);
                    initTable();
                }
            });
        }
        function getCarInfo(car_id) {
            $.ajax({
                url:'/web/car_info/'+car_id,
                type:'GET',
                success:function(table){
                    $('#myModal').find('.modal-body').html('').append(table);
                    $('#myModal').modal('show');
                }
            })
        }
        function editCar(car_id){
            event.preventDefault();
            event.stopPropagation();
            window.location.href='/web/car/'+car_id;
        }
    </script>
@endsection