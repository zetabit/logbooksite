@extends('layouts.app')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Уведомления
        </h1>
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li class="active">Оповещения</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default no-border collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Сортировка</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="display: none;">
                        <div class="col-md-9">
                            <div class="form-group col-md-6">
                                <label>Автомобиль</label>
                                <select class="form-control" name="car">
                                    <option value="0" selected>Не выбран</option>
                                    @if(!empty($cars))
                                        @foreach($cars as $car)
                                            <option value="{{$car->id}}"><b>{{$car->number}}</b> ({{$car->getBrandName()}} {{$car->getModelName()}})</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Тип</label>
                                <select class="form-control" name="type">
                                    <option value="-1" selected>Не выбран</option>
                                    @if(!empty($types))
                                        @foreach($types as $type)
                                            <option value="{{$type[0]}}">{{$type[1]}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                        </div>
                        <div class="form-group col-md-3" id="status-list">
                            <div style="padding-top:25px;">
                                <button class="btn btn-sm btn-info" onclick="applyFilterParams()">Применить</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Оповещения</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Автомобиль</th>
                                <th>Менеджер</th>
                                <th>Сообщение</th>
                                <th>Создано</th>
                                <th>Просмотрено</th>
                            </tr>
                            @foreach($notifications as $notification)
                                {!! $notification->render() !!}
                            @endforeach
                            @if($more)
                                <tr class="more_button_row" align="center">
                                    <td colspan="5"><button class="btn btn-info" style="width:25%;" onclick="moreNotifications()">ЕЩЕ</button></td>
                                </tr>
                            @endif
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog" style="width: 80%;" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('footer_js')
    @parent
    <script>
        var filter=0;
        $('select').on('change',function(){
            filter=0;
        });
        function openNotification(notif_row) {
            var notif_id=$(notif_row).attr('data-notif-id');
            var type=$(notif_row).attr('data-type');
            if(type==='damage'){
                $('.modal-dialog').css({'width':'80%'});
            }else{
                $('.modal-dialog').attr('style','');
            }
            $.ajax({
                url:'/web/show_notif/'+notif_id,
                success:function(notification){
                    $('.modal-body').html('').append(notification);
                    $('#myModal').modal('show');
                    $('#'+notif_id).removeClass('fa-remove').addClass('fa-check').css('color', 'green');
                }
            });
        }
        function moreNotifications() {
            $('.more_button_row').remove();
            var notification_count=$('.info_row').length;
            var car_id=$('select[name="car"]').val();
            var type=$('select[name="type"]').val();
            var request_data={
                count: notification_count,
                type : type,
                car_id : car_id
            };
            $.ajax({
               url:'/web/more_notifs',
               data: {notif_request_data:request_data},
               success:function(rows){
                   $('tbody').append(rows)
               }
            });
        }
        function applyFilterParams() {
            if(filter>0){
                var notification_count=$('.info_row').length;
            }else{
                var notification_count=0;
            }
            filter++;
            var car_id=$('select[name="car"]').val();
            var type=$('select[name="type"]').val();
            var request_data={
                count: notification_count,
                type : type,
                car_id : car_id
            };
            $('.info_row').remove();
            $('.more_button_row').remove();
            $.ajax({
                url: '/web/more_notifs',
                data: {notif_request_data:request_data},
                success: function(rows){
                    $('tbody').append(rows);
                }
            });
        }

    </script>
@endsection