@extends('layouts.app')
@section('header_css')
    @parent
<link rel="stylesheet" href="/Admin/bower_components/select2/dist/css/select2.min.css">
@endsection
@section('content')

    <section class="content-header">
        <h1 style="color:#ecf0f5;">Автомобиль</h1>
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li><a href="/web/cars"><i class="fa fa-list"></i> Список машин</a></li>
            <li class="active">@if(isset($car)) Редактирование автомобиля @else Добавление автомобиля @endif</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">@if(isset($car)) Редактирование автомобиля @else Добавление автомобиля @endif</h3>
                    @if(isset($car)) <button class="btn pull-right" style="color:white; border-color:white; background-color:#222d32;" onclick="delete_car({{$car->id}})">Удалить автомобиль</button> @endif
                </div>
                <hr/>
                <div class="box-body">
                    <form action="{{ url('/web/car/save') }}" method="post" name="car_form" enctype="multipart/form-data">
                        @if(isset($car)) <input type="hidden" name="car_id" value="{{$car->id}}"> @endif
                        <div class="col-md-10">
                            <div class="form-group col-md-6">
                                <label>Марка</label>
                                <select name="brand_id" id="brand_id" class="form-control" required>
                                    @if($brands && count($brands)>0)
                                        <option value="0">Не выбрана</option>
                                        @foreach($brands as $brand)
                                            <option value="{{ $brand->id }}" @if(isset($car) && $car->brand_id == $brand->id) selected @endif>{{ $brand->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="from-group col-md-6">
                                <label>Текущий пробег автомобиля</label>
                                <input type="number" name="run_total" id="run_total" class="form-control runuble" required @if(isset($car)) value="{{ $car->run_total }}" @endif/>
                                <span class="help-block"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6">
                                <label>Модель</label>
                                <select name="model_id" id="model_id" class="form-control" required>
                                    <option value="0">Не выбрана</option>
                                    @if(isset($car))
                                        @if(isset($models)&&count($models)>0)
                                            @foreach($models as $model)
                                                <option value="{{$model->id}}" @if($model->id == $car->model_id) selected @endif>{{$model->name}}</option>
                                            @endforeach
                                        @endif
                                    @endif
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="from-group col-md-6">
                                <label>Пробег при последнем ТО</label>
                                <input type="number" name="run_last" id="run_last" class="form-control runuble" @if(isset($car)) value="{{ $car->run_last }}" @endif/>
                                <span class="help-block"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="from-group col-md-6">
                                <label>Регистрационный номер</label>
                                <input type="text" style="text-transform: uppercase" name="number" id="number" class="form-control" required @if(isset($car)) value="{{ $car->number }}" @endif/>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Плановый интервал сервисного обслуживания</label>
                                <select name="run" id="run" class="form-control runuble" required>
                                    <option value="0">Не выбран</option>
                                    @if(isset($runs_list))
                                        @foreach($runs_list as $run)
                                            <option @if(isset($car)&&$car->run==$run) selected @endif value="{{ $run }}">{{ $run }}</option>
                                        @endforeach
                                    @endif
                                    @if(isset($car)&&!in_array($car->run,$runs_list))
                                        <option selected value="{{ $car->run }}">{{ $car->run }}</option>
                                    @endif
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="from-group col-md-6">
                                <label>Номер СТС</label>
                                <input type="text" id="sts" style="text-transform: uppercase" name="sts" class="form-control" required @if(isset($car)) value="{{ $car->sts }}" @endif/>
                                <span class="help-block"></span>
                            </div>
                            <div class="from-group col-md-6">
                                <label>Оставшийся километраж до прохождения ТО</label>
                                <input type="number" id="diff_run_closest_total" name="diff_run_closest_total" class="form-control" @if(isset($car)) value="{{ $car->run_closest - $car->run_total }}" @endif readonly/>
                                <span class="help-block"></span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="from-group col-md-6">
                                <label>VIN</label>
                                <input type="text" style="text-transform: uppercase" id="vin" name="vin" class="form-control" @if(isset($car)) value="{{ $car->vin }}" @endif/>
                                <span class="help-block"></span>
                            </div>
                            <div class="from-group col-md-6">
                                <label>Цвет</label>
                                <select name="color_id" id="color_id" class="form-control" required>
                                    <option value="0">Не выбран</option>
                                        @if(isset($colors)&&count($colors)>0)
                                            @foreach($colors as $color)
                                                <option value="{{$color->id}}" @if(isset($car)&&$color->id == $car->color_id) selected @endif>{{$color->name}}</option>
                                            @endforeach
                                        @endif
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-2 form-group">
                            <label>Текущий статус</label>
                            <div class="checkbox">
                                <label>
                                    <input type="radio" name="status" value="1" disabled @if(isset($car)&&$car->status==1) checked @endif>
                                    На линии
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="radio" name="status" value="0" @if(isset($car)&&$car->status==0) checked @endif>
                                    В простое
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="radio" name="status" value="2" @if(isset($car)&&$car->status==2) checked @endif>
                                    В ремонте
                                </label>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <div class="col-md-12" style="margin-top:40px;">
                        <button class="btn col-md-2 pull-right" onclick="decline()" style="color:#00c0ef; background-color:white; border-color: #00c0ef; margin-left:40px;">Отменить</button>
                        <button class="btn btn-info col-md-2 pull-right" onclick="save_changes()">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer_js')
    @parent
    <script src="/Admin/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script>
        var fields= {
            'brand_id': 'Пожалуйста, укажите марку автомобиля.',
            'model_id': 'Пожалуйста, укажите модель автомобиля.',
            'number': 'Пожалуйста, укажите регистриционный номер автомобиля.',
            'sts': 'Пожалуйста, укажите СТС номер автомобиля.',
            'run_total': 'Пожалуйста, укажите текущий пробег автомобиля.',
            'run_last': 'Пожалуйста, укажите пробег автомобиля во время последнего ТО.',
            'run': 'Пожалуйста, укажите интервал прохождения ТО.',
            'diff_run_closest_total': 'Пожалуйста, укажите километраж до прохождения следующего ТО.',
            'status': 'Пожалуйста, укажите текущий статус автомобиля.'
        };
        $(function () {
            $('select[name="brand_id"]').change(function(ev){
                var s = ev.target;
                var id = s.options[s.selectedIndex].value;
                $('select[name="model_id"]')
                    .find('option')
                    .remove()
                    .end()
                    .attr('disabled', true);
                $.ajax({
                    url: '{{url('/web/get_brand_models')}}/'+id,
                    success:function(data){
                        if(data.length==0){
                            $('select[name="model_id"]').append($("<option></option>")
                                .text('Моделей данной марки не существует, вы можете добавить их'));
                        }else{

                            $('select[name="model_id"]').html(data)
                        }
                        $('select[name="model_id"]')[0].removeAttribute('disabled');
                        $('select[name="model_id"]').select2({
                            tags: true
                        });

                    }
                });
            }).select2({
                tags: true
            });
            $('select[name="model_id"]').select2({
                tags: true
            });
            $('select[name="color_id"]').select2({
                tags: true
            });
            $('select[name="run"]').select2({
                tags: true
            });
            count_run_diff();
            $('.runuble').on('change', function(){
                count_run_diff()
            });
        });
        function count_run_diff(){
            var diff_run=(parseInt($('#run_last').val())+parseInt($('#run').val()))-parseInt($('#run_total').val());
            $('#diff_run_closest_total').val(diff_run);
        }
        function save_changes(){
            event.preventDefault();
            var correct_counter=0;
            for (var field in fields){
                var parent_div=$('#'+field).parent('div');
                var children_span=$(parent_div).find('span.help-block');
                if(field!=='status'){
                    if($('#'+field).val()!==undefined&&$('#'+field).val()!==''&&$('#'+field).val()!=0){
                        correct_counter++;
                        $(parent_div).removeClass('has-error');
                        $(children_span).html('');
                    }else{
                        $(parent_div).addClass('has-error');
                        $(children_span).html(fields[field]);
                    }
                }else{
                    if($('form[name="car_form"] input[type="radio"]:checked').val()!==undefined){
                        correct_counter++;
                        $(parent_div).removeClass('has-error');
                        $(children_span).html('');

                    }else{
                        $(parent_div).addClass('has-error');
                        $(children_span).html(fields[field]);
                    }
                }
            }
            if(correct_counter===9){
                $("form[name='car_form']").submit();
            }
        }
        function decline(){
            var answer=confirm("Вы уверены, что хотите покинуть страницу? Изменения не будут сохранены.");
            if(answer){
                window.location.href='{{ url('/web/cars') }}';
            }
        }
        function delete_car(car_id){
            var answer=confirm("Вы уверены, что хотите удалить автомобиль? Данные о нем останутся в архиве.");
            if(answer){
                $.ajax({
                    method:'POST',
                    url:'{{ url('/web/car/remove') }}',
                    data:{car_id: car_id},
                    success:function(msg){
                       window.location.href='{{ url('/web/cars') }}';
                    }
                });

            }
        }
    </script>
@endsection