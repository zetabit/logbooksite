@extends('layouts.app')
@section('header_css')
    @parent
    <link rel="stylesheet" href="/Admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Водители
        </h1>
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li class="active">Водители</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" style="margin-top:20px;">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <p class="pull-left" style="font-size: 120%;">
                            Водители <span class="label label-primary">@if($drivers) {{count($drivers)}} @endif</span>
                        </p>
                        <a class="btn btn-info pull-right" href="{{url('/web/driver/add_driver_data')}}">Добавить сотрудника</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Список водителей</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending">ID</th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">ФИО </th>
                                            <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Номер водительского удостоверения</th>
                                            <th id="edit">Редактировать</th>
                                            <th id="drop">Удалить</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($drivers && count($drivers)>0)
                                            @foreach($drivers as $driver)
                                                <tr id="driver_row_{{$driver->id}}" role="row" style="cursor: pointer;" onclick="getDriverInfo({{ $driver->id }})">
                                                    <td class="sorting_1">{{ $driver->generateAbbreviation }}</td>
                                                    <td>{{ $driver->fio() }}</td>
                                                    <td>{{ $driver->document_number}}</td>
                                                    <td onclick="editDriver({{$driver->id}})" style="text-align: center; vertical-align: middle;"><i class="fa fa-edit" style="font-size: 150%; margin:0 auto;"></i></td>
                                                    <td onclick="dropDriver({{$driver->id}})" style="text-align: center; vertical-align: middle;"><i class="fa fa-trash" style="font-size: 150%; margin:0 auto;"></i></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1">ID</th>
                                            <th rowspan="1">ФИО</th>
                                            <th rowspan="1">Номер водительского удостоверения</th>
                                            <th>Редактировать</th>
                                            <th>Удалить</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
        <!-- /.row -->
        <!-- /.row -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" id="close" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                        <button type="button" id="back" class="btn btn-info" onclick="showDriverInfo()" style="display: none;">Назад</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section ('footer_js')
    @parent
    <script src="/Admin/js/jquery.dataTable.localised.js"></script>
    <script src="/Admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            initTable();
        });
        function initTable(){
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });
        }
        function editDriver(driver_id){
            event.preventDefault();
            event.stopPropagation();
            window.location.href='/web/driver/edit_info/'+driver_id;
        }
        function dropDriver(driver_id){
            event.preventDefault();
            event.stopPropagation();
            $.ajax({
                url:'/web/driver/remove',
                type:'POST',
                data:{'driver_id':driver_id},
                success:function(resp){
                    $('#driver_row_'+driver_id).remove();
                }
            });
        }
        function getDriverInfo(driver_id) {
            $.ajax({
                url:'/web/driver_info/'+driver_id,
                type:'GET',
                success:function(table){
                    $('#myModal').find('.modal-body').html('').append(table);
                    $('#myModal').modal('show');
                    $('.thumbnail').on('click', function(){fullsizePicture(this)});
                }
            })
        }
        function fullsizePicture(picture) {
            var pic=$(picture);
            var src=pic.find('img').attr('src');
            if(pic.length){
                $('#myModal').find('.box').hide();
                $('#fullsize_picture').html('').html('<img data-src="holder.js/100%x200" src="'+src.replace('_0','')+'" style="width:100%;">').show();
                $('#close').hide();
                $('#back').show();
                console.log(src);
            }else{
                console.log(0);
            }

        }
        function showDriverInfo(){
            $('#myModal').find('.box').show();
            $('#fullsize_picture').hide();
            $('#close').show();
            $('#back').hide();
        }
    </script>
@endsection