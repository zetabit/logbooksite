<html>
<head></head>
<body>
<h1>{{$title}}</h1>
<p>Добро пожаловать в <b>Felix Felix</b>.</p>
<p>Для завершения процесса регистрации активируйте ваш аккаунт по ссылке {{ url('/activate?activationCode='.$activation_code) }}</p>
<p>Ваш логин: <b>{{ $login }}</b></p>
<p>Пароль: <b>{{ $password }}</b></p>
</body>
</html>