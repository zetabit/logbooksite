@extends('layouts.app')
@section('header_css')
    @parent
    <link rel="stylesheet" href="/Admin/css/bootstrap-formhelpers.min.css">
@endsection
@section('content')
    <section class="content-header">
        <h1 style="color:#ecf0f5;">Менеджер</h1>
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li><a href="/web/managers"><i class="fa fa-list"></i> Список сотрудников</a></li>
            <li class="active">@if(!isset($info)) Добавление сотрудника @else Редактирование сотрудника @endif</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">@if(!isset($info)) Добавление сотрудника @else Редактирование сотрудника @endif</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-10">
                        <div class="col-md-6">
                            <div style="height:40px;color:grey;font-size:18px;">
                                Общие сведения (1/2)
                            </div>
                            @if(isset($manager))
                                <input type="hidden" id="manager_id" name="manager_id" value="{{ $manager->id }}">
                            @endif
                            <div class="form-group">
                                <label>Фамилия</label>
                                <input name="firstname" id="firstname" type="text" class="form-control" @if(isset($info)) value="{{ $info->firstname }}" @endif/>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>Имя</label>
                                <input name="lastname" id="lastname" type="text" class="form-control" @if(isset($info)) value="{{ $info->lastname }}" @endif/>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>Отчество</label>
                                <input name="patronymic" id="patronymic" type="text" class="form-control" @if(isset($info)) value="{{ $info->patronymic }}" @endif />
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>Контактный телефон</label>
                                <input type="text" name="phone" id="phone" class="form-control bfh-phone" data-format="+d (ddd) ddd-dddd" placeholder="+7 (___)___-____" @if(isset($info)) value="{{ $info->phone }}" @endif>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-md-6" style="border:1px solid lightgrey;">
                            <div class="form-group col-md-6">
                                <label>Должность</label>
                                <select name="role" id="role" class="form-control" required>
                                    <option value="0">Не выбрана</option>
                                    @if(isset($roles)&&count($roles)>0)
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" @if(isset($manager)&&$role==$manager->role) selected @endif>{{ $role->readable_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div id="permission_list">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th>Раздел</th>
                                        <th>Просматривать</th>
                                        <th>Добавлять</th>
                                        <th>Редактировать</th>
                                    </tr>
                                    <tr>
                                        <td>Автомобили</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->cars_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->cars_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->cars_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Сотрудники</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->managers_view)Да@elseНет@endif @else - @endif </td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->managers_add)Да@elseНет@endif @else - @endif </td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->managers_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Водители</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->drivers_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->drivers_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->drivers_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Планировщик задач</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->tasks_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->tasks_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->tasks_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Штрафы</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->fines_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->fines_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->fines_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Уведомления</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->notifications_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->notifications_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->notifications_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Статистика</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->statistic_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->statistic_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->statistic_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Архив</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->archive_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->archive_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->archive_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Путевые листы</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->lists_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->lists_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->lists_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Проводить осмотры</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->checkups_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->checkups_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->checkups_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    <tr>
                                        <td>Создавать акты</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->cars_view)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->cars_add)Да@elseНет@endif @else - @endif</td>
                                        <td>@if(isset($manager)) @if($manager->role->permission->cars_edit)Да@elseНет@endif @else - @endif</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12" style="margin-top:40px;">
                        <button class="btn col-md-2 pull-right" onclick="decline()" style="color:#00c0ef; background-color:white; border-color: #00c0ef; margin-left:40px;">Отменить</button>
                        <button class="btn btn-info col-md-2 pull-right" onclick="next_step()">Продолжить</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer_js')
    @parent
    <script src="/Admin/js/bootstrap-formhelpers-phone.js"></script>
    <script>
        var user_info;
        var next_step_url='@if(!isset($manager)){{ url('/web/manager/add/2') }} @else {{url('/web/manager/edit_data')}}/{{$manager->id}} @endif';
        var fields= {
            firstname:"Пожалуйста, введите имя сотрудника.",
            lastname:"Пожалуйста, введите фамилию сотрудника.",
            patronymic:"Пожалуйста, введите отчество сотрудника.",
            phone:"Пожалуйста, введите контактный номер телефона сотрудника.",
            role:"Пожалуйста, выберите роль сотрудника."
        };

        function next_step() {
            var correct_counter=0;
            for (var field in fields){
                var parent_div=$('#'+field).parent('div');
                var children_span=$(parent_div).find('span.help-block');
                if($('#'+field).val()!==undefined&&$('#'+field).val()!==''){
                    correct_counter++;
                    $(parent_div).removeClass('has-error');
                    $(children_span).html('');
                }else{
                    $(parent_div).addClass('has-error');
                    $(children_span).html(fields[field]);
                }
            }
            if(correct_counter===5){
                user_info={
                    manager_id:$('input[name="manager_id"]').val(),
                    firstname:$('input[name="firstname"]').val(),
                    lastname:$('input[name="lastname"]').val(),
                    patronymic:$('input[name="patronymic"]').val(),
                    phone:$('input[name="phone"]').val().replace(/\D/g, ''),
                    role_id:$('select[name="role"]').val()
                };
                $.ajax({
                    method:'GET',
                    url: next_step_url,
                    success:function(resp){
                        $('section.content').html('').append(resp);
                    }

                });
            }

        }

        function save(){
            if($('input[name="login"]').val()!==''&&$('input[name="login"]').val()!==undefined){
                user_info.login=$('input[name="login"]').val();
                if(user_info.manager_id!==undefined&&user_info.manager_id!=='')
                {
                    if($('input[name="pass"]').val()===$('input[name="pass-confirm"]').val()||$('input[name="pass"]').val()===undefined||$('input[name="pass"]').val()===''){
                        user_info.pass=$('input[name="pass"]').val();
                        $.ajax(
                            {
                                url:'{{ url('/web/manager/save') }}',
                                method:'POST',
                                data:user_info,
                                success:function(msg){
                                    window.location.href='/web/managers';
                                }
                            }
                        );
                    }else if($('input[name="pass"]').val()!=$('input[name="pass-confirm"]').val()){
                        alert("Пароли не совпали.");
                    }
                }else{
                    if($('input[name="pass"]').val()==='' || $('input[name="pass"]').val()===undefined){
                        alert('Пожалуста, введите пароль для пользователя.');
                    }else{
                        if($('input[name="pass"]').val()===$('input[name="pass-confirm"]').val()){
                            user_info.pass=$('input[name="pass"]').val();
                            $.ajax(
                                {
                                    url:'{{ url('/web/manager/save') }}',
                                    method:'POST',
                                    data:user_info,
                                    success:function(msg){
                                        window.location.href='/web/managers';
                                    }
                                }
                            );
                        }else {
                            console.log($('input[name="pass"]').val());
                            console.log($('input[name="pass-confirm"]').val());
                            alert("Пароли не совпали.");
                        }
                    }

                }
            }else {
                alert('Пожалуйста, введите логин сотрудника.');
            }
        }

        function decline() {
            var answer=confirm("Вы уверены что хотите покинуть страницу? Данные не будут сохранены");
            if(answer){
                window.location.href='{{ url('/web/managers') }}'
            }else{

            }
        }

        $('select[name="role"]').change(
            function(){
                $.ajax({
                    url : '{{ url('/web/get_role_permissions') }}/'+$(this).val(),
                    success : function(msg){
                        $('#permission_list').html('').append(msg)
                    }
                });
            }
        );
    </script>
@endsection