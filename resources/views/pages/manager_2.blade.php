<section class="content">
    <div class="row">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">@if(!isset($manager)) Добавление сотрудника @else Редактирование сотрудника @endif</h3>
            </div>
            <div class="box-body">

                <div class="col-md-10">
                    <div class="col-md-6">
                        <div style="height:40px;color:grey;font-size:18px;">
                            Общие сведения (2/2)
                        </div>
                        <div class="form-group">
                            <label>Логин</label>
                            <input name="login" type="text" class="form-control" @if(isset($manager)) value="{{ $manager->email }}" @endif/>
                        </div>
                        <div class="form-group">
                            <label> @if(isset($manager)) Новый пароль @else Пароль @endif</label>
                            <input name="pass" type="password" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Подтверждение пароля</label>
                            <input name="pass-confirm" type="password" class="form-control"/>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12" style="margin-top:40px;">
                    <button class="btn col-md-2 pull-right" onclick="decline()" style="color:#00c0ef; background-color:white; border-color: #00c0ef; margin-left:40px;">Отменить</button>
                    <button class="btn btn-info col-md-2 pull-right" onclick="save()">@if(isset($manager)) Сохранить изменения @else Добавить сотрудника @endif</button>
                </div>
            </div>
        </div>
    </div>
</section>
