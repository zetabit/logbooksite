@extends('layouts.app')
@section('header_css')
    @parent
    <link rel="stylesheet" href="/Admin/plugins/bootstrap-slider/slider.css">
    <link rel="stylesheet" href="/Admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('header_js')
    <script src="/ckeditor/ckeditor.js"></script>
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li><a href="{{ route('contracts') }}"><i class="fa fa-registered"></i> Документы</a></li>
            <li class="active">документ</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" style="margin-top:20px;">
            <h1>Цена создания нового типа договора обсуждается с руководством и затем добавляется в ядро.</h1>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('footer_js')
    @parent

    <script src="/Admin/js/jquery.dataTable.localised.js"></script>
    <script src="/Admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/Admin/plugins/bootstrap-slider/bootstrap-slider.js"></script>
@endsection