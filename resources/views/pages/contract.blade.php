@extends('layouts.app')
@section('header_css')
    @parent
    <link rel="stylesheet" href="/Admin/plugins/bootstrap-slider/slider.css">
    <link rel="stylesheet" href="/Admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('header_js')
    <script src="/ckeditor/ckeditor.js"></script>
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li><a href="{{ route('contracts') }}"><i class="fa fa-registered"></i> Документы</a></li>
            <li class="active">документ</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" style="margin-top:20px;">
            <div class="col-xs-12">
                <div class="box no-border">
                    <div class="box-body">
                        <p class="pull-left" style="font-size: 120%;">
                            Документация <span class="label label-primary">{{ count($contracts) }}</span>
                        </p>
                        <a class="btn btn-info pull-right" href="{{ route('contracts.add') }}">Новый документ</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default no-border collapsed-box">

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- Main row -->

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Договор {{ $contract->generateAbbreviation }} - {{ $contract->typeTemplate }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="ckeditor" class="dataTables_wrapper form-inline dt-bootstrap" contenteditable="true">
                            {!! $contract->data !!}
                        </div>

                        <div>
                            <span>Используемые константы:</span>
                            @foreach($contract->setDataTemplate() as $key => $val)
                                @if($loop->index > 0), @endif
                                {{ $key }}
                            @endforeach
                        </div>

                        <button class="btn btn-default" id="save" data-type="{{ $contract->type }}">
                            <span>
                                <i class="fa fa-refresh"></i> Сохранить
                            </span>
                        </button>
                        <button class="btn btn-default" id="print" data-type="{{ $contract->type }}" {{ strlen($contract->generated) == 0 ? ' disabled ' : '' }}>
                            <span>
                                <i class="fa fa-print"></i> Cмотреть / Распечатать
                            </span>
                        </button>
                        <div id="status"></div>
                    </div>
                    <!-- /.box-body -->
                </div>


                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">История</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                <i class="fa fa-plus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="height: 400px; overflow-y: scroll">
                        <table class="table">
                            <tbody><tr>
                                <th style="width: 10px">#</th>
                                <th style="width: 10px">шаблон</th>
                                <th>тип</th>
                                <th>дата</th>
                                <th style="width: 40px">ссылка</th>
                            </tr>
                            @foreach ($contracts as $contractItem)
                            <tr>
                                <td>{{ $contractItem->generateAbbreviation }}</td>
                                <td>{{ $contractItem->is_template ? 'да' : 'нет' }}</td>
                                <td>{{ $contractItem->typeTemplate }}</td>
                                <td>{{ $contractItem->date }}</td>
                                <td><a href="{{ route('contracts.show', ['id' => $contractItem->id]) }}">открыть</a> </td>
                            </tr>
                            @endforeach
                            </tbody></table>
                    </div>
                </div>

            </div>
                <!-- /.box -->
        </div>
        <!-- /.row -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-lg" style="width: 80%;" role="document">
                <div class="modal-content">
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('footer_js')
    @parent

    <script src="/Admin/js/jquery.dataTable.localised.js"></script>
    <script src="/Admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/Admin/plugins/bootstrap-slider/bootstrap-slider.js"></script>
    <script>
      CKEDITOR.disableAutoInline = true;
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.inline( 'ckeditor' );
      $('#save').on('click', function(ev) {
        var data = CKEDITOR.instances.ckeditor.getData();
        var id = '{{ $contract->id }}';

        $(this).attr('disabled', false);

        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: '{{ route('contracts.save') }}',
          data: {data: data, id: id},
          type: 'POST',
          success: function (response) {
            if (typeof response.result == 'undefined' || response.result != 'ok') {
              $('#status').html("ошибка");
              btn.attr('disabled', false);
              return;
            }

            if (response.result == 'ok') {
              window.location.href = "{{ route('contracts.show', ['id' => '#']) }}".replace('#', response.id);
              btn.attr('disabled', true);
            }
          },
          error: function (response) {
            $('#status').html("ошибка");
            btn.attr('disabled', false);
          }
        });
      });

      $('#print').on('click', function(ev) {
        window.location.href = '{{ route('contracts.print', ['id' => $contract->id]) }}';
      });
    </script>
@endsection