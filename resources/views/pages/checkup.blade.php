@extends('layouts.app')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Осмотр авто
        </h1>
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li class="active">Осмотр авто</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ $checkup->typeText }} #{{ $checkup->generateAbbreviation }} машины {{ $checkup->car->number }} [{{ $checkup->updated_at }}] (Менеджер - {{$checkup->user->info->fio()}}; Водитель - {{ $checkup->driver->fio() }})</h3>
                        <a href="/web/cars?car_id={{$checkup->car->id}}" class="btn col-md-1 btn-info pull-right">Назад</a>
                </div>
                        <div class="row">

                            <div class="col-lg-12">
                                <h1>Части авто:</h1>
                                @foreach($checkup->parts as $part)
                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                        <a style="border-width: 5px; @if(count($part->notifications)>0) border-color: red; @else border-color: green; @endif" class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="{{ $part->carPart->name }}" data-caption="{{ $part->picture->alt or '' }}" data-image="{{ $part->picture->path }}" data-target="#image-gallery">
                                            <img class="img-responsive" src="{{ $part->picture->path_thumb }}" alt="Short alt text" style="width: 100%">
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">

                            <div class="col-lg-12">
                                <h1>Общий вид:</h1>
                                @foreach($checkup->carPictures as $picture)
                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                        <a style="@if(strlen($picture->alt)) border-color: red; @else border-color: green; @endif"
                                           class="thumbnail" href="#" data-image-id="" data-toggle="modal"
                                           data-title=""
                                           data-caption="{{ $picture->alt or '' }}"
                                           data-image="{{ $picture->path }}"
                                           data-target="#image-gallery"
                                        >
                                            <img class="img-responsive" src="{{ $picture->path_thumb }}" alt="Short alt text" style="width: 100%">
                                        </a>
                                    </div>
                                @endforeach
                            </div>


                        </div>
                    </div>
                    @if(isset($completeness))
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1>Комплект:</h1>
                                    <button class="btn btn-info col-md-2 margin" @if(!$completeness->back_wheel) disabled @endif>Запасное колесо</button>
                                    <button class="btn btn-info col-md-2 margin" @if(!$completeness->fire_extinguisher) disabled @endif>Огнетушитель</button>
                                    <button class="btn btn-info col-md-2 margin" @if(!$completeness->jack) disabled @endif>Домкрат</button>
                                    <button class="btn btn-info col-md-2 margin" @if(!$completeness->first_aid_kit) disabled @endif>Аптечка</button>
                                    <button class="btn btn-info col-md-2 margin" @if(!$completeness->towing) disabled @endif>Буксировочное крепление</button>
                                    <button class="btn btn-info col-md-2 margin" @if(!$completeness->warning_triangle) disabled @endif>Знак аварийной остановки</button>
                                    <button class="btn btn-info col-md-2 margin" @if(!$completeness->radio) disabled @endif>Магнитола</button>
                                    <button class="btn btn-info col-md-2 margin" @if(!$completeness->battery) disabled @endif>Аккумулятор</button>
                                    <button class="btn btn-info col-md-2 margin" @if(!$completeness->baloon_wrench) disabled @endif>Балонный ключ</button>
                                </div>
                            </div>
                        </div>
                    @endif


                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('footer')
    @parent
    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="image-gallery-title"></h4>
                </div>
                <div class="modal-body">
                    <img id="image-gallery-image" class="img-responsive" src="">
                </div>
                <div class="modal-footer">
                    <div class="col-md-8" id="image-gallery-caption" style="color:red; margin-top:10px; margin-bottom:10px; text-align: left;">
                        This text will be overwritten by jQuery
                    </div>
                    <div class="row col-md-12">

                    <div class="col-md-3" style="display: inline-block;">
                        <button type="button" style="width:100%" class="btn btn-primary" id="show-previous-image">Назад</button>
                    </div>

                    <div class="col-md-3 pull-right" style="display: inline-block;">
                        <button type="button" style="width:100%" id="show-next-image" class="btn btn-default">Дальше</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_js')
    @parent
    <script>
        $(document).ready(function(){

            loadGallery(true, 'a.thumbnail');

            //This function disables buttons when needed
            function disableButtons(counter_max, counter_current){
                $('#show-previous-image, #show-next-image').show();
                if(counter_max == counter_current){
                    $('#show-next-image').hide();
                } else if (counter_current == 1){
                    $('#show-previous-image').hide();
                }
            }

            /**
             *
             * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
             * @param setClickAttr  Sets the attribute for the click handler.
             */

            function loadGallery(setIDs, setClickAttr){
                var current_image,
                    selector,
                    counter = 0;

                $('#show-next-image, #show-previous-image').click(function(){
                    if($(this).attr('id') == 'show-previous-image'){
                        current_image--;
                    } else {
                        current_image++;
                    }

                    selector = $('[data-image-id="' + current_image + '"]');
                    updateGallery(selector);
                });

                function updateGallery(selector) {
                    var $sel = selector;
                    current_image = $sel.data('image-id');
                    $('#image-gallery-caption').text($sel.data('caption'));
                    $('#image-gallery-title').text($sel.data('title'));
                    $('#image-gallery-image').attr('src', $sel.data('image'));
                    disableButtons(counter, $sel.data('image-id'));
                }

                if(setIDs == true){
                    $('[data-image-id]').each(function(){
                        counter++;
                        $(this).attr('data-image-id',counter);
                    });
                }
                $(setClickAttr).on('click',function(){
                    updateGallery($(this));
                });
            }
        });
    </script>
@endsection