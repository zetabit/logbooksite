@extends('layouts.app')
@section('header_css')
    @parent
    <link rel="stylesheet" href="/Admin/css/bootstrap-formhelpers.min.css">
    <link rel="stylesheet" href="/Admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
@endsection
@section('content')

    <section class="content-header">
        <h1 style="color:#ecf0f5;">Водитель</h1>
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li><a href="/web/drivers"><i class="fa fa-list"></i> Список водителей</a></li>
            <li class="active">@if(!isset($driver)) Добавление водителя @else Редактирование водителя @endif</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">@if(!isset($driver)) Добавление сотрудника @else Редактирование сотрудника @endif</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-10">
                        <div style="height:40px;color:grey;font-size:18px;" class="col-md-12">
                            Общие сведения (1/2)
                        </div>
                        <form name="user_info" enctype="multipart/form-data" method="POST" action="{{ url('/web/driver/save') }}">
                            <div class="col-md-6">
                                @if(isset($driver))
                                    <input type="hidden" name="driver_id" value="{{ $driver->id }}">
                                @endif
                                <div class="form-group">
                                    <label>Фамилия</label>
                                    <input name="firstname" id="firstname" type="text" class="form-control" @if(isset($driver)) value="{{ $driver->firstname }}" @endif/>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Имя</label>
                                    <input name="lastname" id="lastname" type="text" class="form-control" @if(isset($driver)) value="{{ $driver->lastname }}" @endif/>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Отчество</label>
                                    <input name="patronymic" id="patronymic" type="text" class="form-control" @if(isset($driver)) value="{{ $driver->patronymic }}" @endif />
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Контактный телефон</label>
                                    <input type="text" name="main_phone" id="main_phone" class="form-control bfh-phone" data-format="+d (ddd) ddd-dddd" placeholder="+7 (___)___-____" @if(isset($driver)) value="{{ $driver->main_phone }}" @endif>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Дополнительный контактный телефон</label>
                                    <input type="text" name="additional_phone" id="additional_phone" class="form-control bfh-phone" data-format="+d (ddd) ddd-dddd" placeholder="+7 (___)___-____" @if(isset($driver)) value="{{ $driver->additional_phone }}" @endif>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Место регистрации</label>
                                    <input name="registration_address" id="registration_address" type="text" class="form-control" @if(isset($driver)) value="{{ $driver->registration_address }}" @endif/>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Номер водительского удостоверения</label>
                                    <input name="document_number" id="document_number" type="text" class="form-control" @if(isset($driver)) value="{{ $driver->document_number }}" @endif />
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Дата выдачи водительского удостоверения</label>
                                    <input type="text" name="document_date" id="document_date" class="form-control datepicker" data-date-format="dd-mmyyyy" @if(isset($driver)) value="{{ date('d-m-Y', strtotime($driver->document_date)) }}" @endif>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12" style="margin-top:40px;">
                        <button class="btn col-md-2 pull-right" onclick="decline()" style="color:#00c0ef; background-color:white; border-color: #00c0ef; margin-left:40px;">Отменить</button>
                        <button class="btn btn-info col-md-2 pull-right" onclick="next_step()">Продолжить</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0">
                                <span class="sr-only"><span id="current"></span>/<span id="total"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('footer_js')
    @parent
    <script src="/Admin/js/bootstrap-formhelpers-phone.js"></script>
    <script src="/Admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script>
        var user_info;
        var fields= {
            firstname:"Пожалуйста, введите имя сотрудника.",
            lastname:"Пожалуйста, введите фамилию сотрудника.",
            patronymic:"Пожалуйста, введите отчество сотрудника.",
            main_phone:"Пожалуйста, введите контактный номер телефона сотрудника.",
            registration_address:"Пожалуйста, введите адрес регистрации водителя.",
            document_number:"Пожалуйста, введите номер водительского удостоверения.",
            document_date:"Пожалуйста, введите дату выдачи водительского удостоверения."
        };
        var next_step_url='@if(!isset($driver)){{ url('/web/driver/add_driver_images') }} @else {{url('/web/driver/edit_data')}}/{{$driver->id}} @endif';
        $(function () {
            $.fn.datepicker.dates['ru'] = {
                days: ["Воскресение", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
                daysShort: ["Вос", "Пон", "Вто", "Сре", "Чет", "Пят", "Суб"],
                daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
                today: "Сегодня",
                clear: "Очистить",
                format: "dd-mm-yyyy",
                titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
                weekStart: 0
            };
            $('#document_date').datepicker({
                format:'dd-mm-yyyy',
                language: 'ru',
                startDate: '-100y',
                autoclose:true
            });
        });
        function next_step() {
            event.preventDefault();
            var correct_counter=0;
            for (var field in fields){
                var parent_div=$('#'+field).parent('div');
                var children_span=$(parent_div).find('span.help-block');
                if($('#'+field).val()!==undefined&&$('#'+field).val()!==''){
                    correct_counter++;
                    $(parent_div).removeClass('has-error');
                    $(children_span).html('');
                }else{
                    $(parent_div).addClass('has-error');
                    $(children_span).html(fields[field]);
                }
            }
            if(correct_counter===7){
                $.ajax({
                    method:'POST',
                    url: next_step_url,
                    data:$('form[name="user_info"]').serialize(),
                    success:function(resp){
                        $('section.content').children().hide();
                        $('section.content').append(resp)
                        var max_height=0;
                        $('.thumbnail').each(function(){
                            if($(this).height()>max_height){
                                max_height=parseFloat($(this).height());
                            }
                        });
                        console.log(max_height);
                        if(max_height>0){
                            $('.thumbnail').css({'height':max_height});
                        }
                    }

                });

            }


        }
        function save(){
            event.preventDefault();
            $("form[name='main_pictures']").submit();
        }
        function decline() {
            event.preventDefault();
            var answer=confirm("Вы уверены что хотите покинуть страницу? Данные не будут сохранены");
            if(answer){
                window.location.href='{{ url('/web/drivers') }}'
            }else{

            }
        }
        function previewMainImages(target) {
            if (target.files) $.each(target.files, readAndPreview);
            function readAndPreview(i, file) {
                if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
                    return alert(file.name +" is not an image");
                } // else...
                var reader = new FileReader();
                $(reader).on("load", function() {
                    $(target).closest('div').css(
                        {
                            'background-image':'url("' + this.result + '")',
                            'background-repeat':'no-repeat',
                            'background-size':'contain',
                            'background-position':'center'
                        }).find('div').hide();
                });
                reader.readAsDataURL(file);
            }

        }
        function previewAdditionalImages(){
            event.preventDefault();
            var additional_form=$("#additional_pictures");
                var params   = additional_form.serializeArray(),
                    files = additional_form.find('#additional_pics')[0].files;
            $('#myModal').modal('show');
            $('#total').html(files.length);
            function sendFiles(counter) {
                var formData = new FormData();
                $.each(params, function(j, val) {
                    formData.append(val.name, val.value);
                });
                formData.append('additional_pic', files[counter]);
                $.ajax({
                    type: $(additional_form).attr('method'),
                    url: $(additional_form).attr('action'),
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function(data)
                    {
                        $('#current').html(++counter);
                        $('.progress-bar-primary').css({'width':(counter*100/files.length)+'%'});
                        $('#additional_images').append(data);
                        if(counter<files.length){
                            sendFiles(counter);
                        }else{
                            $('#myModal').modal('hide');
                        }
                    }
                });
            }
            sendFiles(0);
        }
        function chooseFile(target){
            event.preventDefault();
            $(target).closest('div').find('input[type="file"]').trigger('click');
        }
        function drop_image(number){
            event.preventDefault();
            if (confirm("Вы уверены, что хотите убрать фото из списка?")) {
                $.ajax({
                    method:'POST',
                    url: '{{url('/web/driver/remove_additional_image')}}',
                    data:{'image_id':number},
                    success:function(resp){
                        $('#image_preview_'+number).remove();
                    }

                });

            } else {
                console.log('done');
            }

        }
        function set_description(number){
            event.preventDefault();
            var descr=$('input[name="description_'+number+'"]').val();
            $.ajax({
                    method:'POST',
                    url: '{{url('/web/driver/set_additional_image_description')}}',
                    data: {'image_id':number,'description':descr},
                    success:function(resp){
                        console.log('done');
                    }

                });

        }
        function show_fullsize(el){
            var src=$(el).find('img').attr('src');
            var description=$(el).find('p').text();
            console.log(description);
            $('#message').text(description);
            $('#driver_image').attr('src',src);
            $('#driverModal').modal('show');
        }
    </script>
@endsection