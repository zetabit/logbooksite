@extends('layouts.app')
@section('header_css')
    @parent
    <link rel="stylesheet" href="/Admin/plugins/bootstrap-slider/slider.css">
    <link rel="stylesheet" href="/Admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('header_js')
    <script src="/ckeditor/ckeditor.js"></script>
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li class="active">документ</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" style="margin-top:20px;">
            <div class="col-xs-12">
                <div class="box no-border">
                    <div class="box-body">
                        <p class="pull-left" style="font-size: 120%;">
                            Документация <span class="label label-primary">{{ count($contracts) }}</span>
                        </p>
                        <a class="btn btn-info pull-right" href="{{ route('contracts.add') }}">Новый документ</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-default no-border collapsed-box">

                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- Main row -->

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Документация</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc">ID</th>
                                <th class="sorting">Тип</th>
                                <th class="sorting">Дата</th>
                                <th>Смотреть</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($contracts && count($contracts)>0)
                                @foreach($contracts as $contract)
                                    <tr role="row" style="cursor: pointer;">
                                        <td>{{ $contract->generateAbbreviation }}</td>
                                        <td>{{ $contract->typeTemplate }}</td>
                                        <td>{{ $contract->updated_at }}</td>
                                        <td onclick=" window.location.href = '{{ route('contracts.show', ['id' => $contract->id]) }}' " style="text-align: center; vertical-align: middle;"><i class="fa fa-edit" style="font-size: 150%; margin:0 auto;"></i></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th rowspan="1">ID</th>
                                <th rowspan="1">Тип</th>
                                <th rowspan="1">Дата</th>
                                <th rowspan="1">Смотреть</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
                <!-- /.box -->
        </div>
        <!-- /.row -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-lg" style="width: 80%;" role="document">
                <div class="modal-content">
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('footer_js')
    @parent

    <script src="/Admin/js/jquery.dataTable.localised.js"></script>
    <script src="/Admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/Admin/plugins/bootstrap-slider/bootstrap-slider.js"></script>
    <script>
      CKEDITOR.disableAutoInline = true;
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.inline( 'ckeditor' );
    </script>
@endsection