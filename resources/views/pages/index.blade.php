@extends('layouts.app')

@section('content')
    <style>
        .no-right-padding{
            padding-right:0px;
        }
    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="color:#3c8dbc;">
            Бортовой журнал
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-lg-2 col-xs-4 no-right-padding">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <span>Всего автомобилей</span>
                        <span class="info-box-number">{{ $cars_total }}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-xs-2 no-right-padding">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <span>На линии</span>
                        <span class="info-box-number"> {{ $cars_on_line }}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-xs-2 no-right-padding">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <span>В ремонте</span>
                        <span class="info-box-number">{{ $cars_on_service }}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-1 col-xs-2 no-right-padding">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <span>В простое</span>
                        <span class="info-box-number">{{ $cars_on_hold }}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-4 no-right-padding">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <span>Процент загрузки</span>
                        <span class="info-box-number">@if($cars_total>0) {{ round($cars_on_line*100/$cars_total)}} @else 0 @endif
                            %</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-4 no-right-padding">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <span>Задолженность по штрафам</span>
                        <span class="info-box-number">0 р.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-4 no-right-padding">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <span>Проведено осмотров сегодня</span>
                        <span class="info-box-number">@if(isset($today_checkups)) {{ $today_checkups }} @else 0 @endif</span>
                    </div>
                </div>
            </div>


        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-md-6">
                <!-- LINE CHART -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Машины на линии</h3>

                        <div class="box-tools pull-right">
                            <button type="button" id="week-switcher" class="btn btn-sm btn-info switcher" onclick="switchLineChart('week')">Неделя</button>
                            <button type="button" id="month-switcher" class="btn btn-sm btn-default switcher" onclick="switchLineChart('month')">Месяц</button>
                            <button type="button" id="year-switcher" class="btn btn-sm btn-default switcher" onclick="switchLineChart('year')">Год</button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div id="lineChartCanvas" class="chart">
                            <canvas id="lineChart" style="height: 276px; width: 594px;" width="742" height="312"></canvas>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-6">

                <!-- Donut chart -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>

                        <h3 class="box-title">Диаграмма занятости автомобилей</h3>
                    </div>
                    <div class="box-body">
                        <div id="donut-chart" style="height: 300px; padding: 0px; position: relative;">
                            <canvas class="flot-base"
                                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 594.6px; height: 300px;"
                                    width="743" height="375"></canvas>
                            <canvas class="flot-overlay"
                                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 594.6px; height: 300px;"
                                    width="743" height="375"></canvas>
                            <span class="pieLabel" id="pieLabel0"
                                  style="position: absolute; top: 70.6px; left: 355.9px;"><div
                                        style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">@if($cars_total>0) {{ round($cars_on_service*100/($cars_total))}} @else 0 @endif</div></span>
                            <span class="pieLabel" id="pieLabel1"
                                  style="position: absolute; top: 210.6px; left: 333.9px;"><div
                                        style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">@if($cars_total>0) {{ round($cars_on_hold*100/($cars_total))}} @else 0 @endif</div></span>
                            <span class="pieLabel" id="pieLabel2"
                                  style="position: absolute; top: 129.6px; left: 174.9px;"><div
                                        style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">@if($cars_total>0) {{ round($cars_on_line*100/($cars_total))}} @else 0 @endif</div></span>
                        </div>
                    </div>
                    <!-- /.box-body-->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="background-color:white;">
                <div class="box collapsed-box box-solid" style="background-color:whitesmoke;">
                    <div class="box-header no-border">
                        <div>
                            <h3>Журнал событий</h3>
                        </div>
                    </div>
                </div>
                @if(isset($notifications)&&count($notifications))
                    @foreach($notifications as $notification)
                        {!! $notification->render('mainpage') !!}
                    @endforeach
                @endif
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('footer_js')
    @parent
    <!-- ChartJS -->
    <script src="/Admin/bower_components/chart.js/Chart.js"></script>
    <!-- FLOT CHARTS -->
    <script src="/Admin/js/jquery.flot.adapted.js"></script>
    <script src="/Admin/bower_components/Flot/jquery.flot.pie.js"></script>
    <script>
        $(function () {
            setEqualHeight();
            initLineChart('week');
            var donutData = [
                {
                    label: 'В ремонте',
                    data: @if($cars_total>0) {{ round($cars_on_service*100/($cars_total))}} @else 0 @endif,
                    color: 'darkgray'
                },
                {
                    label: 'В простое',
                    data:  @if($cars_total>0)  {{ round($cars_on_hold*100/($cars_total))}} @else 0 @endif,
                    color: 'lightgray'
                },
                {
                    label: 'На линии',
                    data: @if($cars_total>0) {{ round($cars_on_line*100/($cars_total))}} @else 0 @endif ,
                    color: '#00c0ef'
                }
            ];
            $.plot('#donut-chart', donutData, {
                series: {
                    pie: {
                        show: true,
                        radius: 1,
                        innerRadius: 0,
                        label: {
                            show: true,
                            radius: 2 / 3,
                            formatter: labelFormatter,
                            threshold: 0.1
                        }

                    }
                },
                legend: {
                    show: true
                }
            })
            /*
             * END DONUT CHART
             */
        });
        function initLineChart(interval){

            var labels=[];
            var data=[];
            if(interval==='month'){
                labels=[<?php echo '"'.implode('","', $month_labels).'"' ?>];
                data=[<?php echo '"'.implode('","', $month_data).'"' ?>];
            }else if(interval==='year'){
                labels=[<?php echo '"'.implode('","', $year_labels).'"' ?>];
                data=[<?php echo '"'.implode('","', $year_data).'"' ?>];
            }else{
                labels=[<?php echo '"'.implode('","', $week_labels).'"' ?>];
                data=[<?php echo '"'.implode('","', $week_data).'"' ?>];
            }
            var areaChartData = {
                labels: labels,
                datasets: [
                    {
                        label: '',
                        fillColor: 'rgba(210, 214, 222, 1)',
                        strokeColor: 'rgba(210, 214, 222, 1)',
                        pointColor: 'rgba(210, 214, 222, 1)',
                        pointStrokeColor: '#c1c7d1',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(220,220,220,1)',
                        data: data
                    }
                ]
            };

            var areaChartOptions = {
                //Boolean - If we should show the scale at all
                showScale: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: false,
                //String - Colour of the grid lines
                scaleGridLineColor: 'rgba(0,0,0,.05)',
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - Whether the line is curved between points
                bezierCurve: true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.3,
                //Boolean - Whether to show a dot for each point
                pointDot: true,
                //Number - Radius of each point dot in pixels
                pointDotRadius: 4,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 20,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,
                //Boolean - Whether to fill the dataset with a color
                datasetFill: true,
                //String - A legend template
                legendTemplate: '',
                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true
            };

            //-------------
            //- LINE CHART -
            //--------------

            var lineChart = document.getElementById('lineChartCanvas');
            lineChart.innerHTML = '&nbsp;';
            $('#lineChartCanvas').append('<canvas id="lineChart" style="height: 276px; width: 594px;" width="742" height="312"></canvas>');

            var lineChartCanvas = $('#lineChart').get(0).getContext('2d');
            var lineChart = new Chart(lineChartCanvas);
            var lineChartOptions = areaChartOptions;
            lineChartOptions.datasetFill = false;
            lineChart.Line(areaChartData, lineChartOptions);
        }
        function labelFormatter(label, series) {
            return '<div style="font-size:23px; text-align:center; padding:2px; color: #fff; font-weight: 600;">' + Math.round(series.percent) + '%</div>'
        }
        function switchLineChart(interval){
            $('.switcher').removeClass('btn-info').addClass('btn-default');
            $('#'+ interval +'-switcher').removeClass('btn-default').addClass('btn-info');
            initLineChart(interval);
        }
        function setEqualHeight(){
            var max_height=0;
            $('.no-right-padding').each(function(){
                if($(this).find('.bg-aqua').height()>max_height){
                    max_height=parseFloat($(this).css('height'));
                }
            });
            if(max_height>0){
                $('.no-right-padding').find('.bg-aqua').css({'height':max_height});
            }
        }
    </script>

@endsection