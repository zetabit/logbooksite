<table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
    <thead>
    <tr role="row">
        <th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Марка</th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Модель</th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Гос Номер</th>
        <th>Цвет</th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Дата последнего осмотра</th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Пробег при последнем осмотре</th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Следующее ТО</th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Статус</th>
        <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Водитель</th>
    </tr>
    </thead>
    <tbody>
    @if($cars && count($cars)>0)
        @foreach($cars as $car)
            <tr role="row" style="cursor: pointer;" onclick="window.location.href=('/web/car/{{ $car->id }}')">
                <td class="sorting_1">{{ $car->generateAbbreviation }}</td>
                <td>{{ $car->getBrandName() }}</td>
                <td>{{ $car->getModelName() }}</td>
                <td>{{ $car->number }}</td>
                <td>
                    {{ $car->color()->first()->name }}
                </td>
                <td>@if($car->getLastCheckup()) {{ $car->getLastCheckup()->created_at }} @else Не проводился @endif</td>
                <td>{{ $car->run_total}}</td>
                <td>{{ $car->run_closest}}</td>
                <td><span class="label @if($car->status==App\Models\Car::STATUS_ON_HOLD) label-default @elseif($car->status==App\Models\Car::STATUS_ON_LINE) label-info @elseif($car->status==App\Models\Car::STATUS_ON_SERVICE) label-default bg-navy @endif">{{ $car->getCarStatus()}}</span></td>
                <td>статус</td>
            </tr>
        @endforeach
    @endif
    </tbody>
    <tfoot>
    <tr>
        <th rowspan="1">ID</th>
        <th rowspan="1">Марка</th>
        <th rowspan="1">Модель</th>
        <th rowspan="1">Гос Номер</th>
        <th rowspan="1">Цвет</th>
        <th colspan="1">Дата последнего осмотра</th>
        <th rowspan="1">Пробег при последнем осмотре</th>
        <th rowspan="1">Следующее ТО</th>
        <th rowspan="1">Статус</th>
        <th rowspan="1">Водитель</th>
    </tr>
    </tfoot>
</table>