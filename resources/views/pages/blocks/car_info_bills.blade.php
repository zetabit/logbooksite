<div class="box no-border">
    <div class="box-body">
        <div class="col-md-12">

            <div class="box-header">
                <h3 class="box-title">Штрафы</h3>
            </div>

            <div id="" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="1488" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting">Дата</th>
                                <th class="sorting">Номер машины</th>
                                <th class="sorting">Номер постановления, статья</th>
                                <th class="sorting">Нарушитель</th>
                                <th class="sorting">Статус</th>
                                <th class="sorting">Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (is_array($car->bills))
                                @foreach($car->bills as $bill)
                                    <tr role="row" style="cursor: pointer;">
                                        <td>{{ $bill['from'] }}</td>
                                        <td>{{ $bill['ts'] }}</td>
                                        <td>[#{{ $bill['number'] }}] {{ $bill['StAP'] }}</td>
                                        <td>{{ $bill['violator'] }}</td>
                                        <td>@if($bill['status'] == 'paid') оплачен @else Не оплачен @endif</td>
                                        <td>{{ $bill['price_no_discount'] }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <p style="color:red">{{ $car->bills }} </p>
                                <a href="{{ route('bills_reauth') }}">Переавторизоваться</a><br>
                                <a href="{{ route('bills', ['force' => 1]) }}">Обновить принудительно</a>
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th rowspan="1">Дата</th>
                                <th rowspan="1">Номер машины</th>
                                <th rowspan="1">Название акта</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>