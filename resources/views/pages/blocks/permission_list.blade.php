<table class="table">
    <tbody>
    <tr>
        <th>Раздел</th>
        <th>Просматривать</th>
        <th>Добавлять</th>
        <th>Редактировать</th>
    </tr>
    <tr>
        <td>Автомобили</td>
        <td>@if(isset($permissions)) @if($permissions->cars_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->cars_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->cars_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Сотрудники</td>
        <td>@if(isset($permissions)) @if($permissions->managers_view)Да@elseНет@endif @else - @endif </td>
        <td>@if(isset($permissions)) @if($permissions->managers_add)Да@elseНет@endif @else - @endif </td>
        <td>@if(isset($permissions)) @if($permissions->managers_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Водители</td>
        <td>@if(isset($permissions)) @if($permissions->drivers_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->drivers_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->drivers_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Планировщик задач</td>
        <td>@if(isset($permissions)) @if($permissions->tasks_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->tasks_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->tasks_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Штрафы</td>
        <td>@if(isset($permissions)) @if($permissions->fines_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->fines_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->fines_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Уведомления</td>
        <td>@if(isset($permissions)) @if($permissions->notifications_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->notifications_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->notifications_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Статистика</td>
        <td>@if(isset($permissions)) @if($permissions->statistic_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->statistic_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->statistic_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Архив</td>
        <td>@if(isset($permissions)) @if($permissions->archive_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->archive_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->archive_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Путевые листы</td>
        <td>@if(isset($permissions)) @if($permissions->lists_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->lists_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->lists_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Проводить осмотры</td>
        <td>@if(isset($permissions)) @if($permissions->checkups_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->checkups_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->checkups_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    <tr>
        <td>Создавать акты</td>
        <td>@if(isset($permissions)) @if($permissions->cars_view)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->cars_add)Да@elseНет@endif @else - @endif</td>
        <td>@if(isset($permissions)) @if($permissions->cars_edit)Да@elseНет@endif @else - @endif</td>
    </tr>
    </tbody>
</table>