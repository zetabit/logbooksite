<div class="box no-border">
    <div class="box-body">
        <div >

            <div class="box-header">
                <h3 class="box-title">История</h3>
            </div>

            <div id="" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="history" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">ID</th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Дата Время</th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Номер машины</th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Сотрудник</th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Водитель</th>
                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Название акта</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($acts))
                                @foreach($acts as $act)
                                    <tr role="row" style="cursor: pointer;" onclick="window.location.href='/web/checkup/{{$act->id}}'">
                                        <td>{{$act->generateAbbreviation}}</td>
                                        <td>{{$act->created_at->format('d-m-Y H:i:s')}}</td>
                                        <td>{{$act->car->number}}</td>
                                        <td>{{$act->user->info->fio()}}</td>
                                        <td>{{$act->driver ? $act->driver->fio() : ''}}</td>
                                        <td>
                                            {{ $act->typeText }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                            <tfoot>
                            <tr>
                                <th rowspan="1">ID</th>
                                <th rowspan="1">Дата Время</th>
                                <th rowspan="1">Номер машины</th>
                                <th rowspan="1">Сотрудник</th>
                                <th rowspan="1">Водитель</th>
                                <th rowspan="1">Название акта</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
  $('#history').DataTable({
    'paging': true,
    'lengthChange': false,
    'searching': false,
    'ordering': true,
    'info': true,
    'autoWidth': false
  });
</script>