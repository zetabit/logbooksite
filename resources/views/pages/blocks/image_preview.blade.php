<div class="col-md-10" id="image_preview_{{$additionalPic->id}}">
    <div class="col-md-4">
        <img src="{{$additionalPic->imagePath.'/'.$additionalPic->slug.'.'.$additionalPic->extension}}" style="width: 100%;">
    </div>
    <div class="col-md-8 form-group">
        <div class="col-md-12">
            <label>Название документа
                <input type="text" name="description_{{$additionalPic->id}}" class="form-control col-md-12">
            </label>
        </div>
        <div class="col-md-8">
            <button class="btn col-md-5" style="color:#00c0ef; background-color:white; border-color: #00c0ef;" onclick="drop_image({{$additionalPic->id}})">Отменить добавление</button>
            <button class="btn btn-info col-md-5" style="margin-left: 5%;" onclick="set_description({{$additionalPic->id}})">Продолжить</button>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<hr align="center" width="90%" size="2" color="#dddddd" />