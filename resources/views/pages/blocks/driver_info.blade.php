<div class="box no-border">
    <div class="box-body">
        <div class="form-group col-md-12"><label>Водитель:<input type="text" class="form-control" style="width:200%;" value="{{ $driver->lastname }} {{ $driver->firstname }} {{ $driver->patronymic }}"></label></div>
        <div class="form-group col-md-12"><label>Водительское удостоверение:<input type="text" style="width:200%;" class="form-control" value="{{ $driver->document_number }} от ({{ $driver->document_date }})"></label></div>
        <div class="row">
            @if($front_pic)
                <div class="col-sm-6 col-md-6" >
                    <div class="thumbnail">
                        <img data-src="holder.js/100%x200" style="height: 200px; display: block;" src="{{$front_pic->imagePath.'/'.$front_pic->slug.'_0.'.$front_pic->extension}}" data-holder-rendered="true">
                        <div class="caption">
                            <p>{{$front_pic->description}}</p>
                        </div>
                    </div>
                </div>
            @endif
            @if($back_pic)
                    <div class="col-sm-6 col-md-6" >
                        <div class="thumbnail">
                            <img data-src="holder.js/100%x200" style="height: 200px; display: block;" src="{{$back_pic->imagePath.'/'.$back_pic->slug.'_0.'.$back_pic->extension}}" data-holder-rendered="true">
                            <div class="caption">
                                <p>{{$back_pic->description}}</p>
                            </div>
                        </div>
                    </div>
            @endif
        </div>
        <div class="form-group col-md-12"><label>Телефон:<input type="text" class="form-control" style="width:200%;" value="+{{ $driver->main_phone }}"></label></div>
        @if(!empty($driver->additional_phone))
            <div class="form-group col-md-12"><label>Доп. телефон:<input type="text" class="form-control" style="width:200%;" value="+{{ $driver->additional_phone }}"></label></div>
        @endif
        <div class="form-group col-md-12"><label>Место регистрации:<input type="text" class="form-control" style="width:200%;" value="{{ $driver->registration_address }}"></label></div>

    @if($additional_pics)
            <div class="row">
                @foreach($additional_pics as $picture)
                    <div class="col-sm-6 col-md-6" >
                        <div class="thumbnail">
                            <img data-src="holder.js/100%x200" style="height: 200px; display: block;" src="{{$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension}}" data-holder-rendered="true">
                            <div class="caption">
                                <p>{{$picture->description}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
    </div>

</div>
<div id="fullsize_picture" style="width:100%;">

</div>