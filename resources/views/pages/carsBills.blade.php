@extends('layouts.app')
@section('header_css')
    @parent
    <link rel="stylesheet" href="/Admin/plugins/bootstrap-slider/slider.css">
    <link rel="stylesheet" href="/Admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li class="active">Штрафы</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" style="margin-top:20px;">
            <div class="col-xs-12">
                <div class="box no-border">
                    <div class="box-body">
                        <p class="pull-left" style="font-size: 120%;">
                            Штрафы <span class="label label-primary">@if($bills && is_array($bills)) {{count($bills)}} @endif</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Список автомобилей</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-6"></div>
                                <div class="col-sm-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" id="table_place" style="overflow: scroll; overflow-x: scroll">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting">Дата нарушения</th>
                                            <th class="sorting">Время нарушения</th>
                                            <th class="sorting">Дата протокола</th>
                                            <th id="color">Номер протокола</th>
                                            <th class="sorting">Гос номер</th>
                                            <th class="sorting">СТС</th>
                                            <th class="sorting">Модель</th>
                                            <th class="sorting">Нарушитель</th>
                                            <th id="edit">Оплачен</th>
                                            <th class="sorting">Сумма</th>
                                            <th class="sorting">Скидка</th>
                                            <th class="sorting">Статья</th>
                                            <th class="sorting">Место нарушения</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <a href="{{ route('bills.syncCars') }}">Синхронизовать список авто</a><br>
                                        @if (is_array($bills))
                                            @foreach($bills as $bill)
                                                <?php $car = $bill['car']; ?>
                                                <tr role="row" style="cursor: pointer; background-color: {{ ($bill['from'])->diff(new DateTime())->days > 60 && $bill['status'] != 'paid' ? '#f0808061' : 'none' }}; {{ ($bill['from'])->diff(new DateTime())->days < 20 && $bill['status'] != 'paid' ? 'background-color:#90ee9052' : '' }};">
                                                    <td>{{ $bill['date_time_violation']->format('d.m.Y') }}</td>
                                                    <td>{{ $bill['date_time_violation']->format('H:i:s') }}</td>
                                                    <td>{{ $bill['from']->format('H:i:s') }}</td>
                                                    <td>{{ $bill['number'] }}</td>
                                                    <td>{{ $bill['car']['number'] }}</td>
                                                    <td>{{ $bill['car']['sts'] }}</td>
                                                    <td></td>
                                                    <td>{{ $bill['violator'] }}</td>
                                                    <td>{{ $bill['status'] == 'paid' ? 'да' : 'нет' }}</td>
                                                    <td>{{ $bill['price_with_discount'] }}/{{ $bill['price_no_discount'] }}</td>
                                                    <td>{{ $bill['price_with_discount'] > 0 ? ($bill['from'])->diff(new DateTime())->days : '-' }}</td>
                                                    <td>{{ $bill['StAP'] }}</td>
                                                    <td>{{ $bill['place_of_violation'] }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <p style="color:red">{{ $bills }} </p>
                                            <a href="{{ route('bills_reauth') }}">Переавторизоваться</a><br>
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="sorting">Дата нарушения</th>
                                            <th class="sorting">Время нарушения</th>
                                            <th class="sorting">Дата протокола</th>
                                            <th id="color">Номер протокола</th>
                                            <th class="sorting">Гос номер</th>
                                            <th class="sorting">СТС</th>
                                            <th class="sorting">Модель</th>
                                            <th class="sorting">Нарушитель</th>
                                            <th id="edit">Оплачен</th>
                                            <th class="sorting">Сумма</th>
                                            <th class="sorting">Скидка</th>
                                            <th class="sorting">Статья</th>
                                            <th class="sorting">Место нарушения</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
                <!-- /.box -->
        </div>
        <!-- /.row -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-lg" style="width: 80%;" role="document">
                <div class="modal-content">
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
@section('footer_js')
    @parent

    <script src="/Admin/js/jquery.dataTable.localised.js"></script>
    <script src="/Admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/Admin/plugins/bootstrap-slider/bootstrap-slider.js"></script>
    <script>
        function initTable(){
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });
            $('#edit').removeClass('sorting');
        }
    </script>
@endsection