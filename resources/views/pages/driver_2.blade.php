<div class="row">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Добавление водителя</h3>
        </div>
        <div style="height:30px;color:grey;font-size:14px;" class="col-md-12">
            Добавление фотографий документов (2/2)
        </div>
        <div class="col-md-12">
            <h4 class="box-title" style="color:grey;">Обязательные фотографии</h4>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <div class="col-md-5">
                    <div>
                        <h4>1. Водительское удостоверение</h4>
                    </div>
                    <form action="{{url('/web/driver/save')}}" name="main_pictures" enctype="multipart/form-data" method="POST">
                        <input type="hidden" name="driver_id" value="{{$driver->id}}">
                        <div class="col-md-6">
                            <span>Лицевая сторона</span>
                            <div style="border: 1px dashed black; width:100%; height:214px; text-align: center; vertical-align: middle; position:relative;
                            @if(isset($front_pic))
                                    background-image:url('{{$front_pic->imagePath.'/'.$front_pic->slug.'.'.$front_pic->extension}}');
                                    background-repeat:no-repeat;
                                    background-size:contain;
                                    background-position:center;
                            @endif">
                                <div style="margin-top:42%;">Перетащите и вставьте фотографию сюда</div>
                                <input id="front" type="file" name="front" style="width:100%; height: 100%;opacity:0; top:0; left:0; position:absolute" onchange="previewMainImages(this)">
                            </div>
                            <div>Или выберите файл из компьютера</div>
                            <button class="btn btn-info pull-right" onclick="chooseFile(this)">Выбрать</button>
                        </div>
                        <div class="col-md-6">
                            <span>Тыльная сторона</span>
                            <div style="border: 1px dashed black; width:100%; height:214px; text-align: center; vertical-align: middle; position:relative;
                            @if(isset($back_pic))
                                    background-image:url('{{$back_pic->imagePath.'/'.$back_pic->slug.'.'.$back_pic->extension}}');
                                    background-repeat:no-repeat;
                                    background-size:contain;
                                    background-position:center;
                            @endif">
                                <div style="margin-top:42%;">Перетащите и вставьте фотографию сюда</div>
                                <input id="back" type="file" name="back" style="width:100%; height: 100%;opacity:0; top:0; left:0; position:absolute" onchange="previewMainImages(this)">
                            </div>
                            <div>Или выберите файл из компьютера</div>
                            <button class="btn btn-info pull-right" onclick="chooseFile(this)">Выбрать</button>
                        </div>
                    </form>

                </div>
            </div>
            <div class="clearfix"></div>
            <hr align="center" width="90%" size="2" color="#dddddd" />
            <form action="{{url('/web/driver/save_additional_pics')}}" name="additional_pictures" id="additional_pictures" enctype="multipart/form-data" method="POST">
            <div class="col-md-12" style="height:200px;">
                    <input type="hidden" name="driver_id" value="{{$driver->id}}">
                    <div style="width:96%; height: inherit; border:1px black dashed; margin-left:2%;">
                        <input id="additional_pics" name="additional_pics[]" type="file" multiple style=" width:100%; height:inherit; opacity:0;" onchange="previewAdditionalImages(this)">
                    </div>
            </div>
            </form>
            <div class="clearfix"></div>
            <hr align="center" width="90%" size="2" color="#dddddd" />
            @if(isset($additional_pics))
                <div class="row">
                    @foreach($additional_pics as $picture)
                        <div class="col-sm-4 col-md-3" onclick="show_fullsize(this)">
                            <div class="thumbnail">
                                <img data-src="holder.js/100%x200" style="height: 200px; display: block;" src="{{$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension}}" data-holder-rendered="true">
                                <div class="caption">
                                    <p>{{$picture->description}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
            <div class="col-md-12" id="additional_images">

            </div>
            <div class="clearfix"></div>
            <div class="col-md-12" style="margin-top:40px;">
                <button class="btn col-md-2 pull-right" onclick="decline()" style="color:#00c0ef; background-color:white; border-color: #00c0ef; margin-left:40px;">Отменить</button>
                <button class="btn btn-info col-md-2 pull-right" onclick="save()">Продолжить</button>
            </div>
        </div>
    </div>
    <div id="driverModal" class="modal fade">
        <div class="modal-dialog" style="width: 80%;" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div style="margin-bottom:20px;" id="message">

                    </div>
                    <div>
                        <img src="" id="driver_image" style="width:100%;">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
</div>

