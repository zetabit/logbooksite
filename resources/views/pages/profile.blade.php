@extends('layouts.app')
@section('header_css')
    @parent

@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
            <li class="active">Профиль</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <section class="content-header">
            <h1 style="color:#ecf0f5;">Анкета пользователя</h1>
            <ol class="breadcrumb">
                <li><a href="/web"><i class="fa fa-dashboard"></i> Главная</a></li>
                <li class="active">Редактирование персональных данных</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Редактирование персональных данных</h3>
                    </div>
                    <form action="{{ url('/web/profile/save') }}" method="post" name="car_form" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Фамилия</label>
                                    <input type="text" @if(isset($info)) value="{{ $info->lastname }}" @endif name="lastname" id="lastname"  class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Имя</label>
                                    <input type="text" @if(isset($info)) value="{{ $info->firstname }}" @endif name="firstname" id="firstname" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Отчество</label>
                                    <input type="text" @if(isset($info)) value="{{ $info->patronymic }}" @endif name="patronymic" id="patronymic" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Основной номер телефона</label>
                                    <input type="text" @if(isset($info)) value="{{ $info->phone }}" @endif name="phone" id="phone" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Дополнительный номер телефона</label>
                                    <input type="text" @if(isset($info)) value="{{ $info->additional_phone }}" @endif name="additional_phone" id="additional_phone" class="form-control">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>e-mail</label>
                                    <input type="text" @if(isset($user)) value="{{ $user->email }}" @endif name="email" id="email" class="form-control" disabled>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Пароль</label>
                                    <input type="text" name="password" id="password" class="form-control">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Подтверджение пароля</label>
                                    <input type="text" name="pass_confirm" id="pass_confirm" class="form-control">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Город</label>
                                    <input type="text" @if(isset($address)) value="{{ $address->city }}" @endif name="city" id="city" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Юридический адрес</label>
                                    <input type="text" @if(isset($address)) value="{{ $address->address }}" @endif name="address" id="address" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Организационно-правовая форма</label>
                                    <select name="organizational_form" id="organizational_form" class="form-control" required onchange="showNameField(this)">
                                        <option value="0">Не выбрана</option>
                                        @for($i=0;$i<count($organizational_form);$i++)
                                            <option value="{{ $i+1 }}">{{ $organizational_form[$i] }}</option>
                                        @endfor
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Название</label>
                                    <input type="text" @if(isset($address)) value="{{ $auto_park->campaign_name }}"  @endif name="campaign_name" id="campaign_name" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>ИНН \ КПП организации</label>
                                    <input type="text" @if(isset($address)) value="{{ $auto_park->INN }}" @endif name="INN" id="INN" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Cвидетельство о государственной регистрации</label>
                                    <input type="text" @if(isset($address)) value="{{ $auto_park->gov_registration_num }}" @endif name="gov_registration_num" id="gov_registration_num" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Уполномоченное лицо</label>
                                    <input type="text" @if(isset($auto_park)) value="{{ $auto_park->chief_fio }}" @endif name="chief_fio" id="chief_fio" class="form-control" required>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group">
                                    <label>Основание</label>
                                    <select name="basis" id="basis" class="form-control" required>
                                        <option value="0">Не выбрано</option>
                                        @for($i=0;$i<count($basis);$i++)
                                            <option value="{{ $i+1 }}">{{ $basis[$i] }}</option>
                                        @endfor
                                    </select>
                                    <span class="help-block"></span>
                                </div>

                            </div>

                        </div>
                        <div class="col-md-12" style="margin-top:40px;">
                            <input type="submit" class="btn btn-info col-md-2 pull-right" value="Сохранить">
                        </div>
                    </form>

                </div>


            </div>
        </section>
    </section>
    <!-- /.content -->
@endsection
@section('footer_js')
    @parent
    <script>
        function showNameField(typeSelect){
            var option=$(typeSelect).val();
            if(option==3){
                $('#campaign_name').parent('.form-group').hide();
            }
            else{
                $('#campaign_name').parent('.form-group').show();
            }
            if(option==2){
                $('#basis').append('<option value="3">На основании свидетельства о регистрации ИП</option>');
            }
            else{
                $('#basis option[value="3"]').remove();
            }
        }
    </script>
@endsection
