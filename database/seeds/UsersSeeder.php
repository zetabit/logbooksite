<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Create users.
     *
     * @return void
     */
    public function run()
    {
        $this->createUser('admin@admin.admin', 'tabaraka', \App\Models\User\Role::ROLE_MANAGER, \App\Models\AutoPark::orderBy('id','DESC')->first());
        $this->createUser('admin1@admin.admin', 'tabaraka', \App\Models\User\Role::ROLE_MANAGER, \App\Models\AutoPark::first());
    }

    public function createUser($email, $pswd, $role_name, $autopark) {
        $user = \App\User::where('email', '=', $email)->get()->first();
        if(!$user) {
            $role = \App\Models\User\Role::where('name', '=', $role_name)->first();

            $info = new \App\Models\User\Info(['firstname' => 'noname']);
            $info->save();

            $user = new \App\User([
                'email' => $email,
                'password' => \Illuminate\Support\Facades\Hash::make($pswd),
                'username' => 'username'.rand(0, 1000000)
            ], $info, $role, $autopark);
            $user->save();
        }
    }
}
