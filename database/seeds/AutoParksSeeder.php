<?php

use Illuminate\Database\Seeder;

class  AutoParksSeeder extends Seeder
{
    /**
     * Create roles.
     *
     * @return void
     */
    public function run()
    {
        /**
         * @param $name string
         * @param $address \App\Models\Address
         * @return mixed
         */
        function createAutoPark ($name, $address) {
            $autoPark = \App\Models\AutoPark::firstOrCreate(['name' => $name, 'address_id' => $address->id]);
            return $autoPark;
        }

        /**
         * @param $autoPark
         * @param $user
         */
       $firstAutoPark = createAutoPark('First AutoPark', \App\Models\Address::firstOrCreate(['address' => 'noaddress']));
       $secondAutoPark = createAutoPark('Second AutoPark', \App\Models\Address::firstOrCreate(['address' => 'also noaddress']));
    }
}
