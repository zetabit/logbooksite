<?php

use Illuminate\Database\Seeder;

class ColorsSeeder extends Seeder
{
    /**
     * Create users.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Color::firstOrCreate([
            'name' => 'Белый',
            'color' => '#FFF',
            'number' => 1,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Черный',
            'color' => '#000',
            'number' => 23,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Красный',
            'color' => 'red',
            'number' => 3,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Синий',
            'color' => 'blue',
            'number' => 13,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Зеленый',
            'color' => 'green',
            'number' => 9,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Желтый',
            'color' => 'yellow',
            'number' => 18,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Борнео1',
            'color' => '#FF951D',
            'number' => 6,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Борнео2',
            'color' => '#D10000',
            'number' => 2,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Серый',
            'color' => '#F2F2F2',
            'number' => 8,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Темно-Зеленый',
            'color' => '#066C86',
            'number' => 42,
        ]);
        \App\Models\Color::firstOrCreate([
            'name' => 'Борнео5',
            'color' => '#1CC1FF',
            'number' => 5,
        ]);
    }
}
