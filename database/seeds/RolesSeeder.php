<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Create roles.
     *
     * @return void
     */
    public function run()
    {
        function createRole ($roleName, $readable_name=null, $public=false) {
            $role = \App\Models\User\Role::firstOrCreate(['name' => $roleName,'readable_name'=>$readable_name, 'public'=>$public]);
            return $role;
        }
        $adminRole=createRole(\App\Models\User\Role::ROLE_ADMIN, 'Администратор', true);
        $adminPermission=new \App\Models\Permission([
            'cars_view'=>1,'cars_edit'=>1,'cars_add'=>1,
            'managers_view'=>1,'managers_edit'=>1,'managers_add'=>1,
            'drivers_view'=>1,'drivers_edit'=>1,'drivers_add'=>1,
            'tasks_view'=>1,'tasks_edit'=>1,'tasks_add'=>1,
            'fines_view'=>1,'fines_edit'=>1,'fines_add'=>1,
            'notifications_view'=>1,'notifications_edit'=>1,'notifications_add'=>1,
            'statistic_view'=>1,'statistic_edit'=>1,'statistic_add'=>1,
            'archive_view'=>1,'archive_edit'=>1,'archive_add'=>1,
            'lists_view'=>1,'lists_edit'=>1,'lists_add'=>1,
            'checkups_view'=>1,'checkups_edit'=>1, 'checkups_add'=>1,
            'acts_view'=>1,'acts_edit'=>1,'acts_add'=>1
        ],$adminRole);
        $adminPermission->save();

        $universalRole=createRole(\App\Models\User\Role::ROLE_UNIVERSAL, 'Универсальная');
        $universalPermission=new \App\Models\Permission([
            'cars_view'=>1,'cars_edit'=>1,'cars_add'=>1,
            'managers_view'=>1,'managers_edit'=>0,'managers_add'=>0,
            'drivers_view'=>1,'drivers_edit'=>1,'drivers_add'=>1,
            'tasks_view'=>1,'tasks_edit'=>0,'tasks_add'=>1,
            'fines_view'=>1,'fines_edit'=>0,'fines_add'=>0,
            'notifications_view'=>1,'notifications_edit'=>0,'notifications_add'=>1,
            'statistic_view'=>1,'statistic_edit'=>0,'statistic_add'=>0,
            'archive_view'=>1,'archive_edit'=>1,'archive_add'=>1,
            'lists_view'=>1,'lists_edit'=>0,'lists_add'=>1,
            'checkups_view'=>1,'checkups_edit'=>0, 'checkups_add'=>1,
            'acts_view'=>1,'acts_edit'=>0,'acts_add'=>1
        ],$universalRole);
        $universalPermission->save();

        $managerRole=createRole(\App\Models\User\Role::ROLE_MANAGER, 'Менеджер');
        $managerPermission=new \App\Models\Permission([
            'cars_view'=>1,'cars_edit'=>1,'cars_add'=>1,
            'managers_view'=>0,'managers_edit'=>0,'managers_add'=>0,
            'drivers_view'=>1,'drivers_edit'=>1,'drivers_add'=>1,
            'tasks_view'=>1,'tasks_edit'=>0,'tasks_add'=>1,
            'fines_view'=>1,'fines_edit'=>0,'fines_add'=>0,
            'notifications_view'=>1,'notifications_edit'=>0,'notifications_add'=>1,
            'statistic_view'=>0,'statistic_edit'=>0,'statistic_add'=>0,
            'archive_view'=>1,'archive_edit'=>0,'archive_add'=>0,
            'lists_view'=>1,'lists_edit'=>0,'lists_add'=>1,
            'checkups_view'=>1,'checkups_edit'=>0, 'checkups_add'=>1,
            'acts_view'=>1,'acts_edit'=>0,'acts_add'=>1
        ],$managerRole);
        $managerPermission->save();

        $accountantRole=createRole(\App\Models\User\Role::ROLE_ACCOUNTANT, 'Бухгалтер');
        $accountantPermission=new \App\Models\Permission([
            'cars_view'=>1,'cars_edit'=>1,'cars_add'=>1,
            'managers_view'=>1,'managers_edit'=>1,'managers_add'=>1,
            'drivers_view'=>1,'drivers_edit'=>1,'drivers_add'=>1,
            'tasks_view'=>1,'tasks_edit'=>1,'tasks_add'=>1,
            'fines_view'=>1,'fines_edit'=>1,'fines_add'=>1,
            'notifications_view'=>1,'notifications_edit'=>0,'notifications_add'=>0,
            'statistic_view'=>1,'statistic_edit'=>1,'statistic_add'=>1,
            'archive_view'=>1,'archive_edit'=>0,'archive_add'=>0,
            'lists_view'=>1,'lists_edit'=>0,'lists_add'=>0,
            'checkups_view'=>1,'checkups_edit'=>0, 'checkups_add'=>0,
            'acts_view'=>1,'acts_edit'=>0,'acts_add'=>0
        ],$accountantRole);
        $accountantPermission->save();

        $techWorkerRole=createRole(\App\Models\User\Role::ROLE_TECHNICAL_WORKER, 'Технический сотрудник');
        $techWorkerPermission=new \App\Models\Permission([
            'cars_view'=>0,'cars_edit'=>0,'cars_add'=>0,
            'managers_view'=>0,'managers_edit'=>0,'managers_add'=>0,
            'drivers_view'=>0,'drivers_edit'=>0,'drivers_add'=>0,
            'tasks_view'=>0,'tasks_edit'=>0,'tasks_add'=>0,
            'fines_view'=>0,'fines_edit'=>0,'fines_add'=>0,
            'notifications_view'=>0,'notifications_edit'=>0,'notifications_add'=>0,
            'statistic_view'=>0,'statistic_edit'=>0,'statistic_add'=>0,
            'archive_view'=>0,'archive_edit'=>0,'archive_add'=>0,

            'lists_view'=>1,'lists_edit'=>0,'lists_add'=>1,
            'checkups_view'=>1,'checkups_edit'=>0, 'checkups_add'=>1,
            'acts_view'=>1,'acts_edit'=>0,'acts_add'=>1
        ],$techWorkerRole);
        $techWorkerPermission->save();

    }
}
