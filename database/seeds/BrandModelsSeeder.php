<?php

use Illuminate\Database\Seeder;

class BrandModelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modelsDir = storage_path('app' . DIRECTORY_SEPARATOR . 'models');
        $files = \Illuminate\Support\Facades\File::files($modelsDir);

        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            $brand = \App\Models\Car\Brand::firstOrCreate([
                'name'  =>  $file->getBasename('.txt')
            ]);

            $lines = file_get_contents($file->getPathname());
            $models = explode(PHP_EOL, $lines);

            foreach ($models as $modelString) {
                $model = \App\Models\Car\CarModel::firstOrCreate([
                    'name'      => $modelString,
                    'brand_id'  => $brand->id
                ]);
            }
        }
    }
}
