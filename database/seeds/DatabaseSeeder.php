<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        $this->call(RolesSeeder::class);
        $this->call(AutoParksSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(ColorsSeeder::class);
        $this->call(CarsSeeder::class);
        $this->call(DriversSeeder::class);
        $this->call(CheckupSeeder::class);
        $this->call(CarStatisticSeeder::class);
        \App\Models\User\Session::create([
            'user_id' => \App\User::first()->id,
            'token' => 'e0933cc72817e96fa4d7993c5ead0d3c4b1c0929749ba6f17929a799860bab7c3653dc54860ce5b0c2c143f56315a0f9fc25d46e5bd9421e910ebb9850ece69d',
            'api_token' => 'e0933cc72817e96fa4d7993c5ead0d3c4b1c0929749ba6f17929a799860bab7c3653dc54860ce5b0c2c143f56315a0f9fc25d46e5bd9421e910ebb9850ece69d',
        ]);
    }
}
