<?php

use Illuminate\Database\Seeder;

class CarStatisticSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $auto_park=\App\Models\AutoPark::first();
            for($i=0; $i<=335; $i++){
                $date=335-$i;
                $carStatisticRow = \App\Models\CarStatistic::firstOrCreate([
                    'cars_on_line'=>rand(20,40),
                    'cars_on_hold'=>rand(5,10),
                    'cars_on_service'=>rand(0,5),
                    'created_at'=>time()-$date*86400,
                    'autopark_id'=>$auto_park->id
                ]);
        }

    }
}
