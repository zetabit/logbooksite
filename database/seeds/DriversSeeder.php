<?php

use Illuminate\Database\Seeder;

class DriversSeeder extends Seeder
{
    /**
     * Create users.
     *
     * @return void
     */
    public function run()
    {
        $facker = Faker\Factory::create('ru_RU');

        $autoPark = \App\Models\AutoPark::first();
        $cars = \App\Models\Car::all()->pluck('id')->toArray();

        if (\App\Models\Driver::all()->count() > 0) {
          echo "Drivers already created, nothing done.".PHP_EOL;
          return;
        }

        $this->createDriver('Чеченский','Тестовый','ЭБоярский', $autoPark, $cars);
        $this->createDriver('Украинский','Тестовый','ЭБоярский', $autoPark, $cars);
        $this->createDriver('Русский','Тестовый','ЭБоярский', $autoPark, $cars);
        $this->createDriver('ЯРусский ','Тестовый ','ЭБоярский', $autoPark, $cars);
        $this->createDriver('Пэруанский ','Тестовый ','ЭБоярский', $autoPark, $cars);
        $this->createDriver('Россиянский','Тестовый','ЭБоярский', $autoPark, $cars);
        $this->createDriver('Свидомит','ЯРусский','ЭБоярский', $autoPark, $cars);
        $this->createDriver('Попереднык','Украинский','ЭБоярский', $autoPark, $cars);
        $this->createDriver('Слава','Украинский ','ЭБоярский', $autoPark, $cars);
        $this->createDriver('Совпадение','Думающий','ЭБоярский', $autoPark, $cars);
        $this->createDriver('Игра','Украинский','Понадусевович', $autoPark, $cars);

        for ($i = 0; $i < rand(50, 150); $i++) {
          echo "Creating additional drivers...";
          $driver = $this->createDriver(
            $facker->realText(32),
            $facker->realText(32),
            $facker->realText(32),
            $autoPark,
            $cars
          );
          echo ' created ' . $driver->id . PHP_EOL;
        }
    }

  /**
   * @param $lastname
   * @param $firstname
   * @param $patronymic
   * @param $autoPark
   * @param $ids_cars
   * @return \App\Models\Driver
   */
    public function createDriver( $lastname ,$firstname,$patronymic, $autoPark, $ids_cars) {
        /** @var \App\Models\Driver $driver */
        $driver = new \App\Models\Driver([
            'firstname' => trim($firstname),
            'lastname' => trim($lastname),
            'patronymic' => trim($patronymic),
            'document_number' => $this->generateRandomString()
        ], $autoPark);
        $driver->save();
        $driver->cars()->sync($ids_cars);
        return $driver;
    }

  /**
   * @param int $length
   * @return string
   */
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
