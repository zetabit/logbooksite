<?php

use Illuminate\Database\Seeder;
use App\Models\User\Role;

class CheckupSeeder extends Seeder
{
    private $imagePath;

    /**
     * Create checkups for created cars.
     *
     * @return void
     */
    public function run()
    {

        $user = \App\User::orderBy('id', 'DESC')->first();
        $autoPark=$user->autoPark;
        $cars = $autoPark->cars;
        $drivers = $autoPark->drivers;

        if(!$user) {
            echo 'User not exists, cancel checkup creation';
            return;
        }

        $this->imagePath = storage_path('seeds'.DIRECTORY_SEPARATOR.'checkup').DIRECTORY_SEPARATOR;

        foreach ($cars as $car) {
          $randDriverIndex = rand(0, sizeof($drivers) - 1);
          $randDriver = $drivers[$randDriverIndex];
          echo 'createCheckup ['.$car->id.', '.$user->id.']'.PHP_EOL;
            $this->createCheckup($car, $user, $randDriver);
        }
    }

    /**
     * @param \App\Models\Checkup $checkup
     * @param \App\Models\Car\CarPart $carPart
     * @param $imagePath
     * @return \App\Models\Checkup\CheckupCarPart
     */
    private function createCheckupPart(\App\Models\Checkup $checkup, \App\Models\Car\CarPart $carPart, \App\User $user,$imagePath) {
        $checkupPartPicture = new \App\Models\Car\CarPartPicture();
        $checkupPartPicture->images['image']['if_not_upload'] = $imagePath;
        $checkupPartPicture->save();

        $checkupPart = new \App\Models\Checkup\CheckupCarPart([
            'checkup_id' => $checkup->id,
            'car_part_id' => $carPart->id,
            'car_part_picture_id' => $checkupPartPicture->id,
            'name' => '',
        ]);
        $checkupPart->save();

        if(rand(0,10) < 4){
            $checkupPartPicture->alt = 'Повреждуха';
            $checkupPartPicture->save();
            event(new \App\Events\Checkup\CarDamagedEvent($checkupPart, $user,  $carPart->car));
        }
        return $checkupPart;
    }

    public function createCheckup(\App\Models\Car $car, \App\User $user, \App\Models\Driver $driver, $bCreateNotification = false) {
        $checkup = new \App\Models\Checkup([
            'car_id' => $car->id,
            'user_id' => $user->id,
            'driver_id' => $driver->id,
            'finished' => true,
            'type' => rand(0, 2),
            'receipt_type' => rand(0, 2),
            'run' => $car->run_total + rand(0, 4000),
            'total_state' => rand(2, 10)
        ]);
        $checkup->save();

        $carPicture = new \App\Models\Car\CarPicture([
            'checkup_id' => $checkup->id,
            'car_id' => $car->id
        ]);
        $carPicture->images['image']['if_not_upload'] = $this->imagePath.'car.png';
        $carPicture->save();

        /** @var \App\Models\Car $car */
        $car = $checkup->car;
        /** @var \App\Models\Car\CarPart $part */
        foreach ($car->parts as $part) {
            /** @var \App\Models\Checkup\CheckupCarPart $checkupPart */
            $checkupPart = self::createCheckupPart($checkup, $part, $user,$this->imagePath.'car-photo-done.png');
        }

        $pic_ids = [];
        for($i = 0 ; $i < 4; $i ++) {
            $carPicture = new \App\Models\Car\CarPartPicture();
            $carPicture->images['image']['if_not_upload'] = $this->imagePath . 'car-photo-done.png';
            $carPicture->save();
            $pic_ids[] = $carPicture->id;
        }
        event(new \App\Events\Checkup\CheckupDoneEvent($checkup));
        return true;
    }
}
