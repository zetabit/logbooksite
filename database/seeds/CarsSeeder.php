<?php

use Illuminate\Database\Seeder;

class CarsSeeder extends Seeder
{
    protected $imagePath;

    public function __construct()
    {
        $this->imagePath = storage_path('seeds'.DIRECTORY_SEPARATOR.'checkup').DIRECTORY_SEPARATOR;
    }

    /**
     * Create users.
     *
     * @return void
     */
    public function run()
    {
        /** @var \App\Models\Car\Brand $brand */
        $brand = $this->createBrand('Tesla');
        $models = $brand->models()->get();
        $states=[\App\Models\Car::STATUS_ON_HOLD,\App\Models\Car::STATUS_ON_LINE,\App\Models\Car::STATUS_ON_SERVICE];
        for ($i = 0, $c = $models->count(); $i < $c; $i++) {
            $model = $models[$i];
            $this->createCar($brand, $model,'AX935'.($i+1).'UK', 1, rand(1, 11), 1234321, $states[rand(0,2)]);
        }

        $brand = $this->createBrand('Zaporoget\'s');
        $models = $brand->models()->get();

        for ($i = 0, $c = $models->count(); $i < $c; $i++) {
            $model = $models[$i];
            $this->createCar($brand, $model,'AX934'.($i+1).'UK', \App\Models\AutoPark::first()->id, rand(1, 11), 1234321, $states[rand(0,2)]);
        }
    }

    public function createCar(\App\Models\Car\Brand $brand, \App\Models\Car\CarModel $model, $number, $park_id, $color_id, $sts, $status) {
        $run=rand(20000, 100000);
        $to_run_values=[5000, 10000, 15000];
        $params = [
            'brand_id' => $brand->id,
            'number' => $number,
            'auto_park_id' => $park_id,
            'color_id' => $color_id,
            'run' => $to_run_values[rand(0,2)],
            'run_total' => $run,
            'run_closest' => $run+rand(1000, 10000),
            'sts' => $sts,
            'status' => $status
        ];

        if($model) $params['model_id'] = $model->id;

        /** @var \App\Models\Car $car */
        $car = \App\Models\Car::firstOrCreate($params);
        if(!$car) return false;

        $car->initParts();

        {
            $picture = new \App\Models\Picture();
            $picture->images['image']['if_not_upload'] = $this->imagePath.'car-photo-done.png';
            $picture->save();

            $orderOutfit = new \App\Models\Car\OrderOutfit([
                'car_id' => $car->id,
                'picture_id' => $picture->id
            ]);
            $orderOutfit->save();

            $picture = new \App\Models\Picture();
            $picture->images['image']['if_not_upload'] = $this->imagePath.'car-photo-done.png';
            $picture->save();

            $orderOutfit = new \App\Models\Car\OrderOutfit([
                'car_id' => $car->id,
                'picture_id' => $picture->id
            ]);
            $orderOutfit->save();
        }

        return true;
    }

    public function createBrand($name) {
        $brand = \App\Models\Car\Brand::create([
            'name' => $name
        ]);

        /** @var Faker\Factory $faker */
        $faker = Faker\Factory::create();
        for($i = 0; $i < rand(5, 10); $i++) {
            $this->createModel($faker->words(1, true), $brand);
        }
        return $brand;
    }

    public function createModel($name, \App\Models\Car\Brand $brand){
        $model = \App\Models\Car\CarModel::create([
            'name' => $name,
            'brand_id' => $brand->id
        ]);
        return $model;
    }
}
