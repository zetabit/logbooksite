<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionsAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->boolean('cars_add')->default(false);
            $table->boolean('managers_add')->default(false);
            $table->boolean('drivers_add')->default(false);
            $table->boolean('tasks_add')->default(false);
            $table->boolean('fines_add')->default(false);
            $table->boolean('notifications_add')->default(false);
            $table->boolean('statistic_add')->default(false);
            $table->boolean('archive_add')->default(false);
            $table->boolean('lists_add')->default(false);
            $table->boolean('checkups_add')->default(false);
            $table->boolean('acts_add')->default(false);
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('cars_add');
            $table->dropColumn('managers_add');
            $table->dropColumn('drivers_add');
            $table->dropColumn('tasks_add');
            $table->dropColumn('fines_add');
            $table->dropColumn('notifications_add');
            $table->dropColumn('statistic_add');
            $table->dropColumn('archive_add');
            $table->dropColumn('lists_add');
            $table->dropColumn('checkups_add');
            $table->dropColumn('acts_add');

        });
    }
}
