<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCsrStatisticsAutopark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_statistic', function (Blueprint $table) {
            $table->integer('autopark_id', false, true);
            $table->foreign('autopark_id')
                ->references('id')
                ->on('auto_parks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_statistic', function (Blueprint $table) {
            $table->dropForeign('car_statistic_autopark_id_foreign');
            $table->dropColumn('autopark_id');
        });
    }
}
