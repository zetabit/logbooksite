<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckupCarPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkup_car_parts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('comment');

            $table->unsignedInteger('checkup_id');
            $table->foreign('checkup_id')
                ->references('id')
                ->on('checkups')
                ->onDelete('cascade');

            $table->unsignedInteger('car_part_id');
            $table->foreign('car_part_id')
                ->references('id')
                ->on('car_parts')
                ->onDelete('cascade');

            $table->unsignedInteger('car_part_picture_id')
                ->references('id')
                ->on('car_part_pictures')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkup_car_parts');
    }
}
