<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city');
            $table->string('address');
        });

        DB::table('addresses')->insert([
          'city' => '',
          'address' => 'нет адреса'
        ]);

        Schema::table('users', function (Blueprint $table) {
            $table->integer('address_id', false, true)->default(1);
            $table->foreign('address_id')
                ->references('id')->on('addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_address_id_foreign');
            $table->dropColumn('address_id');
        });
        Schema::dropIfExists('addresses');
    }
}
