<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDriverParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function(Blueprint $table){
            $table->dropColumn('fio');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('patronymic');
            $table->string('main_phone');
            $table->string('additional_phone');
            $table->string('registration_address');
            $table->string('document_number');
            $table->date('document_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function(Blueprint $table){
            $table->string('fio');
            $table->dropColumn('firstname');
            $table->dropColumn('lastname');
            $table->dropColumn('patronymic');
            $table->dropColumn('main_phone');
            $table->dropColumn('additional_phone');
            $table->dropColumn('registration_address');
            $table->dropColumn('document_number');
            $table->dropColumn('document_date');
        });
    }
}
