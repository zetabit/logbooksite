<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model');
            $table->string('number');
            $table->unsignedInteger('run')->default(0);
            $table->unsignedInteger('run_closest')->default(0);
            $table->unsignedSmallInteger('total_state')->default(0);

            $table->integer('auto_park_id', false, true);
            $table->foreign('auto_park_id')
                ->references('id')
                ->on('auto_parks')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
