<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManufactureYearToCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->unsignedSmallInteger('manufacture_year')->default(0);
            $table->string('mtpl_insurance_number')->defaul("");
            $table->date('mtpl_insurance_to')->default('2000-00-00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function(Blueprint $table) {
            $table->dropColumn('manufacture_year');
            $table->dropColumn('mtpl_insurance_number');
            $table->dropColumn('mtpl_insurance_to');
        });
    }
}
