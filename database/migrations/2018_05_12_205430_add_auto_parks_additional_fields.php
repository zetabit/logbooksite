<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAutoParksAdditionalFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_parks', function (Blueprint $table) {
            $table->integer('organizational_form');
            $table->string('campaign_name');
            $table->string('gov_registration_num');
            $table->integer('basis');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_parks', function (Blueprint $table) {
            $table->dropColumn('organizational_form');
            $table->dropColumn('campaign');
            $table->dropColumn('gov_registration_num');
            $table->dropColumn('basis');
        });
    }
}
