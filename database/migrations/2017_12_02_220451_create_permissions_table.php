<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('role_id');
            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade');
            $table->boolean('cars_view')->default(false);
            $table->boolean('cars_edit')->default(false);
            $table->boolean('managers_view')->default(false);
            $table->boolean('managers_edit')->default(false);
            $table->boolean('drivers_view')->default(false);
            $table->boolean('drivers_edit')->default(false);
            $table->boolean('tasks_view')->default(false);
            $table->boolean('tasks_edit')->default(false);
            $table->boolean('fines_view')->default(false);
            $table->boolean('fines_edit')->default(false);
            $table->boolean('notifications_view')->default(false);
            $table->boolean('notifications_edit')->default(false);
            $table->boolean('statistic_view')->default(false);
            $table->boolean('statistic_edit')->default(false);
            $table->boolean('archive_view')->default(false);
            $table->boolean('archive_edit')->default(false);
            $table->boolean('lists_view')->default(false);
            $table->boolean('lists_edit')->default(false);
            $table->boolean('checkups_view')->default(false);
            $table->boolean('checkups_edit')->default(false);
            $table->boolean('acts_view')->default(false);
            $table->boolean('acts_edit')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
