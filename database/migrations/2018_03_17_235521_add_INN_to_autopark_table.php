<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddINNToAutoparkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_parks', function (Blueprint $table) {
            $table->string('INN')->default("");
            $table->string('chief_fio')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_parks', function (Blueprint $table) {
            $table->dropColumn('INN');
            $table->dropColumn('chief_fio');
        });
    }
}
