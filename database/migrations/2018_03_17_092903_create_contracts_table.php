<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('autopark_id', false, true);
            $table->foreign('autopark_id')
              ->references('id')
              ->on('auto_parks')
              ->onDelete('cascade');

            $table->enum('type', ['act_transmit', 'act_receipt']);

            $table->boolean('is_template')->default(false);

            $table->text('data');

            $table->text('generated');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
