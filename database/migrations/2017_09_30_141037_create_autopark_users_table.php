<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoparkUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // many users can have many autoparks
        Schema::create('auto_park_user', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('auto_park_id', false, true);
            $table->foreign('auto_park_id')
                ->references('id')->on('auto_parks')->onDelete('cascade');

            $table->integer('user_id', false, true);
            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');

            // each user in autopark have a different role
            $table->integer('role_id', false, true);
            $table->foreign('role_id')
                ->references('id')->on('roles')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_park_user');
    }
}
