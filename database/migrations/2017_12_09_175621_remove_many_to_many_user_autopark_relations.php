<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveManyToManyUserAutoparkRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('auto_park_user');
        Schema::table('users', function(Blueprint $table){
            $table->unsignedInteger('auto_park_id');
            $table->foreign('auto_park_id')->references('id')
                ->on('auto_parks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropForeign('users_auto_park_id_foreign');
            $table->dropColumn('auto_park_id');
        });
        Schema::create('auto_park_user', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('auto_park_id', false, true);
            $table->foreign('auto_park_id')
                ->references('id')->on('auto_parks')->onDelete('cascade');

            $table->integer('user_id', false, true);
            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');

            // each user in autopark have a different role
            $table->integer('role_id', false, true);
            $table->foreign('role_id')
                ->references('id')->on('roles')->onDelete('cascade');
            $table->timestamps();
        });
    }
}
