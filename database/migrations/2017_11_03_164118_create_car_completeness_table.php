<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarCompletenessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('completenesses', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('back_wheel')->default(false);
            $table->boolean('fire_extinguisher')->default(false);
            $table->boolean('jack')->default(false);
            $table->boolean('first_aid_kit')->default(false);
            $table->boolean('towing')->default(false);
            $table->boolean('warning_triangle')->default(false);
            $table->boolean('radio')->default(false);
            $table->boolean('battery')->default(false);
            $table->boolean('baloon_wrench')->default(false);
            $table->boolean('none')->default(false);

            $table->timestamps();
        });

        Schema::table('checkups', function (Blueprint $table) {
            $table->unsignedInteger('car_completeness_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('completenesses');

        Schema::table('checkups', function (Blueprint $table) {
            $table->dropColumn('car_completeness_id');
        });
    }
}
