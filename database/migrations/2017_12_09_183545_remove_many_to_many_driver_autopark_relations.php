<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveManyToManyDriverAutoparkRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('auto_park_driver');
        Schema::table('drivers', function(Blueprint $table){
            $table->unsignedInteger('auto_park_id');
            $table->foreign('auto_park_id')->references('id')
                ->on('auto_parks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function(Blueprint $table){
            $table->dropForeign('drivers_auto_park_id_foreign');
            $table->dropColumn('auto_park_id');
        });
        Schema::create('auto_park_driver', function (Blueprint $table) {
        $table->unsignedInteger('driver_id');
        $table->foreign('driver_id')
            ->references('id')
            ->on('drivers')
            ->onDelete('cascade');

        $table->unsignedInteger('auto_park_id');
        $table->foreign('auto_park_id')
            ->references('id')
            ->on('auto_parks')
            ->onDelete('cascade');
    });
    }
}
