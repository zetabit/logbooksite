<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLawAddressIndexToAutoparkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_parks', function (Blueprint $table) {
            $table->integer('law_address_id', false, true)->nullable();
            $table->foreign('law_address_id')
              ->references('id')->on('addresses');
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->unsignedInteger('post_index')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_parks', function (Blueprint $table) {
            $table->dropForeign('auto_parks_law_address_id_foreign');
            $table->dropColumn('law_address_id');
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('post_index');
        });
    }
}
