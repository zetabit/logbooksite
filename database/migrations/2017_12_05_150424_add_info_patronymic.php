<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfoPatronymic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('infos', function (Blueprint $table) {
            $table->renameColumn('FirstName', 'firstname');
            $table->renameColumn('LastName', 'lastname');
            $table->string('patronymic');
            $table->string('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('infos', function (Blueprint $table) {
            $table->renameColumn('firstname', 'FirstName');
            $table->renameColumn('lastname', 'LastName');
            $table->dropColumn('patronymic');
            $table->dropColumn('phone');
        });
    }
}
