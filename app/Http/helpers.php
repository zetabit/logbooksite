<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 31.10.17
 * Time: 19:05
 */

const RESPONSE_RESET_AUTH = 401, RESPONSE_CUSTOM_PAGE = 444, RESPONSE_ACCESS_RESTRICTED = 445,
    RESPONSE_ALERT_TEXT = 446, RESPONSE_ALERT_AND_LOGIN = 447, RESPONSE_ALERT_AND_BACK = 448;

if(!function_exists('public_path'))
{
    /**
     * Return the path to public dir
     * @param null $path
     * @return string
     */
    function public_path($path=null)
    {
        return rtrim(app()->basePath('public/'.$path), '/');
    }
}

if (! function_exists('array_get_attributes')) {
    function array_get_attributes($input, $needed_keys)
    {
        $result = [];
        foreach ($input as $key => $value){
            if(in_array($key, $needed_keys)){
                $result[$key] = $value;
            }
        }
        return $result;
    }
}

if (! function_exists('from_database_notification')) {
    /**
     * @param \Illuminate\Notifications\DatabaseNotification $dataBaseNotification
     * @return mixed
     */
    function make_notification_type($dataBaseNotification)
    {
        $type = $dataBaseNotification->type;

        $model = new $type();

        $model->setRawAttributes((array) $dataBaseNotification->getAttributes(), true);

        return $model;
    }
}