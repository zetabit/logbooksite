<?php

namespace App\Http\Controllers\Api;

use App\Events\Checkup\CarDamagedEvent;
use App\Events\Checkup\CheckupDoneEvent;
use App\Models\AutoPark;
use App\Models\Car;
use App\Models\Checkup;
use App\Models\Color;
use App\Models\Driver;
use App\Models\User\Role;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\Console\Output\NullOutput;

class InspectionController extends BaseController
{
    private $authRepo;
    function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepo = $authRepository;
        $this->middleware('auth');
    }

    function startCheckup(Request $request)
    {
        $user = Auth::user();
        /** @var Car $car */
        $car = Car::find($request->get('car_id'));
        /** @var Checkup $lastCheckup */
        $lastCheckup = $car->checkups()->finished()->get()->first();  //first from order by desc

        $type = $lastCheckup ? ($car->status == Car::STATUS_ON_LINE ? Checkup::TYPE_RECEIPT : Checkup::TYPE_TRANSMIT) : Checkup::TYPE_REVIEW;
        //if only review
        $type = $request->has('checkup_review') && $request->get('checkup_review') ? Checkup::TYPE_REVIEW : $type;

        $type = $request->has('checkup_nz') && $request->get('checkup_nz') ? Checkup::TYPE_NZ : $type;

        //check user in autoPark, check role
        /** @var AutoPark $usersAutoPark */
        $usersAutoPark = $user->autoPark;
        if ($car->AutoPark->id !== $usersAutoPark->id || !$user->role->permission->checkups_add) {
            return response("Вы не имеете прав для создания осмотра.", 446);
        }

        $checkup = Checkup::create([
            'user_id' => $user->id,
            'car_id' => $car->id,
            'type'   => $type,
            'driver_id' => $lastCheckup && $car->status == Car::STATUS_ON_LINE ? $lastCheckup->driver_id : 0,
        ]);
        return $this->getCheckup($checkup->id);
    }

    /** Only FOR READ!!
     * @param $id
     * @return Checkup
     */
    private function getCheckup($id) {
        /** @var Checkup $checkup */
        $checkup = Checkup::with('car', 'car.parts', 'car.color', 'carPictures', 'car.orderOutfits', 'completeness')->where('id', '=', $id)->first();
        /** @var Car $car */
        $car = $checkup->car;
        /** @var Car\CarPart $part */
        foreach ($car->parts as $part) {
            $part->prevPictures = $part->prevPictures();
            foreach ($part->prevPictures as $prevPicture) {
                /** @var Car\CarPartPicture $picture */
                $picture = $prevPicture;

                $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
                $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;
            }
        }

        $car['orderOutfits'] = $car->orderOutfits(['only_today' => true])->get();

        $lastCheckup = $car->getLastCheckup();

        if($lastCheckup) $car['lastCheckup'] = $lastCheckup;

        /** @var Checkup\CheckupCarPart $part */
        foreach ($checkup->parts as $part) {
          /** @var Car\CarPartPicture $picture */
          $picture = $part->picture;

          $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
          $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;
        }

        /** @var Car\CarPicture $carPicture */
        foreach ($checkup->carPictures as $carPicture) {
          $picture = $carPicture;

          $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
          $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;
        }

//        /** @var Car $car */
//        $car = $checkup->car;
//        /** @var Car\CarPart $part */
//        foreach ($car->parts as $part) {
//            /** @var Checkup\CheckupCarPart $checkupPart */
//            foreach ($checkup->parts as $checkupPart) {
//                if($part->id == $checkupPart->car_part_id && $checkupPart->picture) {
//                    $part->pictures();
//                }
//            }
//        }
        $checkup->car->model = $checkup->car->getModelBrand();
        return $checkup;
    }

    /**
     * Set driver (and (optional) change checkup type, and (optional) set receipt type)
     * @param Request $request
     * @return array
     */
    function selectDriver(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        /** @var Checkup $checkup */
        $checkup = Checkup::find($request->get('checkup_id'));
        if($checkup->user_id != $user->id) {
            return response('lol', 401);
        }

        if(!$checkup) {
            return [
                'status' => 'notFound'
            ];
        }

        /** @var Driver $driver */
        $driver = Driver::find($request->get('driver'));

//        if(!$driver) {
//            /** @var Driver $driver */
//            $driver = Driver::firstOrCreate([
//                'fio' => $request->get('driver')
//            ]);
//
//            $ids = $user->AutoParks()->pluck('auto_park_id')->toArray();
//            $driver->AutoParks()->sync($ids);
//            $driver->cars()->sync([$checkup->car->id]);
//        }

        if ($driver) {
          $checkup->driver_id = $driver->id;
          $checkup->save();
        }

        if ($request->has('receipt_type')) {
          $checkup->receipt_type = $request->get('receipt_type', Checkup::RECEIPT_NONE);
          $checkup->save();
        }

        return $driver ?
            [
                'status' => "ok",
                'checkup' => $this->getCheckup($checkup->id)
            ]
            :
            [
                'status' => $request->get('driver') == "no_driver" ? "ok" : "notFound",
                'checkup'=> $this->getCheckup($checkup->id)
            ];
    }

    function acceptPhotoPart(Request $request)
    {
        $user = Auth::user();
        $distance = (int)$request->get('distance', '0');
        $checkup = Checkup::find($request->get('checkup_id'));
        if($checkup->user_id != $user->id) {
            return response('lol', 401);
        }
        if($distance > 0 && $distance < $checkup->car->run_total) {
            return json_encode([
                'status' => 499,
                'message' => 'Километраж меньше предыдущего'
            ]);
        }

        /** @var Car\CarPart $carPart */
        $carPart = Car\CarPart::find($request->get('part_id'));
        $car=$carPart->car;


        $type = $request->get('type', 'none');
        if($type == 'base64') {
            $data = $request->get('data');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            $tmp = tempnam('/tmp', 'img');
            file_put_contents($tmp, $data);

            $checkupPartPicture = new \App\Models\Car\CarPartPicture();
            $checkupPartPicture->images['image']['if_not_upload'] = $tmp;
            $checkupPartPicture->alt = $request->get('comment', "");
            $checkupPartPicture->save();

            $checkupPart = new \App\Models\Checkup\CheckupCarPart([
                'checkup_id' => $checkup->id,
                'car_part_id' => $carPart->id,
                'car_part_picture_id' => $checkupPartPicture->id,
                'name' => ''
            ]);
            $checkupPart->save();
            //$comment=$request->get('comment', "");
            // if(!empty($comment)){
            //     event(new CarDamagedEvent($checkupPart, $user, $car));
            // }
            $checkupPart = Checkup\CheckupCarPart::where('id', '=', $checkupPart->id)->with('picture')->first();

            /** @var Car\CarPartPicture $picture */
            $picture = $checkupPart->picture;

            $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
            $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;

            if($distance > 0) {
                $checkup->run = $distance;
                $checkup->save();
            }

            return $checkupPart;
        }

        return response('lol', 501);
    }

    function acceptCompleteness(Request $request)
    {
        $fillable = [
            'back_wheel',    //Запасное колесо
            'fire_extinguisher',
            'jack',
            'first_aid_kit',
            'towing',
            'warning_triangle',
            'radio',
            'battery',
            'baloon_wrench',
            'none'
        ];

        $user = Auth::user();
        $checkup = Checkup::find($request->get('checkup_id'));
        if($checkup->user_id != $user->id) {
            return response('lol', 401);
        }

        if(!$checkup) {
            return response('checkup not found', 501);
        }

        $completeness = new Car\Completeness();
        for ($i = 0; $i < sizeof($fillable); $i++) {
            $item = $fillable[$i];
            $completeness->$item = $request->get($item, "") == "1";
        }
        $completeness->save();

        $checkup->car_completeness_id = $completeness->id;
        $checkup->save();

        return $this->getCheckup($checkup->id);
    }

    function acceptCarPicture(Request $request)
    {
        $user = Auth::user();
        $checkup = Checkup::find($request->get('checkup_id'));
        if($checkup->user_id != $user->id) {
            return response('lol', 401);
        }

        if(!$checkup) {
            return response('checkup not found', 501);
        }

        $type = $request->get('type', 'none');

        if($type == 'base64') {
            $data = $request->get('data');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            $tmp = tempnam('/tmp', 'img');
            file_put_contents($tmp, $data);

            $carPicture = new \App\Models\Car\CarPicture();
            $carPicture->checkup_id = $checkup->id;
            $carPicture->car_id = $checkup->car_id;
            $carPicture->user_id = $user->id;
            $carPicture->images['image']['if_not_upload'] = $tmp;
            $carPicture->alt = $request->get('comment', '');
            $carPicture->save();

            $carPicture->path = "/".$carPicture->imagePath.'/'.$carPicture->slug.'.'.$carPicture->extension;
            $carPicture->path_thumb = "/".$carPicture->imagePath.'/'.$carPicture->slug.'_0.'.$carPicture->extension;

            return $carPicture;
        }
    }

    function acceptOrderOutfit(Request $request)
    {
        $user = Auth::user();
        /** @var Checkup $checkup */
        $checkup = Checkup::find($request->get('checkup_id'));

        $distance = (int)$request->get('distance', '0');

        if ($checkup->user_id != $user->id) {
            return response('lol', 401);
        }

        if($distance > 0 && $distance < $checkup->car->run_total) {
            return json_encode([
              'status' => 499,
              'message' => 'Километраж меньше предыдущего'
            ]);
        }

        if (!$checkup) {
            return response('checkup not found', 501);
        }

        if ($checkup->car->id != $request->get('car_id')) {
            return response('car not found', 502);
        }

        $type = $request->get('type', 'none');

        if($type == 'base64') {
            $data = $request->get('data');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            $tmp = tempnam('/tmp', 'img');
            file_put_contents($tmp, $data);

            $carPicture = new Car\CarPicture([
                'checkup_id' => $checkup->id,
                'car_id' => $checkup->car->id
            ]);

            $carPicture->images['image']['if_not_upload'] = $tmp;
            $carPicture->alt = $request->get('comment', '');

            $carPicture->save();

            Car\OrderOutfit::create([
                'car_id'    =>  $checkup->car->id,
                'picture_id'=>  $carPicture->id,
                'user_id'   =>  $user->id
            ]);

            $carPicture->path = "/".$carPicture->imagePath.'/'.$carPicture->slug.'.'.$carPicture->extension;
            $carPicture->path_thumb = "/".$carPicture->imagePath.'/'.$carPicture->slug.'_0.'.$carPicture->extension;

            if ($distance > 0) {
                $checkup->run = $distance;
                $checkup->to_complete = $request->get('to_complete') == 1;

                if ($request->get('only_order_outfit')) {
                    $checkup->finished = true;
                }

                $checkup->save();

                if ($request->get('only_order_outfit')) {
                    event(new CheckupDoneEvent($checkup));
                }
            }

            return $carPicture;
        }
    }

    function acceptFinishCheckup(Request $request)
    {
        $user = Auth::user();
        $checkup = Checkup::find($request->get('checkup_id'));
        if($checkup->user_id != $user->id) {
            return response('lol', 401);
        }
        $checkup->total_state = (int)$request->get('total_state');
        $checkup->finished = true;
        $checkup->save();

        event(new CheckupDoneEvent($checkup));

        return $checkup;
    }

    private function base64_to_jpeg($base64_string, $output_file) {
        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' );

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );

        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );

        // clean up the file resource
        fclose( $ifp );

        return $output_file;
    }
}
