<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 23.10.2017
 * Time: 22:49
 */

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\AutoPark;
use App\Models\Permission;
use App\Models\User\Info;
use App\Models\User\Role;
use App\Repositories\Contracts\AuthRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\User;

class ManagerController extends BaseController
{
    public function getManagersList(){
        /** @var User $user */
        $user = Auth::user();
        /** @var Permission $permission */
        $permission = $user->role->permission;

        if (!$permission->managers_view) {
            return response("У вас нет доступа", RESPONSE_ALERT_AND_BACK);
        }

        $management=Role::where('name',Role::ROLE_MANAGER)->first();
        $managers=$management->users;
        $manager_list=[];
        $autopark=AutoPark::where('id',1)->first();
        foreach ($managers as $manager){
            $manager_list[]=[
                'id'=>$manager->getAttribute('id'),
                'name'=>$manager->info->getAttribute('FirstName'),
                'surname'=>$manager->info->getAttribute('LastName'),
                'email'=>$manager->getAttribute('email')
            ];
        }
        $response=[
          'autopark_name'=>$autopark->getAttribute('name'),
          'address_street'=>$autopark->address->getAttribute('address'),
          'address_city'=>$autopark->address->getAttribute('city'),
            'managers_list'=>$manager_list
        ];
        return json_encode($response);
    }
    public function saveManagerData(Request $request){
        /** @var User $user */
        $user = Auth::user();
        /** @var Permission $permission */
        $permission = $user->role->permission;

        if (!$permission->managers_edit) {
            return response("У вас нет доступа", RESPONSE_ALERT_AND_BACK);
        }

        $info=new Info(['FirstName'=>$request->get('name'),'LastName'=>$request->get('surname')]);
        $info->save();
        $address=Address::find(1);
        $manager=new User(['email'=>$request->get('email'),'username'=>$request->get('email'),'password'=>Hash::make('password')],$address,$info);
        $manager->save();
        $manager->roles()->sync([Role::getRoleId(Role::ROLE_MANAGER)]);
        return json_encode(['status'=>'success']);
    }
    public function removeManagerData($manager_id){
        /** @var User $user */
        $user = Auth::user();
        /** @var Permission $permission */
        $permission = $user->role->permission;

        if (!$permission->managers_add) {
            return response("У вас нет доступа", RESPONSE_ALERT_AND_BACK);
        }

        $manager=User::find($manager_id);
        $manager->delete();
        return json_encode(['status'=>'success']);
    }
}