<?php

namespace App\Http\Controllers\Api;

use App\Models\Car;
use App\Models\Checkup;
use App\Models\Color;
use App\Models\User\Role;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class HistoryController extends BaseController
{
    private $authRepo;
    function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepo = $authRepository;
        $this->middleware('auth');
    }

    /**
     * Retrieve last checkup and car info.
     * @param Request $request
     * @param $car_id
     * @return array
     */
    function carHistory(Request $request, $car_id)
    {
      /** @var Checkup $lastItem */
        $lastItem = Checkup::where('car_id', '=', $car_id)
            ->with('car', 'carPictures', 'parts', 'parts.picture', 'parts.carPart')
            ->where('finished', '=', true)
            ->orderBy('id', 'desc')->first();

        if($lastItem) {
            /** @var Checkup\CheckupCarPart $part */
            foreach ($lastItem->parts as $part) {
                /** @var Car\CarPart $carPart */
                $carPart = $part->carPart;
                //full icon path without domain
                $carPart->icon_path = strlen($carPart->icon_path) > 0 ? '/img/icons/'.$carPart->icon_path : '';

                /** @var Car\CarPartPicture $picture */
                $picture = $part->picture;

                $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
                $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;
            }
            /** @var Car\CarPicture $carPicture */
            foreach ($lastItem->carPictures as $carPicture) {
                $picture = $carPicture;

                $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
                $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;
            }
        }

        /** @var Car $car */
        $car = Car::where('id', '=', $car_id)->with('parts', 'orderOutfits')->first();

        /** @var Car\OrderOutfit $orderOutfit */
        foreach ($car->orderOutfits as $orderOutfit) {
          $picture = $orderOutfit->picture;

          $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
          $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;
        }

        $return = [
            'last' => $lastItem,
            'car' => $car
        ];
        return $return;
    }

    function carLast5History(Request $request, $car_id)
    {
        $lastItems = Checkup::where('car_id', '=', $car_id)->with('car', 'parts', 'parts.picture', 'parts.carPart')
            ->where('finished', '=', true)
            ->orderBy('id', 'desc')->get();

        foreach ($lastItems as $lastItem) {
            /** @var Checkup\CheckupCarPart $part */
            foreach ($lastItem->parts as $part) {
                /** @var Car\CarPart $carPart */
                $carPart = $part->carPart;
                //full icon path without domain
                $carPart->icon_path = strlen($carPart->icon_path) > 0 ? '/img/icons/'.$carPart->icon_path : '';

                /** @var Car\CarPartPicture $picture */
                $picture = $part->picture;

                $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
                $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;
            }
        }
        return $lastItems;
    }
}
