<?php

namespace App\Http\Controllers\Api;

use App\Models\AutoPark;
use App\Models\Car;
use App\Models\Checkup;
use App\Models\Color;
use App\Models\Driver;
use App\Models\User\Role;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class CarController extends BaseController
{
    private $authRepo;
    function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepo = $authRepository;
        $this->middleware('auth');
    }

    /**
     * Get car list
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    function carsList(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        $cars = $user->cars();
        /** @var Car $car */
        foreach ($cars as &$car) {
            $car['model'] = $car->getBrandName().' '.$car->getModelName();
            $car['checkup'] = $car->getLastCheckup();
            /** @var Driver $driver */
            $driver = $car->getLastCheckupDriver();
            $car['status'] = $car->getCarStatus() . ($car->status == Car::STATUS_ON_LINE && $driver ? '<br>'.$driver->fio() : '');
        }
        return $cars;
    }

    function carDetail(Request $request, $car_id)
    {
        /** @var Car $car */
        $car = Car::where('id', '=', $car_id)->with('parts', 'color', 'AutoPark')->first();
        $car['checkups'] = $car->checkups()->finished()->get();
        $notifications = [];
        /** @var DatabaseNotification $notification */
        foreach ($car->notifications as $notification) {
            $notification['text'] = $notification->render('app');
            $notifications[] = $notification;
        }
        return $car;
    }

    function carSave(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        if (!$user->role->permission->cars_add) {
            return response("Вы не имеете прав на добавление.", 446);
        }
        $autoPark = $user->autoPark;
        /** @var Color $color */
        $color = Color::first();
        if($request->has('car-color')){
            $color = Color::where('color',$request->get('car-color'))->first();
        }
        $brand=Car\Brand::find($request->get('brand_id'));
        if(!$brand){
            $brand=new Car\Brand(['name'=>$request->get('brand_id')]);
            $brand->save();
        }
        $carModel=Car\CarModel::find($request->get('model_id'));
        if(!$carModel){
            $carModel=$brand->models()->create(['name'=>$request->get('model_id')]);
        }
        $car=new Car([
            'brand_id'      =>  $brand->id,
            'model_id'      =>  $carModel->id,
            'number'        =>  $request->has('car-number') ? mb_strtoupper($request->get('car-number')) : null,
            'auto_park_id'  =>  $autoPark->id,
            'color_id'      =>  $color->getAttribute('id'),
            'run'           =>  $request->has('run') ? (int)$request->get('run') : 10000,
            'run_total'     =>  $request->has('run-total') ? (int)$request->get('run-total') : null,
            'run_closest'   =>  $request->get('run-total') ? (int)$request->get('run-total') + (int)$request->get('diff_run_closest_total') : null,
            'run_last'      =>  $request->get('run_last'),
            'sts'           =>  $request->get('sts'),
            'vin'           =>  $request->get('vin'),
            'status'        =>  $request->get('status')
        ]);
        $car->save();
        $car->initParts();
        return json_encode(['status'=>'success']);
    }

    function carDelete(Request $request)
    {
      /** @var User $user */
      $user = Auth::user();
      if (!$user->role->permission->cars_add || !$user->role->permission->cars_edit || !$user->role->permission->cars_view) {
          return response("Вы не имеете прав на удаление.", 446);
      }

      $car = Car::find($request->get('car_id', -1));
      if(!$car) {
        return response('Машина не найдена', 402);
      }
      $car->delete();
    }

    function getBrands()
    {
        return Car\Brand::all();
    }

    function getBrandModels($brand_id = 0)
    {
        return $brand_id > 0 ? Car\CarModel::where('brand_id',$brand_id)->get() : Car\CarModel::all();
    }

}
