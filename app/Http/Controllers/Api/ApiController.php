<?php

namespace App\Http\Controllers\Api;

use App\Models\Car;
use App\Models\Color;
use App\Models\User\Role;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Cookie;

class ApiController extends BaseController
{
    private $authRepo;
    function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepo = $authRepository;
    }

    function postAuth(Request $request)
    {
        $session = $this->authRepo->authorize($request);
        $newCookie = new Cookie( 'auth_token', $session ? $session->token : '', strtotime('+ 5 years'));

        if($request->has('on_web')) {
            return $session ?
                redirect('/web')->withCookie($newCookie) : redirect('/loginN');
        }

        return response(
            json_encode($session ? [
              'status' => 'authorized',
              'session' => $session->toArray()
             ] : [
              'status' => 'error',
              'message' => 'Нет пользователя с такими данными'
             ]
            )
        )->withCookie($newCookie);
    }

    /**
     * Get the color list
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function colorList(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        return Color::all();
    }

    function colorCreate(Request $request)
    {
        $user = Auth::user();

        return Color::create([
            'number' => Color::count(),
            'name'   => $request->get('name', ''),
            'color'  => $request->get('value', '')
        ]);
    }
}
