<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 19.10.2017
 * Time: 23:39
 */

namespace App\Http\Controllers\Api;
use App\Models\User\Role;
use App\Repositories\Contracts\AuthRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\User;

class UserController extends BaseController
{
    private $authRepo;
    function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepo = $authRepository;
        $this->middleware('auth');
    }
    public function getPersonalData(){
        /** @var User $user */
        $user = Auth::user();
        return json_encode([
            'email'=>$user->getAttribute('email'),
            'name'=>$user->info?$user->info->getAttribute('FirstName'):'',
            'surname'=>$user->info?$user->info->getAttribute('LastName'):'',
            'autopark'=>'Lalka',
            'address'=>$user->address?$user->address->getAttribute('address'):'',
            'city'=>$user->address?$user->address->getAttribute('city'):'',
            'role'=>$user->roleText()
        ]);
    }
    public function setPersonalData(Request $request){
        /** @var User $user */
        $user = Auth::user();
        $info=$user->info;
        $info->setAttribute('FirstName',$request->get('name'));
        $info->setAttribute('LastName',$request->get('surname'));
        $info->save();
        $address=$user->address;
        $address->setAttribute('address',$request->get('address'));
        $address->setAttribute('city',$request->get('city'));
        $address->save();
        return json_encode(['status'=>'success']);
    }
    public function changePassword(Request $request){
        $user = Auth::user();
        if(Hash::check($request->get('old-password'), Auth::user()->password))
        {
            $user->setAttribute('password', Hash::make($request->get('new-password')));
            $user->save();
            return json_encode(['status'=>'success']);
        }
        return json_encode(['status'=>'wrong_pass']);

    }
}