<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 12.11.2017
 * Time: 00:34
 */

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\Models\Car;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Laravel\Lumen\Routing\Controller as BaseController;

class NotificationController extends BaseController
{
    function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepo = $authRepository;
        $this->middleware('auth');
    }
    public function notificationList(){
        $user = \Auth::user();
        //$notifications=TextNotification::orderBy('created_at', 'DESC')->get();
        $notification_list=[];
        $notifications = $user->notifications()->limit(10)->get();
        /** @var DatabaseNotification $notification */
        foreach ($notifications as $notification) {
          $notification_list[] = $notification->render('app');
        }
        return json_encode($notification_list);
    }
    function carNotifications(Request $request, $car_id){
        $notifications=Car::where('id', '=', $car_id)->first()->notifications;
        $notification_list=[];
        return json_encode($notification_list);
    }
    function setNotificationsRead(){
//        TODO: set read
//        $notifications=TextNotification::where('is_unread', true)->get();
//        if($notifications && count($notifications)>0 ){
//            foreach ($notifications as $notification){
//                $notification->setAttribute('is_unread',false);
//                $notification->save();
//            }
//        }
        return json_encode(['status'=>'success']);
    }
    function setCarNotificationsRead(Request $request,$car_id){
        $car=Car::find($car_id);
        if($car){
            $notifications=$car->notifications()->where('is_unread', true)->get();
            if($notifications && count($notifications)>0){
                foreach ($notifications as $notification){
                    $notification->setAttribute('is_unread',false);
                    $notification->save();
                }
            }
        }
        return json_encode(['status'=>count($notifications)]);
    }
}