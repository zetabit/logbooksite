<?php

namespace App\Http\Controllers\Api;

use App\Models\AutoPark;
use App\Models\Car;
use App\Models\Checkup;
use App\Models\Color;
use App\Models\Driver;
use App\Models\User\Role;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class DriverController extends BaseController
{
    private $authRepo;
    function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepo = $authRepository;
        $this->middleware('auth');
    }

    /**
     * Get driver list
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    function driverList(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        return $user->AutoPark->drivers()->with('AutoPark')->get();
    }

    function save(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $data = $request->all();
        $frontPicData = $data['frontPic'];
        $frontPicDataAlt = isset($data['frontPicAlt']) ? $data['frontPicAlt'] : "";
        unset($data['frontPic']); if(isset($data['frontPicAlt'])) unset($data['frontPicAlt']);
        $backPicData  = $data['backPic'];
        $backPicDataAlt = isset($data['backPicAlt']) ? $data['backPicAlt'] : "";
        unset($data['backPic']); if(isset($data['backPicAlt'])) unset($data['backPicAlt']);
        $additionalPicsData = isset($data['additional_pictures']) ? $data['additional_pictures'] : [];
        $additionalPicsDataAlts = isset($data['additional_picturesAlts']) ? $data['additional_picturesAlts'] : [];
        unset($data['additional_pictures']); if(isset($data['additional_picturesAlts'])) unset($data['additional_picturesAlts']);

        $driver = Driver::create(array_merge($data, ['auto_park_id' => $user->autoPark->id]));

        if(!$driver) {
          return response('Driver is not created', 501);
        }

        //front
        $data = $frontPicData;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        $tmp = tempnam('/tmp', 'img');
        file_put_contents($tmp, $data);
        $frontPic = new Driver\DriverDocumentPicture(['alt' => $frontPicDataAlt], $driver);
        $frontPic->images['image']['if_not_upload'] = $tmp;
        $frontPic->save();

        //back
        $data = $backPicData;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        $tmp = tempnam('/tmp', 'img');
        file_put_contents($tmp, $data);
        $frontPic = new Driver\DriverDocumentPicture(['alt' => $backPicDataAlt], $driver);
        $frontPic->images['image']['if_not_upload'] = $tmp;
        $frontPic->save();

        //additional pics
        $number = 0;
        foreach ($additionalPicsData as $additionalPicData) {
          $alt = sizeof($additionalPicsDataAlts) >= $number ? trim($additionalPicsDataAlts[$number]) : "";
          $data = $additionalPicData;
          list($type, $data) = explode(';', $data);
          list(, $data)      = explode(',', $data);
          $data = base64_decode($data);

          $tmp = tempnam('/tmp', 'img');
          file_put_contents($tmp, $data);
          $additionalPic = new Driver\DriverDocumentPicture(['alt' => $alt], $driver);
          $additionalPic->images['image']['if_not_upload'] = $tmp;
          $additionalPic->save();
          $number++;
        }

        return [
          'status' => 'success',
          'driver' => $driver
        ];
    }

}
