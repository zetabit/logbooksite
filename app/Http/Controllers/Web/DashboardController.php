<?php

namespace App\Http\Controllers\Web;
use App\Models\Address;
use App\Models\AutoPark;
use App\Models\User\Info;
use App\Models\User\Role;
use Illuminate\Support\Facades\Auth;
use App\Models\Car;
use App\Models\CarStatistic;
use App\Models\Checkup;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Support\Facades\Mail;
use App\Repositories\Contracts\AuthRepositoryInterface;

class DashboardController extends Controller
{
    protected $email='';
    private $authRepo;
    function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepo = $authRepository;
        parent::__construct();
    }
    function login() {
        return view('pages.login');
    }

    function loginN() {
        return view('pages.login', ['error' => 'Пользователь не найден,  либо данные не верны.']);
    }

    function logout() {
        $user = Auth::user();

        return redirect('/login');
    }

    function index() {
        $this->user=Auth::user();
        $autoPark=$this->autoPark=Auth::user()->autoPark;
        $active_cars=$autoPark->cars()->where('active', true);
        $cars_total=$active_cars->count();
        $cars_on_line=$autoPark->cars()->where('active', true)->where('status', Car::STATUS_ON_LINE)->count();
        $cars_on_hold=$autoPark->cars()->where('active', true)->where('status', Car::STATUS_ON_HOLD)->count();
        $cars_on_service=$autoPark->cars()->where('active', true)->where('status', Car::STATUS_ON_SERVICE)->count();
        $week_stat=CarStatistic::where('autopark_id', $autoPark->id)->where('created_at','>',date('Y-m-d H:i:s',strtotime('-7 day')))->get();
        $month_stat=CarStatistic::where('autopark_id', $autoPark->id)->where('created_at','>',date('Y-m-d H:i:s',strtotime('-1 month')))->get();
        $year_stat=CarStatistic::select(DB::raw('MONTH(created_at) month, YEAR(created_at) year, MAX(cars_on_line) max_cars'))
            ->where('autopark_id', $autoPark->id)
            ->groupBy('year','month')->get();
        $week_labels=[];
        $week_data=[];
        $month_labels=[];
        $month_data=[];
        $year_labels=[];
        $year_data=[];
        if($week_stat && count($week_stat)){
            foreach ($week_stat as $record){
                $week_data[]=$record->cars_on_line;
                $week_labels[]=$record->created_at->format('d/m');
            }
        }
        if($month_stat && count($month_stat)){
            foreach ($month_stat as $record){
                $month_data[]=$record->cars_on_line;
                $month_labels[]=$record->created_at->format('d/m');
            }
        }
        foreach($year_stat as $record){
            $year_data[]=$record->max_cars;
            $year_labels[]=$record->month.'/'.$record->year;
        }
        $today_checkups=Checkup::whereBetween('created_at', [date('Y-m-d', strtotime('-1 day')), date('Y-m-d' , strtotime('+1 day'))])->get()->count();
        $notifications = $this->user->notifications()->limit(10)->get();
        return view('pages.index', compact('cars_total','cars_on_line', 'cars_on_hold', 'cars_on_service', 'week_data', 'week_labels', 'month_data', 'month_labels', 'year_data', 'year_labels', 'notifications', 'today_checkups'));
    }

    function fines() {
        return view('pages.fines');
    }

    function checkup($id) {
        $checkup = Checkup::where('id', '=', $id)
            ->finished()
            ->with('carPictures', 'parts', 'parts.picture')
            ->first();
        $completeness=$checkup->completeness;
        /** @var Checkup\CheckupCarPart $part */
        foreach ($checkup->parts as $part) {
            $picture = $part->picture;

            $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
            $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;
        }

        /** @var Car\CarPicture $carPicture */
        foreach ($checkup->carPictures as $carPicture) {
            $picture = $carPicture;

            $picture->path = "/".$picture->imagePath.'/'.$picture->slug.'.'.$picture->extension;
            $picture->path_thumb = "/".$picture->imagePath.'/'.$picture->slug.'_0.'.$picture->extension;
        }


        return view('pages.checkup', compact('checkup', 'completeness'));
    }
    public function send(Request $request){
        /*TODO создание нового юзера и привязка к компании что ли*/
        $password=$this->generatePassword(8);
        $activationCode=$this->generatePassword(32);

        $info=new Info();
        $info->save();

        $address=new Address();
        $address->save();

        $autoPark=new AutoPark(['address_id'=>$address->id], $address);
        $autoPark->save();
        $role=Role::where('name',Role::ROLE_UNIVERSAL)->first();
        $user=new User([
            'firstname'=>$request->get('firstname'),
            'lastname'=>$request->get('lastname'),
            'email'=>$request->get('email'),
            'activation_code'=>$activationCode
        ], $info, $role, $autoPark);
        $user->password=Hash::make($password);
        $this->email=$request->get('email');
        $user->save();
        $this->sendEmail($request, $password, $activationCode);
    }
    public function sendEmail(Request $request, $password, $activationCode)
    {
        $firstname = $request->get('firstname');
        $lastname = $request->get('firstname');
        Mail::send('pages.mail', [
            'title' => 'Добро вожаловать в Felix Felix',
            'subject' => 'Подтверджение регистрации Felix Felix',
            'firstname' => $firstname,
            'lastname' => $lastname,
            'password' => $password,
            'login'=>$request->get('email'),
            'activation_code' => $activationCode
        ], function ($message)
        {

            $message->from('mazdayets@gmail.com', 'Felix Felix');

            $message->to($this->email);

        });

        return response()->json(['message' => 'Request completed']);
    }
    protected function generatePassword($length = 8){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function activate(Request $request){
        $activation_token=$request->get('activationCode');
        if(!empty($activation_token)){
            $user=User::where('activation_code',$activation_token)->first();
            if(!$user){
                die('nit');
            }else{
                $user->active=true;
                $user->save();
                redirect('/web');
            }
        }
    }
}
