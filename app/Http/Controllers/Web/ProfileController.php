<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 09.02.2018
 * Time: 01:17
 */

namespace App\Http\Controllers\Web;
use App\Models\Address;
use App\Models\AutoPark;
use App\Models\User\Info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index(){
        $user=$this->user;
        $auto_park=$this->autoPark;
        $address=$auto_park->address;
        $info=$user->info;
        $basis=['На основании устава','На основании доверенности','На основании свидетельства о регистрации ИП'];
        $organizational_form=['Юридическое лицо','Индивидуальный предприниматель','Физическое лицо'];
        return view('pages.profile', compact('user', 'auto_park', 'address', 'info', 'basis', "organizational_form"));
    }
    public function save(Request $request){
        $autoPark=$this->autoPark;
        /**@var AutoPark $autoPark*/
        $autoPark->__construct([
            "INN"=>$request->get('INN'),
            "chief_fio"=>$request->get('chief_fio'),
            'organizational_form'=>$request->get('organizational_form'),
            'campaign_name'=>$request->get('campaign_name'),
            'gov_registration_num'=>$request->get('gov_registration_num'),
            'basis'=>$request->get('basis')
        ]);
        $autoPark->save();
        /**@var Info $info*/
        $info=$this->user->info;
        $info->__construct([
            'firstname'=>$request->get('firstname'),
            'lastname'=>$request->get('lastname'),
            'patronymic'=>$request->get('patronymic'),
            'phone'=>$request->get('phone'),
            'additional_phone'=>$request->get('additional_phone', null)
        ]);
        $info->save();
        $address=$autoPark->address;
        $address->__construct([
            'address'=>$request->get('address'),
            'city'=>$request->get('city')
        ]);
        $address->save();
        /**@var Address*/
        return redirect('/web');
    }
}