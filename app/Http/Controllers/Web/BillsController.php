<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11/30/2017
 * Time: 2:26 PM
 */

namespace App\Http\Controllers\Web;
use App\Models\AutoPark;
use App\Models\Car\CarModel;
use App\Models\Checkup;
use App\Models\Color;
use App\Services\BillsService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Car;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class BillsController extends Controller
{
   protected $billsService;

    public function __construct(BillsService $billsService)
    {
        parent::__construct();
        $this->add_permission=$this->role_permissions->fines_add;
        $this->edit_permission=$this->role_permissions->fines_edit;
        $this->view_permission=$this->role_permissions->fines_view;
        $this->billsService=$billsService;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \App\Exceptions\ApiNotAuthorizedException
     * @throws \Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    function index(Request $request) {
        $force  = $request->has('force');
        $cars   = $this->user->cars();
        $bills  = $this->billsService->getBillsCar();

        if (is_array($bills))
        foreach ($bills as &$bill) {
            $bill['updated_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $bill['updated_at']);
            $bill['date_time_violation'] = Carbon::parse($bill['date_time_violation']['date']);
            $bill['from'] = Carbon::createFromFormat('Y-m-d', $bill['from']);
        }

        return view('pages.carsBills', compact('bills'));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
     * @throws \App\Exceptions\ApiNotAuthorizedException
     * @throws \Exception
     */
    public function reauth(Request $request)
    {
        $billsService = (new BillsService());
        $billsService->auth(\Auth::user());
        \Cache::clear();
        return \redirect(route('bills'));
    }

    public function syncCars()
    {
        $cars   = $this->user->cars();
        foreach ($cars as $car) {
            $this->billsService->getBillsForCar($car, true);
        }
        return \redirect()->route('bills');
    }
}