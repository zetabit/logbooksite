<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11/30/2017
 * Time: 2:26 PM
 */

namespace App\Http\Controllers\Web;
use App\Models\AutoPark;
use App\Models\Car\CarModel;
use App\Models\Checkup;
use App\Models\Color;
use App\Services\BillsService;
use Illuminate\Http\Request;
use App\Models\Car;
use App\Http\Controllers\Controller;

class CarController extends Controller
{
    public $runs_list=[5000, 10000, 15000, 20000];
    public function __construct()
    {
        parent::__construct();
        
        $this->add_permission=$this->role_permissions->cars_add;
        $this->edit_permission=$this->role_permissions->cars_edit;
        $this->view_permission=$this->role_permissions->cars_view;
    }

    function index() {
        $autoPark = $this->autoPark;
        $cars = $autoPark->cars()->where('active', true)->with('color', 'brand', 'carModel')->get();
        $brands=Car\Brand::all();
        $models=Car\CarModel::all();
        $runs_list=$this->runs_list;
        return view('pages.cars', compact('cars', 'brands', 'models', 'runs_list'));
    }
    function edit($car_id){
        if($this->edit_permission){
            $car=Car::find($car_id);
            if($car->active){
                $runs_list=$this->runs_list;
                $colors=Color::all();
                $brands=Car\Brand::all();
                $models=Car\CarModel::all();
                return view('pages.car', compact('car', 'brands', 'models', 'colors', 'runs_list'));
            }
            return null;
        }
        return null;
    }
    function car_info($car_id){
        if($this->view_permission){
            $car=Car::find($car_id);
            if($car->active){
                $acts   = $car->checkups()
                  ->finished()
                  ->with('user', 'driver')
                  ->get();
                return view('pages.blocks.car_info', compact('car', 'acts'));
            }
            return null;
        }
        return null;
    }
    function car_info_bills($car_id, BillsService $billsService){
        $car=Car::find($car_id);
        if($car->active){
            $car->bills = $billsService->getBillsForCar($car);
            if (!$car->bills) $car->bills = [];
            return view('pages.blocks.car_info_bills', compact('car'));
        }
        return null;
    }
    function add(){
        if($this->add_permission){
            $brands=Car\Brand::all();
            $colors=Color::all();
            $runs_list=$this->runs_list;
            return view('pages.car', compact( 'brands', 'colors', 'runs_list'));
        }
        return null;
    }
    function save(Request $request){
        $car_id=$request->get('car_id');
        $brand=Car\Brand::find($request->get('brand_id'));
        if(!$brand){
            $brand=new Car\Brand(['name'=>$request->get('brand_id')]);
            $brand->save();
        }
        $carModel=CarModel::find($request->get('model_id'));
        if(!$carModel){
            $carModel=$brand->models()->create(['name'=>$request->get('model_id')]);
        }
        $color=Color::find($request->get('color_id'));
        if(!$color){
            $color=new Color(['name'=>$request->get('color_id')]);
            $color->save();
        }
        $car_data=[
            'model_id'=>$carModel->id,
            'brand_id'=>$brand->id,
            'number'=>$request->get('number'),
            'color_id'=>$color->id,
            'run'=>$request->get('run'),
            'run_closest'=>(int)$request->get('run_total')+(int)$request->get('diff_run_closest_total'),
            'run_total'=>$request->get('run_total'),
            'run_last'=>$request->get('run_last'),
            'sts'=>$request->get('sts'),
            'vin'=>$request->get('vin'),
            'status'=>$request->get('status')
        ];
        if(isset($car_id)){
            $car=Car::find($car_id);
            $car->__construct($car_data, $carModel, $brand);
            $car->save();
        }else{
            $park=$this->autoPark;
            $car=$park->cars()->create($car_data);
            $car->carModel()->associate($carModel);
            $car->brand()->associate($brand);
            $car->initParts();
        }
        return redirect('/web/cars');
    }
    function remove(Request $request){
        if($this->edit_permission){
            $car_id=$request->get('car_id');
            $car=Car::find($car_id);
            $car->__construct(['active'=>false]);
            $car->save();
            return 1;
        }
        return null;
    }
    function get_brand_models($brand_id){
        if((int)$brand_id>0){
            $entity_list=$models=Car\CarModel::where('brand_id',$brand_id)->get();
        }elseif((int)$brand_id){
            $entity_list=$models=Car\CarModel::all();
        }
        return view('pages.blocks.select_options', compact('entity_list'));
    }
    function get_sorted_cars(Request $request){
        if($request->get('sort_request_data')){
            $autoPark = $this->autoPark;
            $cars = $autoPark->cars()->where('active', true);
            $params=$request->get('sort_request_data');

            if($params['model']>0){
                $cars=$cars->where('model_id', $params['model']);
            }elseif($params['brand']>0){
                $cars=$cars->where('brand_id', $params['brand']);
            }
            if(!empty($params['number'])){
                $cars=$cars->where('number', 'like', '%' . $params['number'] . '%');
            }
            list($dist_start,$dist_finish)=explode(',',$params['TO_distance']);
            if($dist_start==100){
                $dist_start=0;
            }
            $cars=$cars->whereRaw('(run_closest-run_total) between '.$dist_start.' and '.$dist_finish);
            if(isset($params['car_status'])&&count($params['car_status'])>0){
                $cars=$cars->whereIn('status',$params['car_status']);
            }
            $cars=$cars->get();
            return view('pages.blocks.cars_table_row', compact('cars'));
        }
        else return null;
    }
}