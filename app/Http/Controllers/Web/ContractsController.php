<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11/30/2017
 * Time: 2:26 PM
 */

namespace App\Http\Controllers\Web;
use App\Models\AutoPark;
use App\Models\BaseContract;
use App\Services\BillsService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContractsController extends Controller
{
   protected $billsService;

    public function __construct()
    {
        parent::__construct();
        //$this->add_permission=$this->role_permissions->fines_add;
        $this->edit_permission=$this->role_permissions->managers_edit;
        //$this->view_permission=$this->role_permissions->fines_view;
    }

    function index(Request $request) {

        $user = \Auth::user();
        $contracts = $this->contracts($user);

        return view('pages.contracts', compact('contracts'));
    }

    function add(Request $request) {

        $user = \Auth::user();
        $contracts = $this->contracts($user);

        return view('pages.contract_add', compact('contracts'));
    }

    function show($id) {
        $user = \Auth::user();
        $contracts = collect($this->contracts($user));
        /** @var BaseContract $contractDB */
        $contractDB = BaseContract::findOrFail($id);
        $contract = $contractDB->typedEntity();
        $contracts = $contracts->where('type', $contract->type);

        return view('pages.contract', compact('contracts', 'contract'));
    }

    function save(Request $request) {
        $contract = BaseContract::findOrFail($request->get('id', 0));
        $newContract = new BaseContract();
        $newContract->type = $contract->type;
        $newContract->data = $request->get('data');
        $newContract->variables = $contract->variables;
        $newContract->autopark_id = $contract->autopark_id;
        $newContract->is_template = 1;
        $newContract->save();

        return [
            'result'  => 'ok',
            'text'    => 'сохранено',
            'id'      => $newContract->id,
        ];
    }

    function print_contract($id) {
        $user = \Auth::user();
        $contracts = collect($this->contracts($user));
        /** @var BaseContract $contractDB */
        $contractDB = BaseContract::findOrFail($id);
        $contract = $contractDB->typedEntity();

        return view('pages.contract_print', compact('contract', 'contracts'));
    }

    protected function contracts(User $user = null) {
        $user ? : \Auth::user();
        $contractsCollected = $user->autoPark->contracts;
        /** @var BaseContract[] $contracts */
        $contracts = [];

        /** @var BaseContract $contract */
        foreach ($contractsCollected as $contract)
            $contracts[] = $contract->typedEntity();
        return $contracts;
    }
}