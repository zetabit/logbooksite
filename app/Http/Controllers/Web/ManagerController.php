<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 02.12.2017
 * Time: 23:18
 */

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Models\User\Info;
use Illuminate\Http\Request;
use App\Models\AutoPark;
use App\Models\User\Role;
use App\User;
use Illuminate\Support\Facades\Hash;

class ManagerController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_permission=$this->role_permissions->managers_add;
        $this->edit_permission=$this->role_permissions->managers_edit;
        $this->view_permission=$this->role_permissions->managers_view;
    }

    function index(){
        /** @var User $user */
        $managers=$this->autoPark->users;
        return view('pages.managers', compact('managers'));
    }
    function add($step){
        if($this->add_permission){
            $roles=Role::all();
            return view('pages.manager_'.$step, compact('roles'));
        }
        return null;
    }
    function edit_info($manager_id){
        if($this->edit_permission){
            $roles=Role::all();
            $manager=User::find($manager_id);
            $info=$manager->info;
            return view('pages.manager_1', compact('info','manager', 'roles'));
        }
        return null;

    }
    function edit_data($manager_id){
        if($this->edit_permission){
            $manager=User::find($manager_id);
            return view('pages.manager_2', compact('manager'));
        }
        return null;
    }
    function save(Request $request){
        $manager_id=$request->get('manager_id');
        $role=Role::find($request->get('role_id'));
        $user_data=[
            'email'=>$request->get('login'),
            'password'=>$request->get('pass'),
            'role_id'=>$request->get('role_id')
        ];
        $info_data=[
            'firstname'=>$request->get('firstname'),
            'lastname'=>$request->get('lastname'),
            'patronymic'=>$request->get('patronymic'),
            'phone'=>$request->get('phone')
        ];
        if($manager_id){
            $user=User::find($manager_id);
            $user->__construct($user_data);
            $user->save();
            $user->role()->associate($role);
            $info=$user->info;
            $info->__construct($info_data);
            $info->save();
            if (isset($user_data['password'])) {
                $user->password = Hash::make($user_data['password']);
                $user->save();
            }
        }else{
            $user_data['username']='username'.rand(0,100000);
            $info=new Info($info_data);
            $info->save();
            $user=new User($user_data, $info, $role, $this->autoPark);
            $user->save();
            if (isset($user_data['password'])) {
                $user->password = Hash::make($user_data['password']);
                $user->save();
            }
        }

    }
    function get_role_permissions($role_id){
        $role=Role::find($role_id);
        $permissions=$role->permission;
        return view('pages.blocks.permission_list', compact('permissions'));
    }
}