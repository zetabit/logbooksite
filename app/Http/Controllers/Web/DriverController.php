<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 11.12.2017
 * Time: 21:01
 */

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use Illuminate\Http\Request;
use App\Models\AutoPark;
use Illuminate\Support\Facades\Input;
use function Sodium\add;

class DriverController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_permission=$this->role_permissions->drivers_add;
        $this->edit_permission=$this->role_permissions->drivers_edit;
        $this->view_permission=$this->role_permissions->drivers_view;
    }

    function index(){
        $drivers=$this->autoPark->drivers()->where('active', true)->get();
        return view('pages.drivers', compact('drivers'));
    }
    function edit_info($driver_id){
        if($this->edit_permission){
            $driver=Driver::find($driver_id);
            return view('pages.driver_1', compact('driver'));
        }
        return null;
    }
    function edit_data(Request $request){
        if($this->edit_permission){
            $attributes=[
                'firstname'=>$request->get('firstname')?$request->get('firstname'):'',
                'lastname'=>$request->get('lastname')?$request->get('lastname'):'',
                'patronymic'=>$request->get('patronymic')?$request->get('patronymic'):'',
                'main_phone'=>$request->get('main_phone')?preg_replace('/[^0-9]/', '', $request->get('main_phone')):'',
                'additional_phone'=>$request->get('additional_phone')?preg_replace('/[^0-9]/', '', $request->get('additional_phone')):'',
                'registration_address'=>$request->get('registration_address')?$request->get('registration_address'):'',
                'document_number'=>$request->get('document_number')?$request->get('document_number'):'',
                'document_date'=>$request->get('document_date')?date('Y-m-d', strtotime($request->get('document_date'))):''
            ];
            $driver_id=$request->get('driver_id');
            $driver=Driver::find($driver_id);
            $driver->__construct($attributes);
            $driver->save();

            $front_pic=$driver->documentPictures()->where('type', 'front')->orderBy('created_at', 'desc')->first();
            $back_pic=$driver->documentPictures()->where('type', 'back')->orderBy('created_at', 'desc')->first();
            $additional_pics=$driver->documentPictures()->where('type', 'additional')->get();
            return view('pages.driver_2', compact('driver', 'front_pic', 'back_pic', 'additional_pics'));

        }
        return null;
    }
    function add_driver_data(){
        if($this->add_permission){
            return view('pages.driver_1');
        }
        return null;
    }
    function add_driver_images(Request $request){
        if($this->add_permission){
            $autopark=$this->autoPark;
            $attributes=[
                'firstname'=>$request->get('firstname')?$request->get('firstname'):'',
                'lastname'=>$request->get('lastname')?$request->get('lastname'):'',
                'patronymic'=>$request->get('patronymic')?$request->get('patronymic'):'',
                'main_phone'=>$request->get('main_phone')?preg_replace('/[^0-9]/', '', $request->get('main_phone')):'',
                'additional_phone'=>$request->get('additional_phone')?preg_replace('/[^0-9]/', '', $request->get('additional_phone')):'',
                'registration_address'=>$request->get('registration_address')?$request->get('registration_address'):'',
                'document_number'=>$request->get('document_number')?$request->get('document_number'):'',
                'document_date'=>$request->get('document_date')?date('Y-m-d', strtotime($request->get('document_date'))):''
            ];
            $driver=$autopark->drivers()->create($attributes);
            return view('pages.driver_2', compact('driver'));
        }
        return null;
    }
    function save_additional_pics(Request $request){
        $driver=Driver::find($request->get('driver_id'));
        $additionalPic = new Driver\DriverDocumentPicture(['alt' => $driver->firstname.' '.$driver->lastname.' дополнительная ', 'type'=>'additional'], $driver);
        $additionalPic->images['image']['input_name'] = 'additional_pic';
        $additionalPic->save();
        return view('pages.blocks.image_preview', compact('additionalPic'));
    }
    function save(Request $request){
        $driver=Driver::find($request->get('driver_id'));

        $frontPic = new Driver\DriverDocumentPicture(['alt' => $driver->firstname.' '.$driver->lastname.' лицевая часть', 'type'=>'front'], $driver);
        $frontPic->images['image']['input_name'] = 'front';
        $frontPic->save();

        $backPic = new Driver\DriverDocumentPicture(['alt' => $driver->firstname.' '.$driver->lastname.' тыльная часть', 'type'=>'back'], $driver);
        $backPic->images['image']['input_name'] = 'back';
        $backPic->save();

        return redirect('/web/drivers');
    }
    protected function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    function remove_additional_image(Request $request){
        if($this->edit_permission){
            $image=Driver\DriverDocumentPicture::find($request->get('image_id'));
            $image->delete();
            return 1;
        }
        return null;
    }
    function set_additional_image_description(Request $request){
        if($this->edit_permission){
            $image=Driver\DriverDocumentPicture::find($request->get('image_id'));
            $image->__construct(['description'=>$request->get('description')]);
            $image->save();
            return 1;
        }
        return null;
    }
    function remove(Request $request){
        if($this->edit_permission){
            $driver=Driver::find($request->get('driver_id'));
            $driver->__construct(['active'=>0]);
            $driver->save();
            return 1;
        }
        return null;
    }
    function driver_info($driver_id){
        if($this->view_permission){
            $driver=Driver::find($driver_id);
            $front_pic=$driver->documentPictures()->where('type', 'front')->orderBy('created_at', 'desc')->first();
            $back_pic=$driver->documentPictures()->where('type', 'back')->orderBy('created_at', 'desc')->first();
            $additional_pics=$driver->documentPictures()->where('type', 'additional')->get();
            return view('pages.blocks.driver_info', compact('driver', 'front_pic', 'back_pic', 'additional_pics'));
        }
        return null;
    }
}