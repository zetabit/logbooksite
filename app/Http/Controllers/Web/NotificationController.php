<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 28.01.2018
 * Time: 23:31
 */

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->add_permission=$this->role_permissions->notifications_add;
        $this->edit_permission=$this->role_permissions->notifications_edit;
        $this->view_permission=$this->role_permissions->notifications_view;
    }

    protected $type_list=[
        [0, 'Повреждения', 'App\Notifications\CarDamagedNotification'],
        [1, 'Пробег ТО', 'App\Notifications\CarTRNotification']
    ];
    function notifications(){
        $notifications=$this->user->notifications()->limit(30)->get();
        $more=false;
        if(count($this->user->notifications()->limit(30)->offset(31)->get())>0){
            $more=true;
        }
        $types=$this->type_list;
        $cars=$this->autoPark->cars()->where('active', 1)->get();
        return view('pages.notifications', compact('notifications', 'more', 'cars', 'types'));
    }
    function more(Request $request){
        if($request->get('notif_request_data')){
            $params=$request->get('notif_request_data');
            $type=$params['type'];
            $car_id=$params['car_id'];
            $count=$params['count'];
            $notifs=$this->user->notifications();
            if($type>=0){
                $notifs=$notifs->where('type',$this->type_list[$type][2]);
            }
            if(!empty($car_id)){
                $notifs=$notifs->where('data','LIKE', '{"car_id":'.$car_id.'%');
            }
            $more=false;
            if(count($notifs->limit(31)->offset($count)->get())>30){
                $more=true;
            }
            $notifications=$notifs->limit(30)->offset($count)->get();
            return view('notifications.rows', compact('notifications', 'more'));
        }
        else return null;
    }
    function show($notif_id){
        if($this->view_permission){
            $notification = $this->user->notifications()->where('id',$notif_id)->first();
            $notification->markAsRead();
            return $notification->render('full');
        }
        return null;
    }
}