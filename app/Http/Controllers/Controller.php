<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected $autoPark;
    protected $user;
    protected $role_permissions;
    protected $view_permission;
    protected $edit_permission;
    protected $add_permission;

    public function __construct()
    {
        $this->user = Auth::user();
        $this->autoPark = $this->user ? $this->user->autoPark : null;
        $this->role_permissions = $this->user ? $this->user->role->permission : null;
    }
}
