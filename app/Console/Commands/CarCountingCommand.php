<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 29.11.2017
 * Time: 00:38
 */

namespace App\Console\Commands;
use App\Models\AutoPark;
use App\Models\Car;
use Illuminate\Console\Command;
use App\Models\CarStatistic;
use Illuminate\Support\Facades\Auth;

class CarCountingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'car_counting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Считаем количество автомобилей на линии';

    public function __construct()
    {
        parent::__construct();
    }
    /**
     *
     * @return mixed
     */
    public function handle()
    {
        $autoparks=AutoPark::all();
        foreach ($autoparks as $autopark){
            $cars_on_line=$autopark->cars()->where('active', true)->where('status', Car::STATUS_ON_LINE)->count();
            $cars_on_hold=$autopark->cars()->where('active', true)->where('status', Car::STATUS_ON_HOLD)->count();
            $cars_on_service=$autopark->cars()->where('active', true)->where('status', Car::STATUS_ON_SERVICE)->count();
            $car_statistics=new CarStatistic(['cars_on_line'=>$cars_on_line, 'cars_on_hold'=>$cars_on_hold, 'cars_on_service'=>$cars_on_service, 'autopark_id'=>$autopark->id]);
            $car_statistics->save();
        }
    }
}