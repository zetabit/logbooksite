<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 29.11.2017
 * Time: 00:38
 */

namespace App\Console\Commands;
use App\Models\Address;
use App\Models\AutoPark;
use App\Models\User\Info;
use App\Models\User\Role;
use App\User;
use Illuminate\Console\Command;
use App\Models\CarStatistic;
use Illuminate\Support\Facades\Password;

class CreateAutoParkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create_autopark {autoParkName} {userName} {userEmail} {userPassword}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создаем автопарк с пользователем';

    public function __construct()
    {
        parent::__construct();
    }
    /**
     *
     * @return mixed
     */
    public function handle()
    {
      echo 'User creating....' . PHP_EOL;
      $autoParkName = $this->argument('autoParkName');
      $userName = $this->argument('userName');
      $userEmail= $this->argument('userEmail');
      $userPassword = $this->argument('userPassword');
      $hash = \Illuminate\Support\Facades\Hash::make($userPassword);

      $user = User::where('email', '=', $userEmail)->first();
      if($user) {
        echo 'User with this email ' . $userEmail . ' already created.' . PHP_EOL;
        return;
      }

      $address = Address::create([
        'city' => 'no City',
        'address' => 'no Address'
      ]);
      echo 'Address [' . $address->id . '] is created ....' . PHP_EOL;

      $autoPark = new AutoPark([
        'name' => $autoParkName,
        'address_id' => $address->id
      ]);

      $autoPark->save();

      echo 'AutoPark [' . $autoPark->id . '] is created ....' . PHP_EOL;

      $role = \App\Models\User\Role::where('name', '=', Role::ROLE_ADMIN)->first();

      echo 'Role [' . $role->id . '] is found ....' . PHP_EOL;

      $info = new \App\Models\User\Info([
        'firstname' => 'noname',
      ]);
      $info->save();

      echo 'Info [' . $info->id . '] is created ....' . PHP_EOL;

      //'info_id','username', 'role_id', 'auto_park_id'
      $user = new User([
        //'name' => $userName,
        'email'=> $userEmail,
        //'info_id'=> $info->id,
        'username'=> 'username' . rand(1, 100000000) . rand(1, 1000000000),
        //'role_id' => $role->id,
        //'auto_park_id' => $autoPark->id,
        'password' => $hash
      ], $info, $role, $autoPark);

      $user->save();

      $user->password = $hash;
      $user->save();

      echo $userPassword . ' ' . $user->password . PHP_EOL;

      echo 'User [' . $user->id .'] is created.' . PHP_EOL;
    }
}