<?php

namespace App\Console;
use App\Console\Commands\CarCountingCommand;
use App\Console\Commands\CreateAutoParkCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CarCountingCommand::class,
        CreateAutoParkCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('car_counting')
            ->daily()
            ->withoutOverlapping();
    }
}
