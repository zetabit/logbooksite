<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 01.10.17
 * Time: 23:37
 */

namespace App\Repositories;

use App\Models\User\Session;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\Repositories\Contracts\SessionRepositoryInterface;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SessionRepository implements SessionRepositoryInterface
{
    /**
     * @param Request $request
     * @param $user
     * @return id || null
     */
    public function makeSession(Request $request, $user)
    {
        $session = Session::create([
            'user_id' => $user->id,
            'token' => $this->genToken(),
            'api_token' => $this->genToken(),
            'expired' => (new Carbon())->addYear(),
            'last_ip' => ip2long($request->getClientIp())
        ]);
        return $session ? $session->id : null;
    }

    public function genToken()
    {
        $str = bin2hex(random_bytes(64));
        return $str;
    }
}