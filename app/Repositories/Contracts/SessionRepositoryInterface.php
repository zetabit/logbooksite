<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 01.10.17
 * Time: 23:26
 */

namespace App\Repositories\Contracts;


use Illuminate\Http\Request;

interface SessionRepositoryInterface
{
    public function makeSession(Request $request, $user);

    public function genToken();
}