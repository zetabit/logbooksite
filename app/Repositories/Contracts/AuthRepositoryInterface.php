<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 01.10.17
 * Time: 23:26
 */

namespace App\Repositories\Contracts;


use Illuminate\Http\Request;

interface AuthRepositoryInterface
{
    public function findByEmail($email);

    //private function check($user, $password);

    public function authorize(Request $request);

    public function createSession(Request $request, $user);
}