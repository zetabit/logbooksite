<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 01.10.17
 * Time: 23:37
 */

namespace App\Repositories;

use App\Models\User\Session;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthRepository implements AuthRepositoryInterface
{
    /**
     * @param $email
     * @return User || null
     */
    public function findByEmail($email)
    {
        $user = User::where('email', '=',$email)->get()->first();
        return $user;
    }

    /**
     * @param $user
     * @param $password
     * @return bool
     */
    private function check($user, $password)
    {
        if($user){
            return Hash::check($password, $user->password);
        }
        return false;
    }

    /**
     * @param Request $request
     * @return Session|bool
     */
    public function authorize(Request $request)
    {
        $user = $request->has('email') ? $this->findByEmail($request->get('email')) : null;
        $password = $request->get('password', null);

        if(!$user)
            return false;

        if(!$this->check($user, $password))
            return false;

        $session = $this->createSession($request, $user);
        return $session ? : false;
    }

    /**
     * @param Request $request
     * @param $user
     * @return Session || null
     */
    public function createSession(Request $request, $user)
    {
        $sessionRep = new SessionRepository();
        $session_id = $sessionRep->makeSession($request, $user);
        return $session_id !== null ? Session::find($session_id) : null;
    }
}