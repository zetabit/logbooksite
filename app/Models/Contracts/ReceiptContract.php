<?php

namespace App\Models\Contracts;

use App\Interfaces\BaseContractInterface;
use App\Models\Address;
use App\Models\AutoPark;
use App\Models\BaseContract;
use App\Models\Car;
use App\Models\Checkup;
use App\Models\Driver;
use App\Models\User\Role;
use App\Traits\AbbreviationTrait;
use App\User;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Contracts\ReceiptContract
 *
 * @property int $id
 * @property int $autopark_id
 * @property string $type
 * @property int $is_template
 * @property string $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\AutoPark $autoPark
 * @property-read mixed $generate_abbreviation
 * @property Driver $driver
 * @property Car $car
 * @property Checkup $checkup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereAutoparkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereIsTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ReceiptContract extends BaseContract implements BaseContractInterface
{
    use AbbreviationTrait;

    protected $table = 'contracts';

    protected $args = [
        'driver'    => [
            'key'   => 'driver_id',
            'type'  => Driver::class,
        ],
        'car'       => [
            'key'   => 'car_id',
            'type'  => Car::class,
        ],
        'checkup'   => [
            'key'   => 'checkup_id',
            'type'  => Checkup::class,
        ],
        'auto_park' => [
            'key'   => 'auto_park_id',
            'type'  => AutoPark::class,
        ]
    ];

    public function getTypeTemplateAttribute()
    {
        return 'акт приема';
    }

    /**
     * @return array
     */
    public function setDataTemplate()
    {
        $act_transmit = null;
        return [
          'DRIVER_FIO'              => $this->driver->fio,
          'DRIVER_PASSPORT_NUMBER'  => "номер паспорта",
          'DRIVER_PASSPORT_ISSUED_WHO' => "кем выдан",
          'DRIVER_PASSPORT_ISSUED_WHEN'=> "когда выдан",
          'DRIVER_LICENCE_NUMBER'   =>  $this->driver->document_number,
          'DRIVER_LICENCE_WHEN'     =>  $this->driver->document_date,
          'DRIVER_LICENCE_TO'       =>  "ввести дату",
          'DRIVER_BIRTHDAY'         =>  "ввести дату рождения",
          'DRIVER_PLACE_OF_RESIDENCE'=>  $this->driver->registration_address,
          'DRIVER_PLACE_OF_LIVING'   =>  "фактическое место проживание",
          'DRIVER_PHONE_NUMBERS'    =>  $this->driver->main_phone . (strlen($this->driver->additional_phone) > 0 ? ', ' . $this->driver->additional_phone : ''),
          'CONTRACT_ABBREVIATION'   =>  $this->generateAbbreviation,
          'CONTRACT_DATA'           =>  $this->updated_at,
          'CAR_BRAND_MODEL'         =>  $this->car->getModelBrand(),
          'CAR_NUMBER'      =>  $this->car->number,
          'CAR_TS'          =>  $this->car->sts,
          'CAR_VIN'         =>  $this->car->vin,
          'CAR_YEAR'        =>  $this->car->manufacture_year,
          'CAR_COLOR'       =>  $this->car->color->name,
          'CAR_MTPL_NUMBER' =>  $this->car->mtpl_insurance_number,
          'CAR_MTPL_TO'     =>  $this->car->mtpl_insurance_to,
          'ACT_TRANSMIT_ABBREVIATION'   =>  "",
          'ACT_TRANSMIT_FROM'           =>  "",
          'CHECKUP_ABBREVIATION'=>  $this->checkup->generateAbbreviation,
          'USER_ABBREVIATION'   =>  $this->driver->generateAbbreviation
        ];
    }

    public function getDataTemplateAttribute()
    {
        $generatedData = $this->setDataTemplate();
        $data = $this->data;
        return str_replace(array_keys($generatedData), array_values($generatedData), $data);
    }
}
