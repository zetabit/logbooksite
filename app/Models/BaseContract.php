<?php

namespace App\Models;

use App\Interfaces\BaseContractInterface;
use App\Models\Address;
use App\Models\User\Role;
use App\Traits\AbbreviationTrait;
use App\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Relations\Relation;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\BaseContract
 *
 * @property int $id
 * @property int $autopark_id
 * @property string $type
 * @property int $is_template
 * @property string $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\AutoPark $autoPark
 * @property-read mixed $generate_abbreviation
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereAutoparkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereIsTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseContract whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BaseContract extends Model
{
    use AbbreviationTrait;

    protected $table = 'contracts';

    protected $args = [];

    public $availTypes = ['act_transmit', 'act_receipt'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'autopark_id',
        'type',
        'is_template',
        'data',
        'generated',
        'variables',
    ];

    protected $casts = [
        'created_at'    => 'datetime',
        'updated_at'    => 'datetime',
        'variables'     => 'array',
    ];

    public $timestamps = true;

    /**
     * Set passed to this function, variables.
     */
    public function fromVariables()
    {
        //not classes
        $types = ['string', 'integer', 'float', 'double', 'bool', 'boolean', 'array'];
        if(!isset($this->args)) return;
        $avail_keys = array_keys($this->args);
        $avail_values = array_values($this->args);
        $args = func_get_args();
        foreach ($args as $index => $val) {
            $curKey = $avail_keys[$index];
            $curVal = $avail_values[$index];

            $class = $curVal['type'];

            if (!($val instanceof $class) && !in_array($class, $types)) {
                $this->$curKey = $class::where('id', '=', $val)->first();
            } else {
                $this->$curKey = $val;
            }
        }

        if (method_exists($this, 'afterSetVariables'))
            $this->{'afterSetVariables'}();
    }

    /**
     * Get collected variables from model.
     *
     * @return array
     */
    public function toVariables()
    {
        $avail_keys = array_keys($this->args);
        $avail_values = array_values($this->args);
        $result = [];

        for ($i = 0; $i < sizeof($avail_keys); $i++)
        {
            $key = $avail_keys[$i];
            $val = $avail_values[$i];
            if (isset($val['key'])) {
                $db_key = $val['key'];
                $result[$db_key] = $this->{$key}->id;
            } else {
                $result[$key] = $this->$key;
            }
        }

        if (method_exists($this, 'beforeToVariables'))
            $this->{'beforeToVariables'}();

        return $result;
    }

    public function autoPark(){
        return $this->belongsTo('App\Models\AutoPark', 'autopark_id');
    }

    public function typedEntity()
    {
        $attributes = $this->attributes;
        $type = $this->type;
        $class = Relation::getMorphedModel($type);

        if (!$class)
            throw new \Exception('Type of contract is unavailable.');

        $data = isset($attributes['variables']) ? (array)json_decode($attributes['variables']) : [];

        // Return a new instance of the specified class
        /** @var BaseContract $m */
        $m = $class::find($this->id);
        //$m = new $class($this->attributes);

        $m->fromVariables(...array_values($data));

        if (method_exists($m, 'preRender'))
            $m->{'preRender'}();

        return $m;
    }
}
