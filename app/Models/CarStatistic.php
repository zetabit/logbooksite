<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 29.11.2017
 * Time: 02:21
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CarStatistic
 *
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $cars_on_line
 * @property int $cars_on_hold
 * @property int $cars_on_service
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarStatistic whereCarsOnHold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarStatistic whereCarsOnLine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarStatistic whereCarsOnService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarStatistic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarStatistic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarStatistic whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $autopark_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CarStatistic whereAutoparkId($value)
 */
class CarStatistic extends Model
{
    public function __construct(array $attributes=[]){
        parent::__construct($attributes);
    }
    protected $fillable=['cars_on_line', 'cars_on_hold', 'cars_on_service', 'created_at', 'updated_at', 'autopark_id'];
    public $timestamps=true;
    protected $table='car_statistic';
}