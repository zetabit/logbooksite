<?php

namespace App\Models\Driver;

use App\Models\Car;
use App\Models\Driver;
use App\Models\Picture;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Driver\DriverDocumentPicture
 *
 * @property int $id
 * @property string $alt
 * @property string $slug
 * @property string $extension
 * @property int $driver_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver\DriverDocumentPicture whereAlt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver\DriverDocumentPicture whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver\DriverDocumentPicture whereDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver\DriverDocumentPicture whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver\DriverDocumentPicture whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver\DriverDocumentPicture whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver\DriverDocumentPicture whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $type
 * @property string $description
 * @property-read \App\Models\Driver $driver
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver\DriverDocumentPicture whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver\DriverDocumentPicture whereType($value)
 */
class DriverDocumentPicture extends Picture
{
    protected $table  = 'driver_document_pictures';

    public $imagePath = '/Upload/Drivers';

    public $timestamps=true;

    public function __construct(array $attributes = [], Driver $driver=null)
    {
        $this->fillable[] = 'driver_id';
        $this->fillable[] = 'type';
        $this->fillable[] = 'description';

        $this->images['image']['thumbnails'][0]['width'] = 190;
        $this->images['image']['thumbnails'][0]['height'] = 190;
        $this->images['image']['thumbnails'][0]['allow_enlarge'] = true;

        if($driver){
            $this->driver()->associate($driver);
        }
        parent::__construct($attributes);
    }

    public function driver(){
        return $this->belongsTo(Driver::class);
    }
}
