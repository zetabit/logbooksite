<?php

namespace App\Models;

use App\Models\Address;
use App\Models\User\Role;
use App\User;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\AutoPark
 *
 * @property int $id
 * @property string $name
 * @property int $address_id
 * @property-read \App\Models\Address $address
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car[] $cars
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Driver[] $drivers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AutoPark whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AutoPark whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AutoPark whereName($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CarStatistic[] $car_statistic
 */
class AutoPark extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'address_id',
        'law_address_id',
        "INN",
        "chief_fio",
        'organizational_form',
        'campaign_name',
        'gov_registration_num',
        'basis'
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [], Address $address = null)
    {
        if($address)
            $this->address()->associate($address);
        parent::__construct($attributes);
    }

    public function address() {
        return $this->belongsTo(Address::class);
    }

    public function lawAddress() {
        return $this->belongsTo(Address::class, 'law_address_id');
    }

    public function users() {
        return $this->hasMany(User::class)->with('role');
    }

    /** Какие пользователи получают уведомления о авто
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function usersForNotification() {
      $roles = Role::whereIn('name', [
        Role::ROLE_ADMIN, Role::ROLE_MANAGER,
        Role::ROLE_TECHNICAL_WORKER, Role::ROLE_ACCOUNTANT,
        Role::ROLE_UNIVERSAL
      ])->get();
      $users = $this->users()->whereIn('role_id', $roles->pluck('id')->toArray())->get();
      return $users;
    }

    public function userWithBillsCode() {
        return $this->users()->get()->first(function(User $user) {
            return strlen($user->reg_bill_token) > 0;
        });
    }

    public function cars() {
        return $this->hasMany(Car::class);
    }

    public function drivers() {
        return $this->hasMany(Driver::class);
    }

    public function car_statistic() {
        return $this->hasMany(CarStatistic::class);
    }

    public function contracts() {
        // return $this->morphToMany(BaseContract::class, 'contractable');
        return $this->hasMany(BaseContract::class, 'autopark_id');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\MorphMany|ReceiptContract|null
    //  */
    // public function contracts()
    // {
    //     return $this->morphMany(BaseContract::class, 'contractable');
    // }
}
