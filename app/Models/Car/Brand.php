<?php

namespace App\Models\Car;

use App\Models\Car;
use App\Models\Checkup;
use App\Models\Picture;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Car\Brand
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car[] $cars
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car\CarModel[] $models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Brand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Brand whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Brand extends Model
{
    protected $fillable = [
        'name'
    ];

    public function cars(){
        return $this->hasMany(Car::class);
    }

    public function models(){
        return $this->hasMany(CarModel::class);
    }
}
