<?php

namespace App\Models\Car;

use App\Models\Car;
use App\Models\Checkup;
use App\Models\Picture;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Car\CarModel
 *
 * @property int $id
 * @property string $name
 * @property int $brand_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car[] $cars
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarModel whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarModel whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarModel extends Model
{
    protected $fillable = [
        'name'
    ];

    public function cars(){
        return $this->hasMany(Car::class);
    }
}
