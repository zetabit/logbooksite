<?php

namespace App\Models\Car;

use App\Models\Car;
use App\Models\Picture;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Car\CarPartPicture
 *
 * @property int $id
 * @property string $alt
 * @property string $slug
 * @property string $extension
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPartPicture whereAlt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPartPicture whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPartPicture whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPartPicture whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPartPicture whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPartPicture whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarPartPicture extends Picture
{
    protected $table = 'car_part_pictures';

    public function __construct(array $attributes = [])
    {
        $this->images['image']['thumbnails'][0]['width'] = 190;
        $this->images['image']['thumbnails'][0]['height'] = 190;
        $this->images['image']['thumbnails'][0]['allow_enlarge'] = true;
        parent::__construct($attributes);
    }
}
