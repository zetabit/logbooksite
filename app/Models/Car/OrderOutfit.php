<?php

namespace App\Models\Car;

use App\Models\Car;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Car\OrderOutfit
 *
 * @property int $id
 * @property int $car_id
 * @property int $picture_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Car $car
 * @property-read \App\Models\Car\CarPartPicture $picture
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\OrderOutfit whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\OrderOutfit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\OrderOutfit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\OrderOutfit wherePictureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\OrderOutfit whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\OrderOutfit whereUserId($value)
 */
class OrderOutfit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'car_id',
        'picture_id',
        'user_id'
    ];

    public function car(){
        return $this->belongsTo(Car::class);
    }

    public function picture(){
        return $this->belongsTo(Car\CarPicture::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
