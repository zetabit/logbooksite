<?php

namespace App\Models\Car;

use App\Models\Car;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Car\Completeness
 *
 * @property int $id
 * @property int $back_wheel
 * @property int $fire_extinguisher
 * @property int $jack
 * @property int $first_aid_kit
 * @property int $towing
 * @property int $warning_triangle
 * @property int $radio
 * @property int $battery
 * @property int $baloon_wrench
 * @property int $none
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereBackWheel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereBaloonWrench($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereBattery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereFireExtinguisher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereFirstAidKit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereJack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereNone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereRadio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereTowing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\Completeness whereWarningTriangle($value)
 * @mixin \Eloquent
 */
class Completeness extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'back_wheel',    //Запасное колесо
        'fire_extinguisher',
        'jack',
        'first_aid_kit',
        'towing',
        'warning_triangle',
        'radio',
        'battery',
        'baloon_wrench',
        'none'
    ];
}
