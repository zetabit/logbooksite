<?php

namespace App\Models\Car;

use App\Models\Car;
use App\Models\Checkup;
use App\Models\Picture;
use App\User;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Car\CarPicture
 *
 * @property int $id
 * @property string $alt
 * @property string $slug
 * @property string $extension
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $car_id
 * @property int $checkup_id
 * @property-read \App\Models\Car $car
 * @property-read \App\Models\Checkup $checkup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPicture whereAlt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPicture whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPicture whereCheckupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPicture whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPicture whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPicture whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPicture whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPicture whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $user_id
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPicture whereUserId($value)
 * @property-read \App\Models\Car\OrderOutfit $orderOutfit
 */
class CarPicture extends Picture
{
    protected $table = 'car_pictures';

    public function __construct(array $attributes = [])
    {
        $this->fillable[] = 'checkup_id';
        $this->fillable[] = 'car_id';
        $this->fillable[] = 'user_id';

        $this->images['image']['thumbnails'][0]['width'] = 190;
        $this->images['image']['thumbnails'][0]['height'] = 190;
        $this->images['image']['thumbnails'][0]['allow_enlarge'] = true;

        parent::__construct($attributes);
    }

    public function checkup(){
        return $this->belongsTo(Checkup::class);
    }

    public function car(){
        return $this->belongsTo(Car::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function orderOutfit(){
        return $this->hasOne(OrderOutfit::class, 'picture_id');
    }

    public function isOrderOutfit(){
        return $this->orderOutfit != null;
    }
}
