<?php

namespace App\Models\Car;

use App\Models\Car;
use App\Models\Checkup\CheckupCarPart;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Car\CarPart
 *
 * @property int $id
 * @property string $name
 * @property string $icon_path
 * @property int $position
 * @property int $status
 * @property int $car_id
 * @property-read \App\Models\Car $car
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPart whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPart whereIconPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPart whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPart wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car\CarPart whereStatus($value)
 * @mixin \Eloquent
 */
class CarPart extends Model
{
    //обычный, наряд-заказ, комплектность
    //const TYPE_SIMILAR = 0, TYPE_NZ = 1, TYPE_COMPLETENESS = 2;

    const STATUS_ALL_OK = 0, STATUS_COMMENT = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'car_id',
        'name',
        'icon_path',
        'position',
        'status'
    ];

    public function __construct(array $attributes = [])
    {
        //$this->type = CarPart::TYPE_SIMILAR;
        $this->status = CarPart::STATUS_ALL_OK;
        parent::__construct($attributes);
    }

    public $timestamps = false;

    public function car(){
        return $this->belongsTo(Car::class);
    }

    public function prevPictures(){
        $result = [];
        $checkupParts = CheckupCarPart::with('picture')->where('car_part_id', '=', $this->id)->orderBy('updated_at', 'DESC')->get();
        /** @var CheckupCarPart $checkupPart */
        foreach ($checkupParts as $checkupPart) {
            $result[] = $checkupPart->picture;
        }
        return $result;
    }
}
