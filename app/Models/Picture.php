<?php

namespace App\Models;
use App\Traits\LocaleTrait;
use App\Traits\MediaTrait;
use Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;

/**
 * App\Models\Picture
 *
 * @property int $id
 * @property string $alt
 * @property string $slug
 * @property string $extension
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Picture whereAlt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Picture whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Picture whereExtension($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Picture whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Picture whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Picture whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Picture extends Model
{
    use MediaTrait;
    protected $table = 'pictures';
    protected $fillable = ['alt', 'slug', 'extension'];
    public $imagePath = 'Upload/Pictures';

    public function __construct(array $attributes = []) //необходимо при создании передавать slug_ua
    {
        $this->images['image']['if_not_upload'] = storage_path('app'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'default.jpg');
        //параметры которые всегда передаются
        //
        parent::__construct($attributes);
        //параметры которые могут не передаваться
        if( !isset($this->attributes['slug']) ) $this->attributes['slug'] = "";
        if( !isset($this->attributes['extension']) ) $this->attributes['extension'] = $this->getAttribute('extension') ? $this->getAttribute('extension') : "";

    }

    public function save(array $options = [])
    {
        if(!isset($this->attributes['alt'])) $this->attributes['alt'] = "";
                $rez = $this->uploadImage();
        //если картинку загружаем которой еще нет в БД, либо обновляем уже загруженную в бд
        if( $rez || $this->getAttribute('id')!=null )
            return parent::save($options);
        //если картинки нет в БД и не грузится картинкка, ничего не делаем
    }

    public function delete()
    {
        $this->deleteImages();
        return parent::delete();
    }
}
