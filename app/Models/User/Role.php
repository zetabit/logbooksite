<?php

namespace App\Models\User;

use App\User;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\User\Role
 *
 * @property int $id
 * @property string $name
 * @property string $readable_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission $permission
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Role whereReadableName($value)
 * @mixin \Eloquent
 * @property int $public
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Role wherePublic($value)
 */
class Role extends Model
{
    const ROLE_ADMIN = 'ROLE_ADMIN',    //full access to alligned auto park
        ROLE_MANAGER = 'ROLE_MANAGER',  //only have access to alligned auto park
        ROLE_TECHNICAL_WORKER = 'ROLE_TECHNICAL_WORKER',
        ROLE_ACCOUNTANT = 'ROLE_ACCOUNTANT',
        ROLE_UNIVERSAL = 'ROLE_UNIVERSAL';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'readable_name', 'public'
    ];

    public static function getRoleId($name=""){
        $role = self::where('name',$name)->first();
        return $role->id;
    }

    public function users(){
        return $this->hasMany(User::class);
    }

    public function permission(){
        return $this->hasOne('App\Models\Permission');
    }
    public $timestamps = false;

    public function text(){
      $texts = [
        self::ROLE_ADMIN => 'Администратор',
        self::ROLE_MANAGER => 'Менеджер',
        self::ROLE_TECHNICAL_WORKER => 'Технический спец.',
        self::ROLE_ACCOUNTANT => 'Бухгалтер',
        self::ROLE_UNIVERSAL => 'Универсальный',
      ];
      return isset($texts[$this->name]) ? $texts[$this->name] : '<unknown>';
    }
}
