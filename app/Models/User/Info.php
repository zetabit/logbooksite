<?php

namespace App\Models\User;

use App\User;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\User\Info
 *
 * @property int $id
 * @property string $FirstName
 * @property string $LastName
 * @property-read \App\User $user
 * @mixin \Eloquent
 * @property string $firstname
 * @property string $lastname
 * @property string $patronymic
 * @property string $phone
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Info whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Info whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Info wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Info wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Info whereId($value)
 */
class Info extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'patronymic',
        'phone',
        'additional_phone'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne || User || null
     */
    public function user(){
        return $this->hasOne(User::class);
    }
    public function fio(){
        return $this->lastname.' '.mb_substr($this->firstname,0,1).'. '.mb_substr($this->patronymic,0,1).'.';
    }
}
