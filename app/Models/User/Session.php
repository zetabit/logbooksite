<?php

namespace App\Models\User;

use App\User;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\User\Session
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property string $api_token
 * @property \Carbon\Carbon $expired
 * @property int $last_ip
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Session whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Session whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Session whereExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Session whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Session whereLastIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Session whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Session whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Session whereUserId($value)
 * @mixin \Eloquent
 */
class Session extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'token',
        'api_token',
        'expired',
        'last_ip',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'expired'
    ];

    protected $hidden = [
        'user_id'
    ];

    public $timestamps = true;

    public function __construct(array $attributes = [], User $user = null)
    {
        if($user) $this->user()->associate($user);
        parent::__construct($attributes);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function genToken()
    {
        return (string)random_bytes(64);
    }
}
