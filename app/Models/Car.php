<?php

namespace App\Models;

use App\Models\Car\Brand;
use App\Models\Car\CarModel;
use App\Models\Car\CarPart;
use App\Models\Car\CarPicture;
use App\Models\Car\OrderOutfit;
use App\Traits\AbbreviationTrait;
use App\Traits\Notifiable;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Car
 *
 * @property int $id
 * @property string $model
 * @property string $number
 * @property \App\Models\Color $color
 * @property int $run
 * @property int $run_closest
 * @property int $total_state
 * @property int $auto_park_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $color_id
 * @property int $run_total
 * @property string $sts
 * @property string $vin
 * @property int $brand_id
 * @property int $model_id
 * @property int $status
 * @property-read \App\Models\AutoPark $AutoPark
 * @property-read \App\Models\Car\Brand $brand
 * @property-read \App\Models\Car\CarModel $carModel
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Checkup[] $checkups
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Driver[] $drivers
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car\OrderOutfit[] $orderOutfits
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car\CarPart[] $parts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car\CarPicture[] $pictures
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereAutoParkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereColorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereRun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereRunClosest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereRunTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereSts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereTotalState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereVin($value)
 * @mixin \Eloquent
 * @property int $run_last
 * @property int $active
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereRunLast($value)
 * @property-read mixed $generate_abbreviation
 */
class Car extends Model
{
    use Notifiable, AbbreviationTrait;

    //      простой             на линии            на ремонте
    const STATUS_ON_HOLD = 0, STATUS_ON_LINE = 1, STATUS_ON_SERVICE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'model_id',
        'brand_id',
        'number',
        'auto_park_id',
        'color_id',
        'run',          //пробег ТО delta
        'run_closest',  //пробег до ближайшего ТО
        'run_total',    //общий пробег
        'run_last', //пробег последнегоь ТО - потому что так в макете админки TODO: эту лабутню надо убрать, т.к. всё просчитается спокойно и без этого поля (до ближайшего ТО минус ТО дельта)
        'total_state',
        'sts',
        'vin',
        'status',
        'active',
        'manufacture_year', //год выпуска
        'mtpl_insurance_number',    //номер полиса ОСАГО
        'mtpl_insurance_to',        //ОСАГО действует до дата
    ];

    protected $casts = [
        'mtpl_insurance_to' => Carbon::class
    ];

    public function AutoPark(){
        return $this->belongsTo(AutoPark::class);
    }

    public function color(){
        return $this->belongsTo(Color::class, 'color_id');
    }

    /**
     * Parts of the car
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parts(){
        return $this->hasMany(CarPart::class)->orderBy('position', 'ASC');
    }

    public function pictures(){
        return $this->hasMany(CarPicture::class)->with('orderOutfit');   // TODO: исключить orderOutfits
    }

    public function drivers(){
        return $this->belongsToMany(Driver::class);
    }
    public function driver(){
        return $this->getLastCheckupDriver();
        // return $this->drivers()->orderBy('car_driver.created_at', 'DESC')->first();
    }

    public function checkups(){ //orderBy used in api
        return $this->hasMany(Checkup::class)->orderBy('id', 'desc');
    }

    /** Заказ-наряды
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderOutfits($params = []){

        $relation = $this->hasMany(OrderOutfit::class);

        if(isset($params['only_today']) && $params['only_today'])
          $relation->where('created_at', '>=', 'DATE_SUB(NOW(), INTERVAL 1 DAY)');

        return $relation;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | \App\Models\Car\Brand
     */
    public function brand() {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | \App\Models\Car\CarModel | null
     */
    public function carModel() {
        return $this->belongsTo(CarModel::class, 'model_id');
    }

    public function getBrandName() {
        return $this->brand !== null ? $this->brand->name : '';
    }

    public function getModelName() {
        return $this->carModel !== null ? $this->carModel->name : '';
    }

    public function getModelBrand() {
      return $this->getModelName() .' '. $this->getBrandName();
    }

    public function initParts(){
        $parts = $this->parts;
        if(sizeof($parts) <= 0) {
            $car = $this;
            //fill parts
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Передняя часть',
                'icon_path' => 'front_end.svg',
                'position' => 1
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Левое переднее крыло',
                'icon_path' => 'left_front_wing.svg',
                'position' => 2
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Левая боковая часть',
                'icon_path' => 'left_side.svg',
                'position' => 3
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Левое заднее крыло',
                'icon_path' => 'left_rear_wing.svg',
                'position' => 4
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Правое заднее крыло',
                'icon_path' => 'right_rear_wing.svg',
                'position' => 7
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Правая боковая часть',
                'icon_path' => 'right_lateral_part.svg',
                'position' => 8
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Правое переднее крыло',
                'icon_path' => 'right_front_wing.svg',
                'position' => 9
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Задняя часть',
                'icon_path' => 'rear_part.svg',
                'position' => 5
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Открытый багажник',
                'icon_path' => 'opened_trunk.svg',
                'position' => 6
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Задняя часть салона',
                'icon_path' => 'rear_compartment.svg',
                'position' => 10
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Передняя часть салона',
                'icon_path' => 'front_compartment.svg',
                'position' => 11
            ]);
            \App\Models\Car\CarPart::firstOrCreate([
                'car_id' => $car->id,
                'name' => 'Приборная панель',
                'icon_path' => 'side_panel.svg',
                'position' => 12
            ]);
            //            \App\Models\Car\CarPart::firstOrCreate([
            //                'car_id' => $car->id,
            //                'name' => 'Наряд-заказ',
            //                'icon_path' => 'order_outfits.svg',
            //                'position' => 13,
            //                'type' => CarPart::TYPE_NZ
            //            ]);
            //            \App\Models\Car\CarPart::firstOrCreate([
            //                'car_id' => $car->id,
            //                'name' => 'Комплектность',
            //                'icon_path' => 'completeness.svg',
            //                'position' => 14,
            //                'type' => CarPart::TYPE_COMPLETENESS
            //            ]);
        }
    }

    /**
     * @return Checkup|null
     */
    public function getLastCheckup($type = null){
        $checkups = $this->checkups !== null ? $this->checkups()->latest() : null;
        if ($type) {
            $checkups->where('type', Checkup::TYPE_TRANSMIT);
        }
        return $checkups->first();
    }

    /**
     * @return Driver|null
     */
    public function getLastCheckupDriver() {
      $checkup = $this->getLastCheckup();
      return $checkup ? $checkup->driver : null;
    }

    public function getCarStatus(){
        switch ($this->status){
            case self::STATUS_ON_HOLD:
                return 'в простое';
            case self::STATUS_ON_LINE:
                return 'на линии';
            case self::STATUS_ON_SERVICE:
                return 'в ремонте';

        }
    }
}
