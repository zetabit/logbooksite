<?php

namespace App\Models;

use App\Models\Car\Completeness;
use App\Models\Car\CarPicture;
use App\Models\Checkup\CheckupCarPart;
use App\Traits\AbbreviationTrait;
use App\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Checkup
 *
 * @property int $id
 * @property int $run
 * @property int $total_state
 * @property int $car_id
 * @property int $driver_id
 * @property int $user_id
 * @property int $finished
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $car_completeness_id
 * @property int $type
 * @property int $receipt_type
 * @property-read \App\Models\Car $car
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car\CarPicture[] $carPictures
 * @property-read \App\Models\Car\Completeness $completeness
 * @property-read \App\Models\Driver $driver
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Checkup\CheckupCarPart[] $parts
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup receipt()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup review()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup transmit()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereCarCompletenessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereFinished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereReceiptType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereRun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereTotalState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup finished()
 * @property int $to_complete
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup whereToComplete($value)
 * @property-read mixed $generate_abbreviation
 * @property-read mixed $type_text
 */
class Checkup extends Model
{
    use AbbreviationTrait;

    /**       осмотр           прием            передача        OrderOutfit,Заказ-Наряд  */
    const TYPE_REVIEW = 0, TYPE_RECEIPT = 1, TYPE_TRANSMIT = 2, TYPE_NZ = 3;

    /** причина акта приема */
    /**                         на ремонт          увольнение  */
    const RECEIPT_NONE = 0, RECEIPT_SERVICE = 1, RECEIPT_DISMISSAL = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'car_id',
        'driver_id',
        'run',        // см. car->run_total
        'total_state',// sm. car->total_state
        'finished',
        'car_completeness_id',
        'type',
        'receipt_type',
        'to_complete',  //ТО было пройдено
    ];

    public function __construct(array $attributes = [])
    {
        $this->total_state = 0;
        $this->run = 0;
        $this->type = self::TYPE_REVIEW;
        $this->receipt_type = self::RECEIPT_NONE;
        parent::__construct($attributes);
    }

  /**
   * @param $val
   * @return $this
   * @throws \Exception
   */
    public function setType($val)
    {
      $avail = [self::TYPE_REVIEW, self::TYPE_RECEIPT, self::TYPE_TRANSMIT];
      if(!in_array($val, $avail)) throw new \Exception('new Type is not available'); //not change type
      else $this->type = $val;
      return $this;
    }

  /**
   * @param $val
   * @return $this
   * @throws \Exception
   */
    public function setReceiptType($val)
    {
      $avail = [self::RECEIPT_NONE, self::RECEIPT_SERVICE, self::RECEIPT_DISMISSAL];
      if(!in_array($val, $avail)) throw new \Exception('new Receipt Type is not available'); //not change type
      else $this->receipt_type = $val;
      return $this;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function car(){
        return $this->belongsTo(Car::class);
    }

    public function driver(){
        return $this->belongsTo(Driver::class);
    }

    public function carPictures(){
        return $this->hasMany(CarPicture::class);   // TODO: исключить orderOutfits
    }

    public function orderOutfits(){
        return $this->carPictures->filter(function($item){
            return $item->orderOutfit != null;
        });
    }

    /**
     * Parts of the car
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|CheckupCarPart[]
     */
    public function parts(){
        return $this->hasMany(CheckupCarPart::class);
    }

    public function completeness(){
        return $this->belongsTo(Completeness::class, 'car_completeness_id');
    }

    public function isHaveNotificatedParts() {
        /** @var CheckupCarPart $part */
        foreach ($this->parts as $part) {
            if($part->notification)
                return true;
        }
        return false;
    }

    public function scopeReview(Builder $builder) {
      return $builder->where('type', '=', self::TYPE_REVIEW);
    }

    public function scopeReceipt(Builder $builder) {
      return $builder->where('type', '=', self::TYPE_RECEIPT);
    }

    public function scopeTransmit(Builder $builder) {
      return $builder->where('type', '=', self::TYPE_TRANSMIT);
    }

    public function scopeFinished(Builder $builder) {
      return $builder->where('finished', '=', true);
    }

    public function getTypeTextAttribute()
    {
        if ($this->type == \App\Models\Checkup::TYPE_RECEIPT)
            return 'Акт приема';
        elseif ($this->type == \App\Models\Checkup::TYPE_TRANSMIT)
            return 'Акт передачи';
        elseif ($this->type == \App\Models\Checkup::TYPE_REVIEW)
            return 'Осмотр';
        elseif ($this->type == \App\Models\Checkup::TYPE_NZ)
            return 'Заказ-Наряд';

    }
}
