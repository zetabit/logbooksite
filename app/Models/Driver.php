<?php

namespace App\Models;

use App\Models\Car\CarPart;
use App\Models\Car\CarPicture;
use App\Models\Driver\DriverDocumentPicture;
use App\Traits\AbbreviationTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Driver
 *
 * @property int $id
 * @property string $fio
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AutoPark[] $AutoParks
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car[] $cars
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Driver\DriverDocumentPicture[] $documentPictures
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $auto_park_id
 * @property string $firstname
 * @property string $lastname
 * @property string $patronymic
 * @property string $main_phone
 * @property string $additional_phone
 * @property string $registration_address
 * @property string $document_number
 * @property string $document_date
 * @property-read \App\Models\AutoPark $autoPark
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereAdditionalPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereAutoParkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereDocumentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereMainPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereRegistrationAddress($value)
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Driver whereActive($value)
 * @property-read mixed $generate_abbreviation
 */
class Driver extends Model
{
    use AbbreviationTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'patronymic',
        'main_phone',
        'additional_phone',
        'registration_address',   //место регистрации
        'document_number',   //номер вод. уд.,
        'document_date',
        'auto_park_id',
        'active'
    ];
    public static $image_path='/Upload/Drivers';
    public $timestamps=true;
    public function __construct(array $attributes = [], AutoPark $autoPark=null)
    {
        if($autoPark){
            $this->autoPark()->associate($autoPark);
        }
        parent::__construct($attributes);
    }

    public function autoPark(){
        return $this->belongsTo(AutoPark::class);
    }

    public function cars(){
        return $this->belongsToMany(Car::class)->withTimestamps();
    }

    public function documentPictures(){
        return $this->hasMany(DriverDocumentPicture::class);
    }

    public function fio(){
        return $this->lastname.' '.mb_substr($this->firstname,0,1).'. '.mb_substr($this->patronymic,0,1).'.';
    }
    public function getFioAttribute(){
        return $this->fio();
    }
    public function imageSlug(){
        return $this->transliterate($this->firstname.'_'.$this->lastname);
    }
    protected function transliterate($textcyr = null, $textlat = null) {
        $cyr = array(
            'ё',  'ж',  'х',  'ц',  'ч',  'щ',   'ш',  'ъ',  'э',  'ю',  'я', 'ы', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'ь',
            'Ё',  'Ж',  'Х',  'Ц',  'Ч',  'Щ',   'Ш',  'Ъ',  'Э',  'Ю',  'Я', 'Ы', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Ь');
        $lat = array(
            'yo', 'zh', 'kh', 'ts', 'ch', 'shh', 'sh', '``', 'eh', 'yu', 'ya', 'Y', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', '`',
            'Yo', 'Zh', 'Kh', 'Ts', 'Ch', 'Shh', 'Sh', '``', 'Eh', 'Yu', 'Ya', 'y','A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', '`');
        if($textcyr)
            return mb_strtolower(str_replace($cyr, $lat, $textcyr));
        else if($textlat)
            return mb_strtolower(str_replace($lat, $cyr, $textlat));
        else
            return null;
    }
}
