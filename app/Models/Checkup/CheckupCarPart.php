<?php

namespace App\Models\Checkup;

use App\Models\Car;
use App\Models\Checkup;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\Models\Checkup\CheckupCarPart
 *
 * @property int $id
 * @property string $name
 * @property int $checkup_id
 * @property int $car_part_id
 * @property int $car_part_picture_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property-read \App\Models\Car\CarPart $carPart
 * @property-read \App\Models\Checkup $checkup
 * @property-read \App\Models\Car\CarPartPicture $picture
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup\CheckupCarPart whereCarPartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup\CheckupCarPart whereCarPartPictureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup\CheckupCarPart whereCheckupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup\CheckupCarPart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup\CheckupCarPart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup\CheckupCarPart whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checkup\CheckupCarPart whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
class CheckupCarPart extends Model
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'checkup_id',
        'car_part_id',
        'car_part_picture_id',
        'name'
    ];

    public $timestamps = false;

    public function checkup(){
        return $this->belongsTo(Checkup::class);
    }

    public function carPart(){
        return $this->belongsTo(Car\CarPart::class);
    }

    public function picture(){
        return $this->belongsTo(Car\CarPartPicture::class, 'car_part_picture_id');
    }
}
