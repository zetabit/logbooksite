<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 03.12.2017
 * Time: 00:29
 */

namespace App\Models;
use App\Models\User\Role;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Permission
 *
 * @property int $id
 * @property int $role_id
 * @property int $cars_view
 * @property int $cars_edit
 * @property int $managers_view
 * @property int $managers_edit
 * @property int $drivers_view
 * @property int $drivers_edit
 * @property int $tasks_view
 * @property int $tasks_edit
 * @property int $fines_view
 * @property int $fines_edit
 * @property int $notifications_view
 * @property int $notifications_edit
 * @property int $statistic_view
 * @property int $statistic_edit
 * @property int $archive_view
 * @property int $archive_edit
 * @property int $lists_view
 * @property int $lists_edit
 * @property int $checkups_view
 * @property int $checkups_edit
 * @property int $acts_view
 * @property int $acts_edit
 * @property int $cars_add
 * @property int $managers_add
 * @property int $drivers_add
 * @property int $tasks_add
 * @property int $fines_add
 * @property int $notifications_add
 * @property int $statistic_add
 * @property int $archive_add
 * @property int $lists_add
 * @property int $checkups_add
 * @property int $acts_add
 * @property-read \App\Models\User\Role $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereActsAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereActsEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereActsView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereArchiveAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereArchiveEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereArchiveView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCarsAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCarsEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCarsView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCheckupsAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCheckupsEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCheckupsView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereDriversAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereDriversEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereDriversView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereFinesAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereFinesEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereFinesView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereListsAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereListsEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereListsView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereManagersAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereManagersEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereManagersView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereNotificationsAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereNotificationsEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereNotificationsView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereStatisticAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereStatisticEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereStatisticView($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereTasksAdd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereTasksEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereTasksView($value)
 * @mixin \Eloquent
 */
class Permission extends Model
{
    protected $fillable=[
        'role_id',
        'cars_view','cars_edit','cars_add',
        'managers_view','managers_edit','managers_add',
        'drivers_view','drivers_edit','drivers_add',
        'tasks_view','tasks_edit','tasks_add',
        'fines_view','fines_edit','fines_add',
        'notifications_view','notifications_edit','notifications_add',
        'statistic_view','statistic_edit','statistic_add',
        'archive_view','archive_edit','archive_add',
        'lists_view','lists_edit','lists_add',
        'checkups_view','checkups_edit','checkups_add',
        'acts_view','acts_edit','acts_add'
        ];
    public $timestamps=false;
    public function __construct(array $attributes = [], Role $role=null)
    {
        parent::__construct($attributes);
        if($role){
            $this->role()->associate($role);
        }
    }
    public function role(){
       return $this->belongsTo('App\Models\User\Role');
    }
}