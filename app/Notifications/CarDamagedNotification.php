<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 23.11.17
 * Time: 11:09
 */

namespace App\Notifications;


use App\Models\Car;
use App\Models\Checkup\CheckupCarPart;
use App\Traits\NotificationCustom;
use App\Traits\NotificationCustomConstruct;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use function PHPSTORM_META\type;

class CarDamagedNotification extends Notification
{
    use Queueable, NotificationCustomConstruct;

    /** @var  Car */
    public $car;
    /** @var  CheckupCarPart */
    public $checkupCarPart;
    /** @var  User */
    public $user;

    protected $args = [
        'car' => [                  //variable
            'key' => 'car_id',      //db key
            'type' => Car::class    //class name
        ],
        'checkupCarPart' => [
            'key' => 'checkupCarPart_id',
            'type' => CheckupCarPart::class
        ],
        'manager' => [
            'key' => 'manager_id',
            'type' => User::class
        ]
    ];

}