<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 23.11.17
 * Time: 11:09
 */

namespace App\Notifications;


use App\Models\Car;
use App\Models\Checkup\CheckupCarPart;
use App\Traits\NotificationCustom;
use App\Traits\NotificationCustomConstruct;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use function PHPSTORM_META\type;

/**
 * Car the technical review is needs.
 * Class CarTRNotification
 * @package App\Notifications
 */
class CarTRNotification extends Notification
{
    use Queueable, NotificationCustomConstruct;

    /** @var  Car */
    public $car;

    /** @var integer */
    public $oldRunClosest;

    /** @var integer */
    public $oldRunTotal;

    protected $args = [
        'car' => [                  //variable
            'key' => 'car_id',      //db key
            'type' => Car::class    //class name
        ],
        'oldRunTotal'   => [
            'type' => 'integer'
        ],
        'oldRunClosest' => [
            'type' => 'integer'
        ]
    ];

    public function afterConstruct()
    {
        $this->oldRunClosest = $this->oldRunClosest ? $this->oldRunClosest : $this->car->run_closest;
        $this->oldRunTotal = $this->oldRunTotal ? $this->oldRunTotal : $this->car->run_total;
    }

    public function preRender()
    {
        //$this->car->run_closest = $this->oldRunClosest ? $this->oldRunClosest : $this->car->run_closest;
        //$this->car->run_total   = $this->oldRunTotal ? $this->oldRunTotal : $this->car->run_total;
    }

}