<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        /** adds some validation staff, ex:
        $this->app['validator']->extend('numericarray', function ($attribute, $value, $parameters)
        {
            foreach ($value as $v) {
                if (!is_int($v)) {
                    return false;
                }
            }
            return true;
        });
         */
    }

    public function register()
    {
        //
    }
}
