<?php

namespace App\Providers;

use App\Models\User\Session;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            /** @var Request $request */
            $header = $request->header('Api-Token');
            if($header) {
                $session = Session::where('api_token', '=', $header)->first();
                if($request->is('logout')) {
                    $session->delete();
                    $session = false;
                }
                return $session ? $session->user : null;
            }

            $cookie = $request->cookie('auth_token');
            if($cookie) {
                $session = Session::where('token', '=', $cookie)->first();
                if($request->is('logout')) {
                    $session->delete();
                    $session = false;
                }
                return $session ? $session->user : null;
            }
        });
    }
}
