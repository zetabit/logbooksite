<?php

namespace App\Providers;

use App\Models\Contracts\ReceiptContract;
use App\Models\Contracts\TransmitContract;
use App\Repositories\AuthRepository;
use App\Repositories\Contracts\AuthRepositoryInterface;
use App\Services\BillsService;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->contractTypes();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthRepositoryInterface::class, AuthRepository::class);

        $this->bills();
    }

    protected function bills()
    {
        $this->app->singleton(BillsService::class, function ($app) {
            return new BillsService();
        });
    }

    protected function contractTypes()
    {
        Relation::morphMap([
            'act_receipt'   => ReceiptContract::class,
            'act_transmit'  => TransmitContract::class,
        ]);
    }
}
