<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 10.12.2017
 * Time: 01:59
 */

namespace App\Listeners\Checkup;


use App\Events\Checkup\CarDamagedEvent;
use App\Notifications\CarDamagedNotification;

class EventCarDamagedListener
{
    public function __construct()
    {

    }
    public function handle(CarDamagedEvent $event)
    {
        $checkupCarPart = $event->checkupCarPart;
        $car = $event->car;
        $manager = $event->manager;
        /** @var \App\User[] $notifiers */
        $notifiers = $manager->autoPark->usersForNotification();
        /** @var \App\User $notifier */
        foreach ($notifiers as $notifier) {
            $notifier->notify(new CarDamagedNotification($car, $checkupCarPart,$manager));
        }
        $car->notify(new CarDamagedNotification($car, $checkupCarPart,$manager));
        $checkupCarPart->notify(new CarDamagedNotification($car, $checkupCarPart,$manager));
    }
}