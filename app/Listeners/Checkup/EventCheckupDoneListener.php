<?php

namespace App\Listeners\Checkup;

use App\Events\Checkup\CarDamagedEvent;
use App\Events\Checkup\CheckupDoneEvent;
use App\Events\ExampleEvent;
use App\Models\Car;
use App\Models\Car\CarPart;
use App\Models\Checkup;
use App\Models\Checkup\CheckupCarPart;
use App\Models\User\Role;
use App\Notifications\CarTRNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventCheckupDoneListener
{
    /**
     * Handle the event.
     *
     * @param  CheckupDoneEvent  $event
     * @return void
     */
    public function handle(CheckupDoneEvent $event)
    {
      $checkup = $event->checkup;
      $car = $checkup->car;
      $distance = $checkup->run;
      /** @var \App\User $user */
      $user = $checkup->user;
      /** @var \App\Models\AutoPark $autoPark */
      $autoPark = $user->autoPark;
      /** @var \App\User[] $notifiers */
      $notifiers = $autoPark->usersForNotification();

      //check part statuses
      /** @var CheckupCarPart $part */
      foreach ($checkup->parts as $part) {
        if(strlen($part->picture->alt) > 0) {
          $part->carPart->status = CarPart::STATUS_COMMENT;
          event(new CarDamagedEvent($part, $user, $car));   //рассылаем всем кому надо
        } else {
          $part->carPart->status = CarPart::STATUS_ALL_OK;
        }
        $part->carPart->save();
      }

      // set distance
      if($distance > 0 && $distance > $checkup->car->run_total) {
        $car->run_total = $distance;
      }

      if ($checkup->type == Checkup::TYPE_NZ && $checkup->to_complete) {
          $car->run_closest = $car->run_closest + $car->run;    //след ТО = текущий пробег + дельта пробега
      }

      if ($checkup->total_state > 0)    //can be not set (ex. ourderOutfit checkup)
        $car->total_state = $checkup->total_state;  //state of car

      if($car->run_total >= ($car->run_closest - 1000)){  //check needs for technical review or not


        /** @var \App\User $notifier */
        foreach ($notifiers as $notifier) {
          $notifier->notify(new CarTRNotification($car));
        }
        $car->notify(new CarTRNotification($car));
      }

      if ($checkup->parts->count() > 0) {   //if checkup not empty (empty checkup can be created with orderOutfits)
      }
      //upd car status
      if($checkup->type == Checkup::TYPE_TRANSMIT) {
          $car->status = Car::STATUS_ON_LINE;
      } elseif ($checkup->type == Checkup::TYPE_RECEIPT) {
          if($checkup->receipt_type == Checkup::RECEIPT_SERVICE)
              $car->status = Car::STATUS_ON_SERVICE;
          else
              $car->status = Car::STATUS_ON_HOLD;
      }
      $car->save();
    }
}
