<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 22.03.18
 * Time: 23:06
 */

namespace App\Interfaces;


interface BaseContractInterface
{

    public function getTypeTemplateAttribute();

    public function setDataTemplate();

    public function getDataTemplateAttribute();

}