<?php
/**
 * Created by PhpStorm.
 * User: zetabit
 * Date: 16.02.18
 * Time: 23:37
 */

namespace App\Services;


use App\Exceptions\ApiMustReauthorizedException;
use App\Exceptions\ApiNotAuthorizedException;
use App\Models\Car;
use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class BillsService
{
    private $client = null;

    private $apiPoint = '';

    /** @var User $user */
    private $user = null;

    /**
     * ApiService constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $config = [];
        if (getenv('APP_LOG_LEVEL') == 'debug') {
            $config = array_merge($this->initLogMessageFormatter(), $config);
        }
        $this->client = new Client($config);

        $this->apiPoint = env('BILLS_URL') . '/';

        $this->getAuthUser();

        return $this;
    }

    public function getAuthUser(){
        // use this user for auth in bills service
        $this->user = \Auth::user()->autoPark->userWithBillsCode();   //TODO: remove this fucking costil
    }


    /**
     * @return array
     * @throws \Exception
     */
    private function initLogMessageFormatter()
    {
        $config = [];

        $stack = HandlerStack::create();
        $logger = new Logger('Logger');
        $logger->pushHandler(new StreamHandler(storage_path('logs/guzzle-' . date('Y-m-d') . '.log')));

        $stack->push(
          Middleware::log(
            $logger,
            new MessageFormatter('{method} {target} {req_body} - {res_body}')
          )
        );

        $config['handler'] = $stack;
        return $config;
    }



    /**
     * @param $method
     * @param $fields
     * @return \Psr\Http\Message\ResponseInterface
     * @throws ApiNotAuthorizedException
     */
    private function jsonPost($method, $fields)
    {
        $body = json_encode($fields, JSON_PRETTY_PRINT);

        Log::info('Api POST ' . $method . " request: " . $body);

        $response = $this->jsonPostToUrl($this->apiPoint . $method, $body);

        Log::info('Api POST ' . $method . " response: " . $response->getBody());

        return $response;
    }

    /**
     * @param $url
     * @param $body
     * @return \Psr\Http\Message\ResponseInterface
     * @throws ApiNotAuthorizedException
     */
    private function jsonPostToUrl($url, $body)
    {
        $this->getAuthUser();

        $headers = ['Content-Type' => 'application/json', 'Accept' => 'application/json'];

        if ($this->user && strlen($this->user->bill_token) > 0) {
            $headers['Bearer'] = $this->user->bill_token;
        }

        $response = $this->client->post($url, [
          'headers' => $headers,
          'body' => $body,
        ]);

        $data = json_decode($response->getBody(), true);

        if(isset($data['result']) && $data['result'] == 'fail' && isset($data['action']) && $data['action'] == 'login') {
            throw new ApiNotAuthorizedException('User sid is old?');
        }

        return $response;
    }

    /**
     * @param       $method
     * @param array $fields
     *
     * @return array|string
     * @throws ApiNotAuthorizedException
     * @throws ApiMustReauthorizedException
     */
    private function jsonGet($method, $fields = [])
    {
        $this->getAuthUser();

        Log::info('Api GET ' . $method . " " . json_encode($fields));

        $headers = ['Accept' => 'application/json'];

        if ($this->user && strlen($this->user->bill_token) > 0) {
            $headers['Bearer'] = $this->user->bill_token;
        }

        $response = $this->client->get($this->apiPoint . $method, [
          'query' => $fields,
          'headers' => $headers,
        ]);

        $data = json_decode($response->getBody(), true);

        if(isset($data['result']) && $data['result'] == 'fail' && isset($data['action']) && $data['action'] == 'login') {
            throw new ApiNotAuthorizedException('User sid is old?');
        }

        if (isset($data['error'])) {

            if ($data['error'] == 'Token is Invalid') {
                $data = "Вы не авторизованы в сервисе штрафов";
            } elseif ($data['error'] == 'Token is Expired') {

                \Auth::user()->bill_token = "";
                \Auth::user()->save();

                throw new ApiMustReauthorizedException('User token is old, reauth');
            }

        }

        Log::info('Api GET ' . $method . " response: " . $response->getBody());

        return $data;

    }

    /**
     * @param Car  $car
     * @param bool $bForce
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws ApiMustReauthorizedException
     * @throws ApiNotAuthorizedException
     * @throws \Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getBillsForCar(Car $car, $bForce = false)
    {
        $carNumber = $car->number;
        $this->getAuthUser();
        $cached = \Cache::get('bills_for_car_' . $carNumber);
        $res = [];

        if($bForce || is_string($cached) || !$cached) {
            $res = $this->jsonGet('getBillsForCar/'.$carNumber, []);

            if (is_string($res)) {

            } elseif (isset($res['reason']) && $res['reason'] == 'Car not found.') {
                $res = $this->addCar($car);
                $res = is_string($res) ? $res : null;
            } else {
                $res = isset($res['bills']) ? $res['bills'] : null;
            }

            \Cache::set('bills_for_car_' . $carNumber, $res, new \DateInterval("P1M"));
        } else
            $res = $cached;

        return $res;
    }

    public function addCar(Car $car)
    {
        $this->getAuthUser();

        $res = $this->jsonPost('addCar', [
            'number'    => $car->number,
            'vin'       => strlen($car->vin) ? $car->vin : " ",
            'sts'       => $car->sts
        ]);

        $data = json_decode($res->getBody(), true);

        if (isset($data['reason']) && $data['reason'] == 'Not enough accounts') {
            return 'Не хватает аккаунтов';
        }

        return $data['result'] == 'ok';
    }

    /**
     * Authorize user with auth token.
     *
     * @param User $user
     *
     * @throws ApiNotAuthorizedException
     */
    public function auth(User $user)
    {
        $this->getAuthUser();

        $res = null;

        $res = $this->jsonPost(
          'login',
          [
            'email'    => $user->email,
            'password' => $user->reg_bill_token
          ]
        );

        $data = $res ? json_decode($res->getBody(), true) : [];

        if (! isset($data['access_token']) && isset($data['error']) && $data['error'] == 'invalid_credentials') {
            $data = $this->register($user);
            if (strlen($data) > 0)
                $user->bill_token = $data;
        } elseif (isset($data['access_token']) && isset($data['token_type']) && $data['token_type'] == 'bearer' ) {
            if (strlen($data['access_token']) > 0)
                $user->bill_token = $data['access_token'];
        }

        $user->save();
    }

    /**
     * Register user and save auth bill token to entity.
     *
     * @param User $user
     *
     * @return bool
     * @throws ApiNotAuthorizedException
     */
    public function register(User $user)
    {
        $this->getAuthUser();

        $res = $this->jsonPost('register', [
            'email' =>  $user->email,
            'reg_token'=>$user->reg_bill_token
        ]);

        $data = json_decode($res->getBody(), true);

        if (! isset($data['result']) || $data['result'] != 'ok' || !isset($data['token'])) {
            return false;
        }

        $user->bill_token = $data['token'];
        $user->save();
        return true;
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws ApiNotAuthorizedException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function getCarsBills()
    {
        $this->getAuthUser();

        $cached = \Cache::get('getCarsBills');
        $res = [];

        if(!$cached) {
            $res = $this->jsonGet('getCarsBills', []);

            \Cache::set('getCarsBills', $res, new \DateInterval("P1M"));
        } else
            $res = $cached;

        return $res;
    }

    /**
     * @return string|array
     * @throws ApiNotAuthorizedException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function getBillsCar()
    {
        $this->getAuthUser();
        $user = $this->user;

        $data = $this->jsonGet('getBillsCar');

        return isset($data['bills']) ? $data['bills'] : $data;
    }

}