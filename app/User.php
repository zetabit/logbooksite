<?php

namespace App;

use App\Models\Address;
use App\Models\AutoPark;
use App\Models\User\Info;
use App\Models\User\Role;
use App\Models\User\Session;
use App\Traits\AbbreviationTrait;
use Illuminate\Auth\Authenticatable;
use App\Traits\Notifiable;
use Illuminate\Support\Collection;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $info_id
 * @property int $address_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AutoPark $AutoPark
 * @property-read \App\Models\Address $address
 * @property-read \App\Models\User\Info $info
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Role $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Session[] $sessions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInfoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @mixin \Eloquent
 * @property int $role_id
 * @property int $auto_park_id
 * @property-read \App\Models\AutoPark $autoPark
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAutoParkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleId($value)
 * @property string $bill_token
 * @property string $reg_bill_token
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBillToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRegBillToken($value)
 * @property-read mixed $generate_abbreviation
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, Notifiable, AbbreviationTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name', 'email', 'info_id','username', 'role_id', 'auto_park_id',
        'bill_token', 'reg_bill_token', 'active', 'activation_code'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function __construct(array $attributes = [], Info $info = null, Role $role = null, AutoPark $autopark=null)
    {
        if($info)
            $this->info()->associate($info);
        if($role)
            $this->role()->associate($role);
        if($autopark)
            $this->autoPark()->associate($autopark);

        parent::__construct($attributes);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function roleText()
    {
      return $this->role ? $this->role->text() : '<unknown>';
    }

    public function sessions(){
        return $this->hasMany(Session::class);
    }

    public function info(){
        return $this->belongsTo(Info::class);
    }

    public function autoPark(){
        return $this->belongsTo('App\Models\AutoPark');
    }

    public function cars(){
        return $this->autoPark->cars()->with('color', 'brand', 'carModel')->where('active', true)->get();
    }
}
