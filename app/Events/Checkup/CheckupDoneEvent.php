<?php

namespace App\Events\Checkup;

use App\Events\Event;
use App\Models\Checkup;

class CheckupDoneEvent extends Event
{
    public $checkup;

  /**
   * Create a new event instance.
   *
   * @param Checkup $checkup
   */
    public function __construct(Checkup $checkup)
    {
        $this->checkup = $checkup;
    }
}
