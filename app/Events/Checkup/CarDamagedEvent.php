<?php
/**
 * Created by PhpStorm.
 * User: mazda
 * Date: 10.12.2017
 * Time: 01:58
 */

namespace App\Events\Checkup;

use App\Events\Event;
use App\Models\Car;
use App\Models\Checkup\CheckupCarPart;
use App\User;

class CarDamagedEvent extends Event
{
    public $checkupCarPart;
    public $manager;
    public $car;
    public function __construct(CheckupCarPart $checkupCarPart, User $manager, Car $car)
    {
        $this->checkupCarPart = $checkupCarPart;
        $this->manager=$manager;
        $this->car=$car;
    }
}