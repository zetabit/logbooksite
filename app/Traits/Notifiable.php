<?php

namespace App\Traits;

trait Notifiable
{
    use HasDatabaseNotifications, RoutesNotifications;
}
