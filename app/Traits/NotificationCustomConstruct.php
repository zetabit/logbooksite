<?php

namespace App\Traits;

trait NotificationCustomConstruct
{
    public function __construct()
    {
        //not classes
        $types = ['string', 'integer', 'float', 'double', 'bool', 'boolean', 'array'];
        if(!isset($this->args)) return;
        $avail_keys = array_keys($this->args);
        $avail_values = array_values($this->args);
        $args = func_get_args();
        foreach ($args as $index => $val) {
            $curKey = $avail_keys[$index];
            $curVal = $avail_values[$index];

            $class = $curVal['type'];

            if (!($val instanceof $class) && !in_array($class, $types)) {
                $this->$curKey = $class::where('id', '=', $val)->first();
            } else {
                $this->$curKey = $val;
            }
        }

        if (method_exists($this, 'afterConstruct'))
            $this->{'afterConstruct'}();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {
        $avail_keys = array_keys($this->args);
        $avail_values = array_values($this->args);
        $result = [];

        for ($i = 0; $i < sizeof($avail_keys); $i++)
        {
            $key = $avail_keys[$i];
            $val = $avail_values[$i];
            if (isset($val['key'])) {
                $db_key = $val['key'];
                $result[$db_key] = $this->{$key}->id;
            } else {
                $result[$key] = $this->$key;
            }
        }

        return $result;
    }
}
