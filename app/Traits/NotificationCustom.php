<?php

namespace App\Traits;

trait NotificationCustom
{
    public function render($variant = 'short')
    {
        $attributes = $this->attributes;
        $type = $this->type;
        $class = $type;

        $data = isset($attributes['data']) ? (array)json_decode($attributes['data']) : [];

        // Return a new instance of the specified class
        $m = new $class(...array_values($data));

        foreach ($attributes as $key => $val) {
            if($key != 'data')// && in_array($key, $m->fillable))
                $m->attributes[$key] = $val;
        }

        if (method_exists($m, 'preRender'))
            $m->{'preRender'}();

        $type = (str_replace(['notifications\\', 'notification','app\\'], ['','',''], strtolower($type)));
        return (string)view('notifications.'.$type.'_'.$variant, ['notification' => $m, 'databaseNotification' => $this])->render();
    }
}
