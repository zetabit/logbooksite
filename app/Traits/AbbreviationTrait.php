<?php namespace App\Traits;

use App\Models\Car;
use App\Models\Checkup;
use App\Models\BaseContract;
use App\Models\Contracts\ReceiptContract;
use App\Models\Driver;
use App\User;

trait AbbreviationTrait{

    public function getGenerateAbbreviationAttribute()
    {
        $abbreviation = "";

        switch (__CLASS__) {
            case Car::class:
                $abbreviation = 'К' . $this->auto_park_id . 'А' . $this->id;
                break;
            case User::class:
                $abbreviation = 'К' . $this->auto_park_id . 'С' . $this->id;
                break;
            case Driver::class:
                $abbreviation = 'К' . $this->auto_park_id . 'В' . $this->id;
                break;
            case Checkup::class:
                switch ($this->type) {
                    case Checkup::TYPE_REVIEW:
                        $abbreviation = 'К' . $this->user->auto_park_id . 'С' . $this->user_id . 'А' . $this->car_id . 'В' . $this->driver_id . 'О' . $this->created_at->format('Ymd') . '/' . $this->id;
                        break;
                    case Checkup::TYPE_RECEIPT:
                        $abbreviation = 'К' . $this->user->auto_park_id . 'С' . $this->user_id . 'А' . $this->car_id . 'В' . $this->driver_id . 'АВ' . $this->created_at->format('Ymd') . '/' . $this->id;
                        break;
                    case Checkup::TYPE_TRANSMIT:
                        $abbreviation = 'К' . $this->user->auto_park_id . 'С' . $this->user_id . 'А' . $this->car_id . 'В' . $this->driver_id . 'АП' . $this->created_at->format('Ymd') . '/' . $this->id;
                        break;
                    case Checkup::TYPE_NZ:
                        $abbreviation = 'К' . $this->user->auto_park_id . 'С' . $this->user_id . 'А' . $this->car_id . 'В' . $this->driver_id . 'О' . $this->created_at->format('Ymd') . '/' . $this->id;
                        break;
                }
                break;
            case BaseContract::class:
            case ReceiptContract::class:
                $abbreviation = 'ДОГ' . $this->id;
                break;
        }

        return $abbreviation;
    }

}