<?php namespace App\Traits;

use Eventviva\ImageResize;
use Illuminate\Support\Facades\Input;
use Validator;

trait MediaTrait{
    public $images = [
        'image' => [    //default
            'input_name' => 'image',
            'type' => IMAGETYPE_JPEG,   //needed
            'filename'   => '', //optional, if empty - using slug
            'ext'   => 'jpg',   //needed
            'quality'   => 100, //optional
            'permissions' => 0644,   //optional
            'use_hash_in_slug' => true,
            'if_not_upload' => '',  //if not upload, copying default img
            //'to_width' => 1100, //optional, resize to width
            'thumbnails' => [
                [
                    'filename' => '', //optional, if empty - using slug, slug_{$number}
                    'width' => 340,
                    'height' => 354,
                    'allow_enlarge' => false,
                    'crop' => true,
                    'position' => \Eventviva\ImageResize::CROPCENTER
                ],

            ]
        ]
    ];
    /*
     * public $imagePath = 'Upload';
     */

    /**
     * @param string $images_name
     * @return bool
     */
    public function uploadImage($images_name = 'image')
    {
        $imParams = isset($this->images[$images_name]) ? $this->images[$images_name] : $this->images['image'];
        $name = $imParams['input_name'];
        // getting all of the post data
        $file = array($name => Input::file($name));
        // setting up rules
        $rules = array($name => 'required|image|mimes:jpeg,png,ico,tiff,gif,bmp'); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
        }
        $destinationPath = public_path().DIRECTORY_SEPARATOR. $this->imagePath; // upload path

        if (Input::file($name)) {
            if( Input::file($name)->isValid() || isset($imParams['if_not_upload']) && strlen($imParams['if_not_upload'])>0 ) $this->deleteImages();
        }
        if($imParams['use_hash_in_slug'] && Input::file($name)) $this->setAttribute('slug', md5_file(Input::file($name)->path()).'-'.$this->generateRandomString(5) );
        $this->setAttribute('extension', $imParams['ext']);
        $imParams['filename'] = isset($imParams['filename']) && strlen($imParams['filename'])>0 ? $imParams['filename'] : $this->getAttribute('slug');
        $fileName = $imParams['filename'].'.'.$imParams['ext'];//renaming image
        $fname = $destinationPath.DIRECTORY_SEPARATOR.$fileName;
        $thumbnails = $imParams['thumbnails'];
        if( !file_exists( $destinationPath )) mkdir( $destinationPath );
        // checking file is valid.
        if (Input::file($name)) {
            /*
            if(!Input::file($name)->isValid()){
                // sending back with error message.
                return false;
            }
            */
            $image = new ImageResize( Input::file($name)->path() );
            if(isset($imParams['to_width']) && $image->getSourceWidth()>$imParams['to_width'])
            {
                $image->resizeToWidth($imParams['to_width']);
            }
            $image->save($fname, $imParams['type'], isset($imParams['quality']) ? $imParams['quality'] : 100, isset($imParams['permissions']) ? $imParams['permissions'] : 0644);
            foreach ($thumbnails as $key => $val){
                $thumbnail = $val;
                $thumbnail['filename'] = isset($thumbnail['filename']) && strlen($thumbnail['filename'])>0 ? $thumbnail['filename'] : $this->getAttribute('slug').'_'.$key;
                if($imParams['use_hash_in_slug'] && Input::file($name)) $thumbnail['filename'] = $this->getAttribute('slug').'_'.$key;
                $fileName_thumb = $thumbnail['filename'].'.'.$imParams['ext'];// renaming image
                $fname_thumb = $destinationPath.DIRECTORY_SEPARATOR.$fileName_thumb;

                if(isset($thumbnail['crop']) && $thumbnail['crop']) {
                    $image->crop($thumbnail['width'], $thumbnail['height'], $thumbnail['allow_enlarge'], $thumbnail['position']);
                    $image->save($fname_thumb, $imParams['type'], isset($imParams['quality']) ? $imParams['quality'] : 100, isset($imParams['permissions']) ? $imParams['permissions'] : 0644);
                } else {
                    $image->resize($thumbnail['width'], $thumbnail['height'], $thumbnail['allow_enlarge']);
                    $image->resize($thumbnail['width'], $thumbnail['height'], $thumbnail['allow_enlarge']);
                    $image->save($fname_thumb, $imParams['type'], isset($imParams['quality']) ? $imParams['quality'] : 100, isset($imParams['permissions']) ? $imParams['permissions'] : 0644);
                }
            }
            return true;
        } elseif ( isset($imParams['if_not_upload']) && strlen($imParams['if_not_upload'])>0 && file_exists($imParams['if_not_upload']) && !file_exists($fname)){
            if($imParams['use_hash_in_slug'] && file_exists($imParams['if_not_upload'])) $this->setAttribute('slug', md5_file($imParams['if_not_upload']).'-'.$this->generateRandomString(5) );
            $this->setAttribute('extension', $imParams['ext']);
            $imParams['filename'] = isset($imParams['filename']) && strlen($imParams['filename'])>0 ? $imParams['filename'] : $this->getAttribute('slug');
            $fileName = $imParams['filename'].'.'.$imParams['ext'];//renaming image
            $fname = $destinationPath.DIRECTORY_SEPARATOR.$fileName;
            //\File::copy($imParams['if_not_upload'], $fname);
            $image = new ImageResize( $imParams['if_not_upload'] );
            $fname = str_replace("/", DIRECTORY_SEPARATOR, $fname);
            $image->save($fname , $imParams['type'], isset($imParams['quality']) ? $imParams['quality'] : 100, isset($imParams['permissions']) ? $imParams['permissions'] : 0644);
            foreach ($thumbnails as $key => $val){
                $thumbnail = $val;
                $thumbnail['filename'] = isset($thumbnail['filename']) && strlen($thumbnail['filename'])>0 ? $thumbnail['filename'] : $this->getAttribute('slug').'_'.$key;
                if($imParams['use_hash_in_slug'] && Input::file($name)) $thumbnail['filename'] = $this->getAttribute('slug').'_'.$key;
                $fileName_thumb = $thumbnail['filename'].'.'.$imParams['ext'];// renaming image
                $fname_thumb = $destinationPath.DIRECTORY_SEPARATOR.$fileName_thumb;

                if(isset($thumbnail['crop']) && $thumbnail['crop']) {
                    $image->crop($thumbnail['width'], $thumbnail['height'], $thumbnail['allow_enlarge'], $thumbnail['position']);
                    $image->save($fname_thumb, $imParams['type'], isset($imParams['quality']) ? $imParams['quality'] : 100, isset($imParams['permissions']) ? $imParams['permissions'] : 0644);
                } else {
                    $image->resize($thumbnail['width'], $thumbnail['height'], $thumbnail['allow_enlarge']);
                    $image->resize($thumbnail['width'], $thumbnail['height'], $thumbnail['allow_enlarge']);
                    $image->save($fname_thumb, $imParams['type'], isset($imParams['quality']) ? $imParams['quality'] : 100, isset($imParams['permissions']) ? $imParams['permissions'] : 0644);
                }
            }
            return true;
        }
        return false;
    }

    public function setImageFileName($image, $filename){
        $this->images[$image]['filename'] = $filename;
        foreach ($this->images[$image]['thumbnails'] as $key => $val){
            $val['filename'] = $filename.'_thumb';

        }
    }

    public function setImageInputFileName($image, $input_name){
        $this->images[$image]['input_name'] = $input_name;
    }

    public function deleteImages(){
        foreach ($this->images as $im_name => $imParams){
            $destinationPath = public_path().DIRECTORY_SEPARATOR. $this->imagePath; // upload path
            $imParams['filename'] = isset($imParams['filename']) && strlen($imParams['filename'])>0 ? $imParams['filename'] : $this->getAttribute('slug');
            $fileName = $imParams['filename'].'.'.$imParams['ext'];//renaming image
            $fname = $destinationPath.DIRECTORY_SEPARATOR.$fileName;
            if(file_exists($fname)) unlink($fname);
            $thumbnails = $imParams['thumbnails'];
            foreach ($thumbnails as $key=>$val) {
                $thumbnail = $val;
                $thumbnail['filename'] = isset($thumbnail['filename']) && strlen($thumbnail['filename'])>0 ? $thumbnail['filename'] : $this->getAttribute('slug').'_'.$key;
                $fileName_thumb = $thumbnail['filename'].'.'.$imParams['ext'];// renaming image
                $fname_thumb = $destinationPath.DIRECTORY_SEPARATOR.$fileName_thumb;
                if(file_exists($fname_thumb)) unlink($fname_thumb);
            }
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}